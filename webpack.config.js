const webpack = require('webpack');
const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');
const { WebpackManifestPlugin } = require('webpack-manifest-plugin');
const TerserPlugin = require("terser-webpack-plugin");

// const dev = process.env.APP_ENV !== 'production';
const dev = true;

const file = "app"; /* pages || app */
const outputFile = file === "app" ? '' : "/auth";
const publicPath = "/public/assets" + outputFile + "/";

const plugins = [
    new MiniCssExtractPlugin({
        filename: dev ? '[name].css' : '[name].[chunkhash:5].css',
        chunkFilename: dev ? '[id].css' : '[id].[chunkhash:5].css',
    }),
];

if (dev) {
    plugins.push(new webpack.HotModuleReplacementPlugin());
}

if (!dev) {
    plugins.push(new WebpackManifestPlugin());
}
  
let config = {

    mode: dev ? 'development' : 'production',

    entry: {
        app: './app/assets/js/' + file + '.js'
    },

    output: {
        path: path.resolve(__dirname, "public/assets" + outputFile),
        filename: dev ? '[name].js' : '[name].[chunkhash:5].js',
        publicPath: publicPath
    },

    devServer: {
        open: true,
        host: 'localhost',
    },

    plugins,

    module: {
        rules: [
            {
                test: /\\.(js|jsx)$/,
                exclude: /(node_modules|bower_components)/,
                use: ['babel-loader'],
            },
            {
                test: /\.css$/i,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                        options: {
                            publicPath: publicPath,
                        },
                    },
                    {
                        loader: "css-loader",
                        options: {
                        //   modules: true,
                          importLoaders: 1,
                        },
                    },
                    'postcss-loader'
                ],
            },
        ],
    },

    devtool: dev ? "eval-cheap-module-source-map" : false,

    optimization: {
        minimize: !dev,
        minimizer: [
            new CssMinimizerPlugin({
                minimizerOptions: {
                    level: {
                        1: {
                            roundingPrecision: 'all=3,px=5'
                        },
                    },

                    preset: [
                        'default',
                        {
                            discardComments: { removeAll: true },
                        },
                    ],
                },
                minify: CssMinimizerPlugin.cleanCssMinify,
            }),
            new TerserPlugin({
                test: /\.js(\?.*)?$/i,
                terserOptions: {
                  ecma: 5,
                  parse: {},
                  compress: {},
                  mangle: true, // Note `mangle.properties` is `false` by default.
                  module: false,
                  // Deprecated
                  output: null,
                  format: null,
                  toplevel: false,
                  nameCache: null,
                  ie8: true,
                  keep_classnames: undefined,
                  keep_fnames: false,
                  safari10: true,
                },
            }),
        ],
        /*removeAvailableModules: !dev,*/
    },
};

module.exports = config;