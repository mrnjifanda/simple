# simple
<p align="center">
  <a href="https://https://www.jifitech.com/frameworks/simple">
    <img width="250" src="https://easytalk-school.com/blob/57031986289b4a93e254bfb2068fbac2be2cdb26/public/images/logo/easytalk/logo-100.png">
  </a>
</p>

# About-us
Simple is a PHP Framework 


## Usage

Install and active PHP ^7.0 and active extension
```jsx
  curl
  fileinfo
  gd
  mbstring
  mysqli
  openssl
  pdo_mysql
  sockets
  amqp
  pdo_sqlite
  sqlite3
```

Active extension amqp. If is not install on your system, you have error in composer install command. On windows if is not install read this link
```jsx
   https://stackoverflow.com/questions/54967199/install-amqp-in-windows-10-php-7-3
```

```jsx
git clone https://gitlab.com/mrnjifanda/simple.git
```

Install PHP dependencies

```jsx
composer install
```

Install NPM dependencies (dev)

```jsx
npm install
```

After run PHP serve

```jsx
php -S localhost:3500 -t public
If you have install node js just type: npm start
```