<?php

namespace Tests\TestHelpers;

use App\Helpers\UrlQuery;
use PHPUnit\Framework\TestCase;

class UrlQueryTest extends TestCase
{

    public function assertURLEquals (string $expected, string $url): void
    {

        $this->assertEquals($expected, urldecode($url));
    }

    public function testWithParam (): void
    {

        $url = UrlQuery::withParam([], "id", 1);
        $this->assertURLEquals("id=1", $url);
    }

    public function testWithParamWithArray (): void
    {

        $url = UrlQuery::withParam([], "key", [1, 2, 3]);
        $this->assertURLEquals("key=1,2,3", $url);
    }

    public function testWithParams (): void
    {

        $url = UrlQuery::withParams(["id" => 1], ["token" => "token-test"]);
        $this->assertURLEquals("id=1&token=token-test", $url);
    }

    public function testWithParamsWithArray (): void
    {

        $url = UrlQuery::withParams(["id" => 1], ["token" => "token-test", "ids" => [1, 2, 3]]);
        $this->assertURLEquals("id=1&token=token-test&ids=1,2,3", $url);
    }
}