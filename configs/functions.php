<?php

use App\Helpers\Lang;

$lang;

/**
 * setLang
 * 
 * Initialisation de la variable langue
 * @param  object $content
 * @return void
 */
function setLang(object $content): void
{
    global $lang;
    if (is_null($lang)) {

        $lang = $content;
    }
}

/**
 * getLang
 * 
 * Recupere la langue du navigateur et verifi si un COOKIE langue existe
 * @return string
 */
function getLang (): string
{

    if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {

        $langs = isset($_COOKIE['lang']) ? $_COOKIE['lang'] : substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
        return in_array($langs, ['fr', 'en']) ? $langs : 'en';
    }

    return 'en';
}

function env(?string $key = null)
{

    if ($key) {

        return isset(ENV[$key]) ? ENV[$key] : null;
    }
    return ENV;
}

/**
 * __
 * 
 * Retourne un mot ou une phrase dans la bonne langue
 * @param  string $path
 * @return string
 */
function __(string $path, string $folder = null): string
{

    global $lang;
    if (is_null($lang)) {

        setLang(Lang::getInstance()->getContent());
    }
    
    $paths = explode('.', $path);
    $string = $lang;
    foreach ($paths as $value) {

        $string = $string->$value;
    }
    return $string;
}

/**
 * replace
 * 
 * Replace all occurrences of the $search string with the replacement string
 * @param  array|string $subject
 * @param  array|string $search
 * @param  array|string $replace
 * @return array|string
 */
function replace($subject, $search = '.', $replace = '/')
{
    
    return str_replace($search, $replace, $subject);
}

/**
 * secureText
 * 
 * Sécuriser les chaînes de caracteres
 * @param  string $string Chaîne de caracter à sécurisé
 * @return string
 */
function secureText (string $string): ?string
{

    return strip_tags(htmlspecialchars($string));
}

/**
 * getContentFolder
 * 
 * Permet de recuper tout le contenu d'un dossier
 * @param  string $pathToFolder 
 * @return array|null
 */
function getContentFolder(string $pathToFolder): ?array
{

    if (!is_dir($pathToFolder)) {

        return null;
    }

    $contentFolder = scandir($pathToFolder . DIRECTORY_SEPARATOR, SCANDIR_SORT_DESCENDING);
    $countContentFolder = count($contentFolder);

    if ($countContentFolder <= 2) {

        return [];
    }

    unset($contentFolder[$countContentFolder - 1]);
    unset($contentFolder[$countContentFolder - 2]);
    return $contentFolder;
}

/**
 * isAjax
 * 
 * Verifie si la resquete est en Synchrone ou Asynchrone
 * @return bool
 */
function isAjax (): bool
{

    return (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest') ? true : false;
}

/**
 * requireToVar
 * 
 * @param string $path
 * @param mixed $value
 * @return string
 */
function requireToVar (string $path, $value, $app = null): string
{

    ob_start();
    require_once(ROOT . 'app' . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . $path);
    return ob_get_clean();
}

function component(string $path, array $params = []): string
{

    ob_start();
    extract($params);
    require_once($path);
    return ob_get_clean();
}

function layout(string $pathToFile, array $variables = [], ?string $pathToLayout = null): string
{

    extract($variables);
    ob_start();
    require_once BASE . replace($pathToFile) . '.php';
    $content = ob_get_clean();
    if ($pathToLayout && file_exists(LAYOUTS . replace($pathToLayout) . '.php')) {

        ob_start();
        require_once LAYOUTS . replace($pathToLayout) . '.php';
        $content = ob_get_clean();
    }
    return $content;
}


/**
 * str_remove
 * 
 * Permet de retirer un mot dans un texte
 * @param  string $srt
 * @param  string $str_remove
 * @return string
 */
function str_remove(string $srt, string $str_remove): string
{

    return str_replace($str_remove, '', $srt);
}

/**
 * getExtension
 * 
 * Recupere l'extension d'un fichier
 * @param  string $name
 * @return string
 */
function getExtension(string $name): string
{

    return strtolower(substr(strrchr($name, '.'), 1));
}

/**
 * is_image
 *
 * Verifier si un fichier est une image
 * @param  null|string $path
 * @return bool
 */
function is_image(?string $path = null): bool
{
	$a = getimagesize($path);
	$image_type = $a[2];
	if(in_array($image_type , array(IMAGETYPE_GIF , IMAGETYPE_JPEG ,IMAGETYPE_PNG , IMAGETYPE_BMP))) {

		return true;
	}
	return false;
}

/**
 * toArray
 * 
 * Convertir un object en un tableau de la forme ['id' => id, 'name' => name]
 * @param  mixed $objects
 * @return array
 */
function toArray ($objects = null): array
{

    $array = [];
    if (!$objects || empty($objects)) return $array;
    foreach ($objects as $object) { array_push($array, ['id' => $object->getId(), 'name' => $object->getName()]); }
    return $array;
}

function divPage (string $content): string
{

    return '<div class="content-page js-scroll">' . $content . '</div>';
}

/**
 * isExit
 * 
 * Verifier si une cle existe dans une tableau
 * @param  string $variable
 * @param  array $global
 * @return bool
 */
function isExit (string $variable, array $global): bool
{

    return isset($global[$variable]) && !empty($global[$variable]);
}

/**
 * isValue
 * 
 * Return la valeur si l'element existe ou null
 * @param  string $variable
 * @param  array $global
 * @return mixed
 */
function isValue (string $variable, array $global)
{

    return isExit($variable, $global) ? $global[$variable] : null;
}

/**
 * createFolder
 * 
 * Permet la création de répertoires imbriqués spécifiés.
 * @param  string $pathname Chemin du fichier
 * @return string|null
 */
function createFolder (string $pathname): bool
{

    return mkdir($pathname, 0777, true);
}

/**
 * imagesFolder
 * 
 * Permet la création de répertoires imbriqués a partie du dossier public/images.
 * @param  string $variable
 * @param  array $global
 * @return string|null
 */
function imagesFolder (string $pathname): bool
{

    $dir = ROOT . 'public' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . $pathname;
    return is_dir($dir) ? true : createFolder($dir);
}


/**
 * checkIsActiveLink
 * 
 * Verifier si le lien selectionner est le lien courant de la page
 * @param  string $href
 * @return bool
 */
function checkIsActiveLink (?string $href): bool
{

    $url = $_SERVER['REQUEST_URI'];
    return $href === $url ? true : false;
}

function setEmail(string $name): string
{

    return strtolower($name . '@' . ENV['APP_URL']);
}

/**
 * pays
 * 
 * Retourune la liste de tous les pays
 * @return array
 */
function pays (): array
{

    return ['Cameroun', 'Afghanistan', 'Afrique du Sud', 'Albanie', 'Algérie', 'Allemagne', 'Andorre', 'Angola', 'Antigua-et-Barbuda', 'Arabie saoudite', 'Argentine', 'Arménie', 'Australie', 'Autriche', 'Azerbaïdjan', 'Bahamas', 'Bahreïn', 'Bangladesh', 'Barbade', 'Belau', 'Belgique', 'Belize', 'Bénin', 'Bhoutan', 'Biélorussie', 'Birmanie', 'Bolivie', 'Bosnie-Herzégovine', 'Botswana', 'Brésil', 'Brunei', 'Bulgarie', 'Burkina', 'Burundi', 'Cambodge', 'Cameroun', 'Canada', 'Cap-Vert', 'Chili', 'Chine', 'Chypre', 'Colombie', 'Comores', 'Congo', 'Cook', 'Corée du Nord', 'Corée du Sud', 'Costa Rica', 'Côte d\'Ivoire', 'Croatie', 'Cuba', 'Danemark','Djibouti', 'Dominique', 'Égypte', 'Émirats arabes unis', 'Équateur', 'Érythrée', 'Espagne', 'Estonie', 'États-Unis', 'Guinée', 'Guinée-Bissao', 'Guinée équatoriale', 'Guyana', 'Haïti', 'Honduras', 'Hongrie', 'Inde', 'Indonésie', 'Iran', 'Iraq', 'Irlande', 'Islande', 'Israël', 'Italie', 'Jamaïque', 'Japon', 'Jordanie', 'Kazakhstan', 'Kenya', 'Kirghizistan', 'Kiribati', 'Koweït', 'Laos', 'Lesotho', 'Lettonie', 'Liban', 'Liberia', 'Libye', 'Liechtenstein', 'Lituanie', 'Luxembourg', 'Macédoine', 'Madagascar', 'Malaisie', 'Malawi', 'Maldives', 'Mali', 'Malte', 'Maroc', 'Marshall', 'Maurice', 'Mauritanie', 'Mexique', 'Micronésie', 'Moldavie', 'Monaco', 'Mongolie', 'Mozambique', 'Namibie', 'Nauru', 'Népal', 'Nicaragua', 'Niger', 'Nigeria', 'Niue', 'Norvège', 'Nouvelle-Zélande', 'Oman', 'Ouganda', 'Ouzbékistan', 'Pakistan', 'Panama', 'Papouasie - Nouvelle Guinée', 'Paraguay', 'Pays-Bas', 'Pérou', 'Philippines', 'Pologne', 'Portugal', 'Qatar', 'République centrafricaine', 'République dominicaine', 'République tchèque', 'Roumanie', 'Royaume-Uni', 'Russie', 'Rwanda', 'Saint-Christophe-et-Niévès', 'Sainte-Lucie', 'Saint-Marin', 'Saint-Siège ou leVatican', 'Saint-Vincent-et-les Grenadines', 'Salomon', 'Salvador', 'Samoa occidentales', 'Sao Tomé-et-Principe', 'Sénégal', 'Seychelles', 'Sierra Leone', 'Singapour', 'Slovaquie', 'Slovénie', 'Somalie', 'Soudan', 'Sri Lanka', 'Suède', 'Suisse', 'Suriname', 'Swaziland', 'Syrie', 'Tadjikistan', 'Tanzanie', 'Tchad', 'Thaïlande', 'Togo', 'Tonga', 'Trinité-et-Tobago', 'Tunisie', 'Turkménistan', 'Turquie', 'Tuvalu', 'Ukraine', 'Uruguay', 'Vanuatu', 'Venezuela', 'Viêt Nam', 'Yémen', 'Yougoslavie', 'Zaïre', 'Zambie', 'Zimbabwe'];
}