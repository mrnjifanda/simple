<?php


    require_once dirname(__DIR__) . DIRECTORY_SEPARATOR . 'app.php';

    use TeamTNT\TNTSearch\TNTSearch;
    use TeamTNT\TNTSearch\Stemmer\PorterStemmer;

    $db_config = $database->getDbInfos();
    $tnt = new TNTSearch;
    $tnt->loadConfig([
        'driver'    => 'mysql',
        'host'      => $db_config['host'],
        'database'  => $db_config['database'],
        'username'  => $db_config['username'],
        'password'  => $db_config['password'],
        'storage'   => ROOT . 'configs' . DIRECTORY_SEPARATOR . 'cmd' . DIRECTORY_SEPARATOR . 'tntsearch' . DIRECTORY_SEPARATOR . 'sqlite',
        'stemmer'   => PorterStemmer::class
    ]);

    $indexer = $tnt->createIndex('countries.index');
    $indexer->query('SELECT * FROM zones_countries');
    // $indexer->setPrimaryKey('article_id');
    $indexer->setLanguage('fench');
    $indexer->run();