-- MySQL dump 10.13  Distrib 8.0.28, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: easytalk_app
-- ------------------------------------------------------
-- Server version	8.0.28-0ubuntu0.20.04.3

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `contacts_mail_categories`
--

DROP TABLE IF EXISTS `contacts_mail_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `contacts_mail_categories` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `icon` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `supprimer` int DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `year` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacts_mail_categories`
--

/*!40000 ALTER TABLE `contacts_mail_categories` DISABLE KEYS */;
INSERT INTO `contacts_mail_categories` VALUES (1,'Boîte de réception','inbox','reception',NULL,NULL,'2021-03-04 22:04:20',NULL),(2,'Favoris','star-outline','favoris',NULL,NULL,'2021-03-04 22:04:49',NULL),(3,'Brouillon','cube-send','brouillon',NULL,NULL,'2021-03-04 22:05:11',NULL),(4,'Envoyés','send','envoyes',NULL,NULL,'2021-03-04 22:05:37',NULL),(5,'Archiver','archive','archives',NULL,NULL,'2021-03-04 22:06:29',NULL),(6,'Envois programmés','alarm-multiple','programmes',NULL,NULL,'2021-03-04 22:06:53',NULL),(7,'Spams','block-helper','spam',NULL,NULL,'2021-03-04 22:08:21',NULL),(8,'Corbeille','delete','corbeille',NULL,NULL,'2021-03-04 22:08:35',NULL);
/*!40000 ALTER TABLE `contacts_mail_categories` ENABLE KEYS */;

--
-- Table structure for table `schools`
--

DROP TABLE IF EXISTS `schools`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `schools` (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_continent` int NOT NULL,
  `id_country` int NOT NULL,
  `id_region` int NOT NULL,
  `id_departement` int NOT NULL,
  `id_arrondissement` int NOT NULL,
  `name` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `abreviation` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `devise` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` int NOT NULL,
  `cycle_academic` int NOT NULL,
  `enseignement` int NOT NULL,
  `language` int NOT NULL,
  `logo` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `supprimer` int DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `year` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schools`
--

/*!40000 ALTER TABLE `schools` DISABLE KEYS */;
INSERT INTO `schools` VALUES (1,5,1,6,5,4,'College Polyvalent de l\'unite','cpu','Discipline-Travail-Succes',2,3,3,1,'CPU-20210706111507.png',NULL,NULL,'2021-06-07 11:15:07',1),(2,5,1,1,1,1,'Mon ecole eric','MEE','M-E-E',1,3,2,3,'MEE-20210906174533.png',NULL,NULL,'2021-06-09 17:45:33',1),(3,5,2,2,2,2,'Mon ecole eric bis','MEEdfg','sdf-sdf-ggggg',2,1,2,3,NULL,NULL,NULL,'2021-06-09 17:50:01',1),(4,5,3,3,10,7,'Togo School','togschool','togo-school',2,3,2,2,'TOGSCHOOL-20211006192918.png',NULL,NULL,'2021-06-10 19:29:18',1);
/*!40000 ALTER TABLE `schools` ENABLE KEYS */;

--
-- Table structure for table `schools_address`
--

DROP TABLE IF EXISTS `schools_address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `schools_address` (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_school` int NOT NULL,
  `phone` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `phonefix` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `address_school` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `supprimer` int DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `year` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schools_address`
--

/*!40000 ALTER TABLE `schools_address` DISABLE KEYS */;
INSERT INTO `schools_address` VALUES (1,1,'695512345','695512345','cpu@gmail.com','Mbouda, Ouest, Cameroun',NULL,NULL,'2021-06-07 11:15:07',1),(2,1,'96974545','96974545','eric@gmail.com','sadsa sads sad',NULL,NULL,'2021-06-09 17:45:33',1),(3,1,'5498421','9846515','MEEdfg@gmail.com','Mbouda, Ouest, Cameroun',NULL,NULL,'2021-06-09 17:50:01',1),(4,1,'96571586','96571586','togschool@gmail.com','Mbouda, Ouest, Cameroun',NULL,NULL,'2021-06-10 19:29:18',1);
/*!40000 ALTER TABLE `schools_address` ENABLE KEYS */;

--
-- Table structure for table `schools_admins`
--

DROP TABLE IF EXISTS `schools_admins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `schools_admins` (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_user` int NOT NULL,
  `id_school` int NOT NULL,
  `started_at` datetime DEFAULT NULL,
  `ended_at` datetime DEFAULT NULL,
  `status` int DEFAULT NULL,
  `supprimer` int DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `year` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schools_admins`
--

/*!40000 ALTER TABLE `schools_admins` DISABLE KEYS */;
/*!40000 ALTER TABLE `schools_admins` ENABLE KEYS */;

--
-- Table structure for table `schools_configs_cycle`
--

DROP TABLE IF EXISTS `schools_configs_cycle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `schools_configs_cycle` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `supprimer` int DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `year` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schools_configs_cycle`
--

/*!40000 ALTER TABLE `schools_configs_cycle` DISABLE KEYS */;
INSERT INTO `schools_configs_cycle` VALUES (1,'Maternelle',NULL,NULL,NULL,NULL),(2,'Primaire',NULL,NULL,NULL,NULL),(3,'Secondaire',NULL,NULL,NULL,NULL),(4,'Supérieur',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `schools_configs_cycle` ENABLE KEYS */;

--
-- Table structure for table `schools_configs_enseignement`
--

DROP TABLE IF EXISTS `schools_configs_enseignement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `schools_configs_enseignement` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `supprimer` int DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `year` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schools_configs_enseignement`
--

/*!40000 ALTER TABLE `schools_configs_enseignement` DISABLE KEYS */;
INSERT INTO `schools_configs_enseignement` VALUES (1,'Technique',NULL,NULL,NULL,NULL),(2,'Général',NULL,NULL,NULL,NULL),(3,'Polyvalent',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `schools_configs_enseignement` ENABLE KEYS */;

--
-- Table structure for table `schools_configs_langues`
--

DROP TABLE IF EXISTS `schools_configs_langues`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `schools_configs_langues` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `supprimer` int DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `year` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schools_configs_langues`
--

/*!40000 ALTER TABLE `schools_configs_langues` DISABLE KEYS */;
INSERT INTO `schools_configs_langues` VALUES (1,'Francophone',NULL,NULL,NULL,NULL),(2,'Anglophone',NULL,NULL,NULL,NULL),(3,'Bilingue',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `schools_configs_langues` ENABLE KEYS */;

--
-- Table structure for table `schools_configs_type`
--

DROP TABLE IF EXISTS `schools_configs_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `schools_configs_type` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `supprimer` int DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `year` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schools_configs_type`
--

/*!40000 ALTER TABLE `schools_configs_type` DISABLE KEYS */;
INSERT INTO `schools_configs_type` VALUES (1,'Public',NULL,NULL,NULL,NULL),(2,'Priver',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `schools_configs_type` ENABLE KEYS */;

--
-- Table structure for table `settings_years`
--

DROP TABLE IF EXISTS `settings_years`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `settings_years` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `supprimer` int DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `year` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings_years`
--

/*!40000 ALTER TABLE `settings_years` DISABLE KEYS */;
/*!40000 ALTER TABLE `settings_years` ENABLE KEYS */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `role` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `identifiant` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `nom` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `prenom` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `date_naissance` date NOT NULL,
  `lieu_naissance` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `sexe` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `region_origine` int DEFAULT NULL,
  `nationalite` int NOT NULL,
  `pays_de_residense` int NOT NULL,
  `addresse` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `cni` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `matricule` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `maladies` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `autres_informations` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `statut` int DEFAULT NULL,
  `lasted_at` datetime DEFAULT NULL,
  `remember_token` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `confirmed_at` datetime DEFAULT NULL,
  `confirmation_token` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `reset_token` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `reset_at` datetime DEFAULT NULL,
  `supprimer` int DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `year` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin','AF20CMR1','admin','$2y$10$ErUERSr/OkK5RvQEjxDPyuDWT65Hzw5nzoyVo5PuBP5a9nelfS99C','Njifanda Pieumou','Eric Parfait','1999-07-12','Baleng','M',NULL,1,1,'Akwa-Nord','205AD54S','17S95361','695512345','admin@gmail.com',NULL,NULL,NULL,NULL,'1gAedTkhVkzP75l6nbPYsuZYW6HmLEWGfEBXGvOxWVNoy6xkls3OYEtjq619w5FDxbQLPedlWTfs7m8QpKYqbjPOCQxtHGD5Y73dz8cFqwuCzyJeabO54AwIwcf5mprprsmh12SLmra0sbol46t0IkQPW2EthyhzH8yD2dSI17EGF9Xok7rXzksIVvrNKSArQhjllus3hGgrFa6gHNsbnUkhUTcnA3wVx6Bd49CKMYGI4X0BluQ84Jt77t','2020-09-28 16:48:25',NULL,'6N9zUCCklOAulSWOR4u3kD8jqiRYp7glbiOOthLqJVyxv8T36EGa5CkGrADzZKdgipsE7qQ7HUVxFHx3mpITxjldsxcBdHQVNjbbdBNni0ULNsjlvHRaSwTrwRv7SgNBDB16BJlZUTI9qGIe2XQr4p','2021-05-05 17:39:48',NULL,NULL,'2020-09-28 16:45:25',1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

--
-- Table structure for table `years`
--

DROP TABLE IF EXISTS `years`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `years` (
  `id` int NOT NULL AUTO_INCREMENT,
  `debut` int NOT NULL,
  `fin` int NOT NULL,
  `supprimer` int DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `years`
--

/*!40000 ALTER TABLE `years` DISABLE KEYS */;
INSERT INTO `years` VALUES (1,2020,2021,NULL,NULL,'2020-10-16 10:18:42');
/*!40000 ALTER TABLE `years` ENABLE KEYS */;

--
-- Table structure for table `zones_arrondissement`
--

DROP TABLE IF EXISTS `zones_arrondissement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `zones_arrondissement` (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_continent` int NOT NULL,
  `id_country` int NOT NULL,
  `id_region` int NOT NULL,
  `id_departement` int NOT NULL,
  `name` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `supprimer` int DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `year` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zones_arrondissement`
--

/*!40000 ALTER TABLE `zones_arrondissement` DISABLE KEYS */;
INSERT INTO `zones_arrondissement` VALUES (1,5,1,1,1,'Bepanda',NULL,NULL,'2021-03-21 21:21:27',1),(2,5,2,2,2,'arranNigeria',NULL,NULL,'2021-03-23 15:47:06',1),(3,4,5,4,3,'SuisseArrondissement',NULL,NULL,'2021-03-25 16:32:43',1),(4,5,1,6,5,'Bamboutos',NULL,NULL,'2021-03-28 02:35:26',1),(5,5,1,1,1,'zxc zcxz',NULL,NULL,'2021-05-17 17:41:41',1),(6,5,1,8,6,'cxzvfdsf ',NULL,NULL,'2021-05-17 17:45:20',1),(7,5,3,3,10,'xc sc',NULL,NULL,'2021-05-17 17:46:35',1),(8,5,3,3,10,'sdasdffh',NULL,NULL,'2021-05-17 17:46:48',1);
/*!40000 ALTER TABLE `zones_arrondissement` ENABLE KEYS */;

--
-- Table structure for table `zones_continents`
--

DROP TABLE IF EXISTS `zones_continents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `zones_continents` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `supprimer` int DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `year` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zones_continents`
--

/*!40000 ALTER TABLE `zones_continents` DISABLE KEYS */;
INSERT INTO `zones_continents` VALUES (1,'Amérique du Nord',NULL,NULL,'2020-10-07 08:12:15',1),(2,'Amérique du Sud',NULL,NULL,'2020-10-07 08:12:15',1),(3,'Asie',NULL,NULL,'2020-10-07 08:12:15',1),(4,'Europe',NULL,NULL,'2020-10-07 08:12:15',1),(5,'Afrique',NULL,NULL,'2020-10-07 08:12:15',1),(6,'Océanie',NULL,NULL,'2020-10-07 08:12:15',1);
/*!40000 ALTER TABLE `zones_continents` ENABLE KEYS */;

--
-- Table structure for table `zones_countries`
--

DROP TABLE IF EXISTS `zones_countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `zones_countries` (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_continent` int NOT NULL,
  `name` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `supprimer` int DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `year` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zones_countries`
--

/*!40000 ALTER TABLE `zones_countries` DISABLE KEYS */;
INSERT INTO `zones_countries` VALUES (1,5,'Cameroun',NULL,NULL,'2021-03-21 21:19:02',1),(2,5,'Nigeria',NULL,NULL,'2021-03-23 15:45:23',1),(3,5,'Togo',NULL,NULL,'2021-03-23 16:57:55',1),(4,4,'France',NULL,NULL,'2021-03-25 16:17:27',1),(5,4,'Suise',NULL,NULL,'2021-03-25 16:19:31',1),(6,5,'Senegal',NULL,NULL,'2021-05-07 03:09:07',1),(7,5,'sdfsdfd',NULL,NULL,'2021-05-07 03:10:47',1),(8,3,'Togodsd',NULL,NULL,'2021-05-07 03:12:30',1),(9,5,'Camer',NULL,'2021-05-07 12:05:31','2021-05-07 07:21:09',1),(10,4,'Camerounbnc',NULL,NULL,'2021-05-07 15:35:50',1),(11,4,'Italie',NULL,'2021-05-17 17:49:36','2021-05-07 15:41:34',1),(12,3,'Cameroungfcbvxz',NULL,NULL,'2021-05-07 15:43:12',1),(13,6,'CamerounNew',NULL,'2021-05-10 18:50:56','2021-05-07 19:10:34',1),(14,1,'Camerounwasdff',NULL,NULL,'2021-05-10 18:09:39',1),(15,4,'EricToto',NULL,NULL,'2021-05-17 14:17:22',1),(16,2,'EricToto2',NULL,NULL,'2021-05-17 14:18:49',1),(17,3,'EricToto3',NULL,'2021-05-17 14:24:08','2021-05-17 14:19:27',1);
/*!40000 ALTER TABLE `zones_countries` ENABLE KEYS */;

--
-- Table structure for table `zones_departements`
--

DROP TABLE IF EXISTS `zones_departements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `zones_departements` (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_continent` int NOT NULL,
  `id_country` int NOT NULL,
  `id_region` int NOT NULL,
  `name` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `supprimer` int DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `year` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zones_departements`
--

/*!40000 ALTER TABLE `zones_departements` DISABLE KEYS */;
INSERT INTO `zones_departements` VALUES (1,5,1,1,'Douala',NULL,NULL,'2021-03-21 21:20:14',1),(2,5,2,2,'depar1Nigeria1',NULL,NULL,'2021-03-23 15:46:37',1),(3,4,5,4,'SuiseDepartement',NULL,NULL,'2021-03-25 16:27:04',1),(4,4,5,5,'SuiseDepartement2',NULL,NULL,'2021-03-25 16:27:34',1),(5,5,1,6,'Mbouda',NULL,NULL,'2021-03-28 02:35:07',1),(6,5,1,8,'sd sdf gs',NULL,NULL,'2021-05-17 16:54:11',1),(7,5,1,9,'dsdf',NULL,NULL,'2021-05-17 16:54:20',1),(8,5,1,8,'sdgsg',NULL,NULL,'2021-05-17 16:57:23',1),(9,5,1,1,'sgd sg',NULL,NULL,'2021-05-17 16:57:31',1),(10,5,3,3,'adfas faad',NULL,NULL,'2021-05-17 17:46:24',1);
/*!40000 ALTER TABLE `zones_departements` ENABLE KEYS */;

--
-- Table structure for table `zones_regions`
--

DROP TABLE IF EXISTS `zones_regions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `zones_regions` (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_continent` int NOT NULL,
  `id_country` int NOT NULL,
  `name` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `supprimer` int DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `year` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zones_regions`
--

/*!40000 ALTER TABLE `zones_regions` DISABLE KEYS */;
INSERT INTO `zones_regions` VALUES (1,5,1,'Litorral',NULL,NULL,'2021-03-21 21:19:36',1),(2,5,2,'regionNigeria1',NULL,NULL,'2021-03-23 15:45:46',1),(3,5,3,'regTogo',NULL,NULL,'2021-03-23 16:58:40',1),(4,4,5,'SuiseRegion',NULL,NULL,'2021-03-25 16:22:05',1),(5,4,5,'SuiseRegion2',NULL,NULL,'2021-03-25 16:23:04',1),(6,5,1,'Ouest',NULL,NULL,'2021-03-28 02:34:31',1),(7,5,1,'fgfgh',NULL,NULL,'2021-05-17 15:49:32',1),(8,5,1,'sdfsdf',NULL,NULL,'2021-05-17 15:52:30',1),(9,5,1,'asdsffaf',NULL,NULL,'2021-05-17 16:00:04',1),(10,5,1,'dfwfga',NULL,NULL,'2021-05-17 16:00:59',1),(11,5,1,'fafasfsaf',NULL,NULL,'2021-05-17 16:01:59',1),(12,4,5,'ygfdgdg',NULL,NULL,'2021-05-17 16:06:58',1),(13,4,5,'sdfsdffg',NULL,NULL,'2021-05-17 16:07:11',1),(14,5,3,'sdghgfhszg gf',NULL,NULL,'2021-05-17 16:07:35',1),(15,5,3,'dgdfg',NULL,NULL,'2021-05-17 16:08:41',1),(16,5,9,'dfg dfg dg',NULL,NULL,'2021-05-17 16:09:05',1),(17,3,8,'fgfghTogo',NULL,NULL,'2021-05-20 10:28:42',1);
/*!40000 ALTER TABLE `zones_regions` ENABLE KEYS */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-03-19 16:08:55