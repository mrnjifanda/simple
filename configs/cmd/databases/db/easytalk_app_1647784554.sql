

CREATE TABLE `contacts_mail_categories` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `icon` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `supprimer` int DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `year` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

INSERT INTO contacts_mail_categories VALUES("1","Boîte de réception","inbox","reception","","","2021-03-04 22:04:20","");
INSERT INTO contacts_mail_categories VALUES("2","Favoris","star-outline","favoris","","","2021-03-04 22:04:49","");
INSERT INTO contacts_mail_categories VALUES("3","Brouillon","cube-send","brouillon","","","2021-03-04 22:05:11","");
INSERT INTO contacts_mail_categories VALUES("4","Envoyés","send","envoyes","","","2021-03-04 22:05:37","");
INSERT INTO contacts_mail_categories VALUES("5","Archiver","archive","archives","","","2021-03-04 22:06:29","");
INSERT INTO contacts_mail_categories VALUES("6","Envois programmés","alarm-multiple","programmes","","","2021-03-04 22:06:53","");
INSERT INTO contacts_mail_categories VALUES("7","Spams","block-helper","spam","","","2021-03-04 22:08:21","");
INSERT INTO contacts_mail_categories VALUES("8","Corbeille","delete","corbeille","","","2021-03-04 22:08:35","");



CREATE TABLE `schools` (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_continent` int NOT NULL,
  `id_country` int NOT NULL,
  `id_region` int NOT NULL,
  `id_departement` int NOT NULL,
  `id_arrondissement` int NOT NULL,
  `name` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `abreviation` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `devise` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` int NOT NULL,
  `cycle_academic` int NOT NULL,
  `enseignement` int NOT NULL,
  `language` int NOT NULL,
  `logo` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `supprimer` int DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `year` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

INSERT INTO schools VALUES("1","5","1","6","5","4","College Polyvalent de l'unite","cpu","Discipline-Travail-Succes","2","3","3","1","CPU-20210706111507.png","","","2021-06-07 11:15:07","1");
INSERT INTO schools VALUES("2","5","1","1","1","1","Mon ecole eric","MEE","M-E-E","1","3","2","3","MEE-20210906174533.png","","","2021-06-09 17:45:33","1");
INSERT INTO schools VALUES("3","5","2","2","2","2","Mon ecole eric bis","MEEdfg","sdf-sdf-ggggg","2","1","2","3","","","","2021-06-09 17:50:01","1");
INSERT INTO schools VALUES("4","5","3","3","10","7","Togo School","togschool","togo-school","2","3","2","2","TOGSCHOOL-20211006192918.png","","","2021-06-10 19:29:18","1");



CREATE TABLE `schools_address` (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_school` int NOT NULL,
  `phone` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `phonefix` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `address_school` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `supprimer` int DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `year` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

INSERT INTO schools_address VALUES("1","1","695512345","695512345","cpu@gmail.com","Mbouda, Ouest, Cameroun","","","2021-06-07 11:15:07","1");
INSERT INTO schools_address VALUES("2","1","96974545","96974545","eric@gmail.com","sadsa sads sad","","","2021-06-09 17:45:33","1");
INSERT INTO schools_address VALUES("3","1","5498421","9846515","MEEdfg@gmail.com","Mbouda, Ouest, Cameroun","","","2021-06-09 17:50:01","1");
INSERT INTO schools_address VALUES("4","1","96571586","96571586","togschool@gmail.com","Mbouda, Ouest, Cameroun","","","2021-06-10 19:29:18","1");



CREATE TABLE `schools_admins` (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_user` int NOT NULL,
  `id_school` int NOT NULL,
  `started_at` datetime DEFAULT NULL,
  `ended_at` datetime DEFAULT NULL,
  `status` int DEFAULT NULL,
  `supprimer` int DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `year` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;




CREATE TABLE `schools_configs_cycle` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `supprimer` int DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `year` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

INSERT INTO schools_configs_cycle VALUES("1","Maternelle","","","","");
INSERT INTO schools_configs_cycle VALUES("2","Primaire","","","","");
INSERT INTO schools_configs_cycle VALUES("3","Secondaire","","","","");
INSERT INTO schools_configs_cycle VALUES("4","Supérieur","","","","");



CREATE TABLE `schools_configs_enseignement` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `supprimer` int DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `year` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

INSERT INTO schools_configs_enseignement VALUES("1","Technique","","","","");
INSERT INTO schools_configs_enseignement VALUES("2","Général","","","","");
INSERT INTO schools_configs_enseignement VALUES("3","Polyvalent","","","","");



CREATE TABLE `schools_configs_langues` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `supprimer` int DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `year` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

INSERT INTO schools_configs_langues VALUES("1","Francophone","","","","");
INSERT INTO schools_configs_langues VALUES("2","Anglophone","","","","");
INSERT INTO schools_configs_langues VALUES("3","Bilingue","","","","");



CREATE TABLE `schools_configs_type` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `supprimer` int DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `year` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

INSERT INTO schools_configs_type VALUES("1","Public","","","","");
INSERT INTO schools_configs_type VALUES("2","Priver","","","","");



CREATE TABLE `settings_years` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `supprimer` int DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `year` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;




CREATE TABLE `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `role` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `identifiant` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `nom` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `prenom` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `date_naissance` date NOT NULL,
  `lieu_naissance` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `sexe` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `region_origine` int DEFAULT NULL,
  `nationalite` int NOT NULL,
  `pays_de_residense` int NOT NULL,
  `addresse` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `cni` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `matricule` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `maladies` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `autres_informations` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `statut` int DEFAULT NULL,
  `lasted_at` datetime DEFAULT NULL,
  `remember_token` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `confirmed_at` datetime DEFAULT NULL,
  `confirmation_token` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `reset_token` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `reset_at` datetime DEFAULT NULL,
  `supprimer` int DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `year` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

INSERT INTO users VALUES("1","admin","AF20CMR1","admin","$2y$10$ErUERSr/OkK5RvQEjxDPyuDWT65Hzw5nzoyVo5PuBP5a9nelfS99C","Njifanda Pieumou","Eric Parfait","1999-07-12","Baleng","M","","1","1","Akwa-Nord","205AD54S","17S95361","695512345","admin@gmail.com","","","","","1gAedTkhVkzP75l6nbPYsuZYW6HmLEWGfEBXGvOxWVNoy6xkls3OYEtjq619w5FDxbQLPedlWTfs7m8QpKYqbjPOCQxtHGD5Y73dz8cFqwuCzyJeabO54AwIwcf5mprprsmh12SLmra0sbol46t0IkQPW2EthyhzH8yD2dSI17EGF9Xok7rXzksIVvrNKSArQhjllus3hGgrFa6gHNsbnUkhUTcnA3wVx6Bd49CKMYGI4X0BluQ84Jt77t","2020-09-28 16:48:25","","6N9zUCCklOAulSWOR4u3kD8jqiRYp7glbiOOthLqJVyxv8T36EGa5CkGrADzZKdgipsE7qQ7HUVxFHx3mpITxjldsxcBdHQVNjbbdBNni0ULNsjlvHRaSwTrwRv7SgNBDB16BJlZUTI9qGIe2XQr4p","2021-05-05 17:39:48","","","2020-09-28 16:45:25","1");



CREATE TABLE `years` (
  `id` int NOT NULL AUTO_INCREMENT,
  `debut` int NOT NULL,
  `fin` int NOT NULL,
  `supprimer` int DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

INSERT INTO years VALUES("1","2020","2021","","","2020-10-16 10:18:42");



CREATE TABLE `zones_arrondissement` (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_continent` int NOT NULL,
  `id_country` int NOT NULL,
  `id_region` int NOT NULL,
  `id_departement` int NOT NULL,
  `name` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `supprimer` int DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `year` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

INSERT INTO zones_arrondissement VALUES("1","5","1","1","1","Bepanda","","","2021-03-21 21:21:27","1");
INSERT INTO zones_arrondissement VALUES("2","5","2","2","2","arranNigeria","","","2021-03-23 15:47:06","1");
INSERT INTO zones_arrondissement VALUES("3","4","5","4","3","SuisseArrondissement","","","2021-03-25 16:32:43","1");
INSERT INTO zones_arrondissement VALUES("4","5","1","6","5","Bamboutos","","","2021-03-28 02:35:26","1");
INSERT INTO zones_arrondissement VALUES("5","5","1","1","1","zxc zcxz","","","2021-05-17 17:41:41","1");
INSERT INTO zones_arrondissement VALUES("6","5","1","8","6","cxzvfdsf ","","","2021-05-17 17:45:20","1");
INSERT INTO zones_arrondissement VALUES("7","5","3","3","10","xc sc","","","2021-05-17 17:46:35","1");
INSERT INTO zones_arrondissement VALUES("8","5","3","3","10","sdasdffh","","","2021-05-17 17:46:48","1");



CREATE TABLE `zones_continents` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `supprimer` int DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `year` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

INSERT INTO zones_continents VALUES("1","Amérique du Nord","","","2020-10-07 08:12:15","1");
INSERT INTO zones_continents VALUES("2","Amérique du Sud","","","2020-10-07 08:12:15","1");
INSERT INTO zones_continents VALUES("3","Asie","","","2020-10-07 08:12:15","1");
INSERT INTO zones_continents VALUES("4","Europe","","","2020-10-07 08:12:15","1");
INSERT INTO zones_continents VALUES("5","Afrique","","","2020-10-07 08:12:15","1");
INSERT INTO zones_continents VALUES("6","Océanie","","","2020-10-07 08:12:15","1");



CREATE TABLE `zones_countries` (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_continent` int NOT NULL,
  `name` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `supprimer` int DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `year` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

INSERT INTO zones_countries VALUES("1","5","Cameroun","","","2021-03-21 21:19:02","1");
INSERT INTO zones_countries VALUES("2","5","Nigeria","","","2021-03-23 15:45:23","1");
INSERT INTO zones_countries VALUES("3","5","Togo","","","2021-03-23 16:57:55","1");
INSERT INTO zones_countries VALUES("4","4","France","","","2021-03-25 16:17:27","1");
INSERT INTO zones_countries VALUES("5","4","Suise","","","2021-03-25 16:19:31","1");
INSERT INTO zones_countries VALUES("6","5","Senegal","","","2021-05-07 03:09:07","1");
INSERT INTO zones_countries VALUES("7","5","sdfsdfd","","","2021-05-07 03:10:47","1");
INSERT INTO zones_countries VALUES("8","3","Togodsd","","","2021-05-07 03:12:30","1");
INSERT INTO zones_countries VALUES("9","5","Camer","","2021-05-07 12:05:31","2021-05-07 07:21:09","1");
INSERT INTO zones_countries VALUES("10","4","Camerounbnc","","","2021-05-07 15:35:50","1");
INSERT INTO zones_countries VALUES("11","4","Italie","","2021-05-17 17:49:36","2021-05-07 15:41:34","1");
INSERT INTO zones_countries VALUES("12","3","Cameroungfcbvxz","","","2021-05-07 15:43:12","1");
INSERT INTO zones_countries VALUES("13","6","CamerounNew","","2021-05-10 18:50:56","2021-05-07 19:10:34","1");
INSERT INTO zones_countries VALUES("14","1","Camerounwasdff","","","2021-05-10 18:09:39","1");
INSERT INTO zones_countries VALUES("15","4","EricToto","","","2021-05-17 14:17:22","1");
INSERT INTO zones_countries VALUES("16","2","EricToto2","","","2021-05-17 14:18:49","1");
INSERT INTO zones_countries VALUES("17","3","EricToto3","","2021-05-17 14:24:08","2021-05-17 14:19:27","1");



CREATE TABLE `zones_departements` (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_continent` int NOT NULL,
  `id_country` int NOT NULL,
  `id_region` int NOT NULL,
  `name` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `supprimer` int DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `year` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

INSERT INTO zones_departements VALUES("1","5","1","1","Douala","","","2021-03-21 21:20:14","1");
INSERT INTO zones_departements VALUES("2","5","2","2","depar1Nigeria1","","","2021-03-23 15:46:37","1");
INSERT INTO zones_departements VALUES("3","4","5","4","SuiseDepartement","","","2021-03-25 16:27:04","1");
INSERT INTO zones_departements VALUES("4","4","5","5","SuiseDepartement2","","","2021-03-25 16:27:34","1");
INSERT INTO zones_departements VALUES("5","5","1","6","Mbouda","","","2021-03-28 02:35:07","1");
INSERT INTO zones_departements VALUES("6","5","1","8","sd sdf gs","","","2021-05-17 16:54:11","1");
INSERT INTO zones_departements VALUES("7","5","1","9","dsdf","","","2021-05-17 16:54:20","1");
INSERT INTO zones_departements VALUES("8","5","1","8","sdgsg","","","2021-05-17 16:57:23","1");
INSERT INTO zones_departements VALUES("9","5","1","1","sgd sg","","","2021-05-17 16:57:31","1");
INSERT INTO zones_departements VALUES("10","5","3","3","adfas faad","","","2021-05-17 17:46:24","1");



CREATE TABLE `zones_regions` (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_continent` int NOT NULL,
  `id_country` int NOT NULL,
  `name` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `supprimer` int DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `year` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

INSERT INTO zones_regions VALUES("1","5","1","Litorral","","","2021-03-21 21:19:36","1");
INSERT INTO zones_regions VALUES("2","5","2","regionNigeria1","","","2021-03-23 15:45:46","1");
INSERT INTO zones_regions VALUES("3","5","3","regTogo","","","2021-03-23 16:58:40","1");
INSERT INTO zones_regions VALUES("4","4","5","SuiseRegion","","","2021-03-25 16:22:05","1");
INSERT INTO zones_regions VALUES("5","4","5","SuiseRegion2","","","2021-03-25 16:23:04","1");
INSERT INTO zones_regions VALUES("6","5","1","Ouest","","","2021-03-28 02:34:31","1");
INSERT INTO zones_regions VALUES("7","5","1","fgfgh","","","2021-05-17 15:49:32","1");
INSERT INTO zones_regions VALUES("8","5","1","sdfsdf","","","2021-05-17 15:52:30","1");
INSERT INTO zones_regions VALUES("9","5","1","asdsffaf","","","2021-05-17 16:00:04","1");
INSERT INTO zones_regions VALUES("10","5","1","dfwfga","","","2021-05-17 16:00:59","1");
INSERT INTO zones_regions VALUES("11","5","1","fafasfsaf","","","2021-05-17 16:01:59","1");
INSERT INTO zones_regions VALUES("12","4","5","ygfdgdg","","","2021-05-17 16:06:58","1");
INSERT INTO zones_regions VALUES("13","4","5","sdfsdffg","","","2021-05-17 16:07:11","1");
INSERT INTO zones_regions VALUES("14","5","3","sdghgfhszg gf","","","2021-05-17 16:07:35","1");
INSERT INTO zones_regions VALUES("15","5","3","dgdfg","","","2021-05-17 16:08:41","1");
INSERT INTO zones_regions VALUES("16","5","9","dfg dfg dg","","","2021-05-17 16:09:05","1");
INSERT INTO zones_regions VALUES("17","3","8","fgfghTogo","","","2021-05-20 10:28:42","1");

