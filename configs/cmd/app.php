<?php

use App\App;
use App\Database\Database;

    define('URI', $_SERVER['REQUEST_URI'] ?? null);
    define('URL', explode('/', URI));
    define('ROOT', dirname(__DIR__, 2) . DIRECTORY_SEPARATOR);
    define('VIEWS', ROOT . 'app' . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR);
    define('BASE', VIEWS . 'base' . DIRECTORY_SEPARATOR);
    define('LAYOUTS', BASE . 'layouts' . DIRECTORY_SEPARATOR);

    require_once ROOT . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';
    $dotenv = Dotenv\Dotenv::createImmutable(ROOT);
    $env = (array) $dotenv->load();
    define('ENV', $env);

    $app = new App($env);
    $database = new Database($app);

    // RewriteRule ^(.*)$ public/index.php?url=$1 [QSA,L]
