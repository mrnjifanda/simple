<?php

    require_once dirname(__DIR__) . DIRECTORY_SEPARATOR . 'app.php';

    use App\Queues\Queues;

    try {

        $queue = $env['QUEUE_NAME'];
        Queues::consume($queue);
    } catch (\Throwable $th) {

        echo json_encode([
            "message" => $th->getMessage(),
            "code" => $th->getCode()
        ]);
    }
