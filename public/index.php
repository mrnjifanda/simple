<?php

use App\App;
use App\Router;
use App\Helpers\Lang;

define('URI', $_SERVER['REQUEST_URI']);
define('URL', explode('/', URI));
define('ROOT', dirname(__DIR__) . DIRECTORY_SEPARATOR);
define('VIEWS', ROOT . 'app' . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR);
define('BASE', VIEWS . 'base' . DIRECTORY_SEPARATOR);
define('LAYOUTS', BASE . 'layouts' . DIRECTORY_SEPARATOR);

require_once ROOT . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';
$dotenv = Dotenv\Dotenv::createImmutable(ROOT);
$env = (array) $dotenv->load();
define('ENV', $env);

if ($env['APP_ENV'] === "dev") {
    
    $whoops = new \Whoops\Run;
    $whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler);
    if (\Whoops\Util\Misc::isAjaxRequest()) {

        $jsonHandler = new Whoops\Handler\JsonResponseHandler();
        $jsonHandler->setJsonApi(true);
        $whoops->pushHandler($jsonHandler);
    }

    $whoops->register();
}

setLang(Lang::getInstance()->getContent());
$app = new App($env);
if (isExit('page', $_GET) && $_GET['page'] === '1') {

    $url = explode('?', URL)[0];
    unset($_GET['page']);
    $query = http_build_query($_GET);
    $url = $url . (empty($query) ? '' : '?' . $query);
    $app->redirect($url, 301);
}

$router = new Router(ROOT . 'app' . DIRECTORY_SEPARATOR, $app, URI);
require_once ROOT . 'routes' .DIRECTORY_SEPARATOR . 'Routes.php';
