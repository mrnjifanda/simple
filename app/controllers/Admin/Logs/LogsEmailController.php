<?php

namespace Controller\Admin\Logs;

class LogsEmailController extends LogsController
{

    public function __construct()
    {
        parent::__construct();
    }

        
    public function email (): void
    {
        $this->AppRender('logs.email');
    }
}
