<?php

namespace Controller\Admin\Logs;

use App\Html\A;
use App\Logs\Logs;
use App\Html\Table;
use App\Users\Users;
use Controller\Admin\AdminsController;

class LogsController extends AdminsController
{

    public function __construct()
    {
        parent::__construct();
    }

    public function showLogHTML(string $day): string
    {
        $dataDay = Logs::getContentDay($day);
        $content = Logs::arrayTitle($day) .
            Table::open(['Activité', 'User', 'Created At'], 'table-logs-database');

        if ($dataDay) {
            foreach ($dataDay as $key => $value) {
                $value = json_decode($value);
                $content .= Table::tr(
                    $key + 1,
                    '<div class="flex justify-between">
                        <span>' . $value->action .'</span>
                        <span>' . A::view('logs/' . $day, $key, 'ma-class-css') . '</span>
                    </div>',
                    Users::getUsernameById($this->app, $value->id_user),
                    $value->created_at
                );
            }
        } else {
            $content .= Table::tr(0, '...', '...', '...');
        }

        $content .= Table::close();
        return $content;
    }
}
