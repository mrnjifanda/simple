<?php

namespace Controller\Admin\Logs;


class LogsErrorController extends LogsController
{

    public function __construct()
    {
        parent::__construct();
    }

        
    public function error (): void
    {
        $this->AppRender('logs.error');
    }
}
