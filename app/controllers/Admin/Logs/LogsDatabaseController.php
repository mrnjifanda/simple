<?php

namespace Controller\Admin\Logs;

use App\Logs\Logs;
use App\Users\Users;

class LogsDatabaseController extends LogsController
{

    public function __construct()
    {
        parent::__construct();
    }

    public function database (): void
    {
        $years = Logs::getYears();
        $content = $this->showLogHTML(Logs::getLastDay());

        $this->AppRender('logs.database', compact('years', 'content'));
    }

    public function viewDetailDay (array $params): void
    {
        $years = Logs::getYears();
        $content = $this->showLogHTML($params['day']);

        $this->AppRender('logs.database', compact('years', 'content'));
    }

    public function databaseLogDetail (array $params): void
    {
        $day = $params['day'];
        $key = $params['id'];
        $type = 'database';
        $logs = Logs::getDetailLog($day, $key);
        $userDoAction = Users::getUser($this->app, $logs->id_user);

        $this->AppRender('logs.logs-view', compact('type', 'day', 'userDoAction', 'logs'));
    }
}
