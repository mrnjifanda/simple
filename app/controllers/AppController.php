<?php

namespace Controller;

class AppController extends Controller
{
    protected $user;
    protected $role;

    public function __construct()
    {

        parent::__construct();
        $this->user = $this->auth->requireUser();
        $this->role =  method_exists($this->user, 'getRole') ? $this->user->getRole() : null;
    }

    protected function AppRender (string $view, array $params = [], bool $prefix_by_role = true, string $layout = 'app'): void
    {

        $user = $this->user;
        $this->render(($prefix_by_role ? $this->role . '.' : null) . $view, array_merge(compact('user'), $params), $layout);
    }

    public function dashboard (): void
    {

        $this->AppRender('dashboard');
    }

    public function account (): void
    {

        $this->AppRender('account.account', [], false);
    }

    public function logout (): void
    {

        $this->cookies->delete('remember');
        $this->sessions->setFlash('success', 'Déconnexion de votre espace réussie !!!');
        $this->sessions->unset('auth');
        $this->app->redirect('/login');
    }
}
