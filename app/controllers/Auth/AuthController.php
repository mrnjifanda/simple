<?php

namespace Controller\Auth;

// use App\Mails\Mails;

use App\Helpers\Encode;
use App\Html\Form\AuthForm;
use Controller\AuthsController;

class AuthController extends AuthsController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function loginView (): void
    {
        // Mails::mail('Test des mails 6', '<h1>Mon titre 6</h1>');
        // Mails::mail('Test des mails layout', 'testMail', null, null, ['nom' => 'eric']);
        // dd($this->request);
        $this->AuthRender('auth.login');
    }

    public function forgetPasswordView (): void
    {
        $this->AuthRender('auth.forget-password');
    }

    public function resetPasswordView (array $params): void
    {
        $username = $params['username'];
        $token = $params['token'];
        $this->auth->verifyResetToken($username, $token);
        $form = new AuthForm($_POST);
        $this->AuthRender('auth.reset-password', compact('username', 'token', 'form'));
    }

    public function activateAccountView (array $token): void
    {

        $token = $token['token'];
        $form = new AuthForm($_POST);
        $this->AuthRender('auth.activate-account', compact('token', 'form'));
    }

    public function loginPost (): void
    {

        $this->setErrors("Vos identifiants sont incorrects");
        $validation = $this->validation($this->request->post(), [
            'username' => 'required',
            'password' => 'required|min:8',
            'remember' => 'optional|alpha'
        ]);

        if ($validation) {
            $user = $this->auth->login($_POST['username'], $_POST['password'], $_POST['remember'] ?? null);
            if ($user) {

                $encode = new Encode();
                $token = $encode->encode(['id' => $user]);
                $this->sessions->write('auth', $token);
                $this->sessions->setFlash('success', 'Vous êtes maintenant connecté');
                $this->addKey('page', '/dashboard');
                $this->addKey('encode', $token);
                $this->unsetErrors();
            }
        }

        echo $this->response();
    }

    public function forgetPasswordPost (): void
    {
        $validation = $this->validation($_POST, [
            'email' => 'required|email|exist:users:email',
            'remember' => 'required|alpha'
        ]);

        if ($validation) {

            $user = $this->auth->sendForgetPassword($_POST['email']);
            if ($user) {

                $this->sessions->setFlash('success', 'Vérifiez vos messages ou mails');
                $this->addKey('page', '/login');
                $this->addKey('notif', ["type" => "success", "message" => "Instruction de réinitialisation envoyé"]);
                $this->addKey('reset', true);
                $this->unsetErrors();
                //send email url : reset-password/$user['username']/$user['token']
            }
        }

        echo $this->response(true);
    }

    public function resetPasswordPost (array $params): void
    {
        $this->auth->verifyResetToken($params['username'], $params['token']);

        $validation = $this->validation($_POST, [
            'password' => 'required|min:8|confirm',
            'remember' => 'required|alpha'
        ]);

        if ($validation) {
            $this->auth->resetPassword($_POST['password'], $params['username'], true);
            $this->sessions->setFlash('success', 'Mot de passe modifier');
            $this->addKey('notif', ["type" => "success", "message" => "Mot de passe modifier"]);
            $this->addKey('page', '/login');
            $this->addKey('reset', true);
            $this->unsetErrors();
        }

        echo $this->response(true);
    }

    public function activateAccountPost (array $params): void
    {
        // $this->AuthRender('auth.active-account');
    }
}
