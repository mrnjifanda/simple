<?php

    namespace Controller\FormRequest;

    use App\App;
    use App\Helpers\Validator;

    class FormRequest {

        # Importes all validations requests
        use \Traits\Validations\AuthValidationsTrait;

        private $app;
        private $datas = [];
        private $validator = null;

        public function __construct(array $datas, App $app)
        {

            $this->app = $app;
            $this->datas = $datas;
        }

        /**
         * validation
         * 
         * Create new instance of validation class
         * @return Validator
         */
        public function validator(): Validator
        {

            if (is_null($this->validator)) {

                $this->validator = new Validator($this->datas, $this->app);
            }
            return $this->validator;
        }
    }