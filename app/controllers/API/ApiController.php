<?php

    namespace Controller\API;

    # Annotation Swagger
    use OpenApi\Annotations as OA;

    # Traits
    use Traits\API\ApiTraits;
    use Traits\API\ResponseTrait;

    use App\Helpers\Validator;
    use Controller\FormRequest\FormRequest;

    /**
     * @OA\Info(
     *      title="API easyTalk",
     *      version="1.0.0",
     *      description="Documentation de l'API de easyTalk",
     *      @OA\Contact(
     *          name="Mr NJIFANDA",
     *          email="dev@easytalk-school.com"
     *      ),
     *      @OA\License(
     *          name="MIT",
     *          url="https://www.njifanda.com",
     *      ),
     *      @OA\Attachable()
     * ),
     * @OA\SecurityScheme(
     *      securityScheme="bearer_token",
     *      in="header",
     *      name="bearer_token",
     *      type="http",
     *      scheme="bearer",
     *      bearerFormat="JWT",
     * ),
     * @OA\SecurityScheme(
     *      securityScheme="Signature",
     *      in="header",
     *      name="Signature",
     *      type="apiKey",
     * ),
     * @OA\Server(
     *      url="http://localhost:8000/api/v1/",
     *      description="Api Base URL",
     * ),
     * @OA\Schemes(format="http")
     */
    class ApiController
    {

        use ApiTraits;
        use ResponseTrait;

        /**
         * @OA\Tag(
         *     name="Users",
         *     description="Users endpoints",
         * )
         */
        public function __construct()
        {

            $this->initConfig();
            $signiature = $this->request->verify_signiature();
            if (!$signiature) {

                return $this->notSigned();
            }
        }

        protected function formRequest(array $datas = []): FormRequest
        {

            $datas = empty($datas) ? $this->post : $datas;
            return new FormRequest($datas, $this->app);
        }

        protected function validate(array $datas = [], array $rules = [])
        {

            if (!empty($datas) && !empty($rules)) {

                $validator = new Validator($datas, $this->app);
                $validator->validate($rules);
            } else if (empty($rules)) {

                return $this->formRequest($datas);
            }
        }



        /**
         * @OA\Get(
         *      path="/usdfers",
         *      summary="All users in database.",
         *      description="Collect all of a user favorite cars.",
         *      operationId="id_users",
         *      security={
         *          {"bearer_token":{}},
         *          {"Signature":{}},
         *      },
         *      tags={"Users"},
         *      @OA\Parameter(
         *          name="limit",
         *          in="query",
         *          description="Le nombre d'article a recuperer",
         *          required=false,
         *          @OA\Schema(type="integer")
         *      ),
         *      @OA\Response(
         *          response="200",
         *          description="All users",
         *          @OA\JsonContent(type="array", @OA\Items(ref="#/components/schemas/User"))
         *      )
         * )
         */
        public function users()
        {

            // try {

            //     $users = $this->test_service->all();
            //     $this->mail_service->register('ericparfait.njifanda@gmail.com', 'OkK5RvQEjxDPyuDWT65Hzw5nzoyVo5PuBP5a9nelfS99C');
            //     return $this->respone(false, $users);
            // } catch (\Exception $e) {

            //     return $this->respone(true, [
            //         "message" => $e->getMessage(),
            //         "code" => $e->getCode()
            //     ]);
            // }
        }

        /**
         * @OA\Get(
         *      path="/users/{id}",
         *      summary="Get one user by id.",
         *      description="Collect of a user.",
         *      operationId="id_one_user",
         *      security={
         *          {"bearer_token":{}},
         *          {"Signature":{}},
         *      },
         *      tags={"Users"},
         *      @OA\Parameter(
         *          name="id",
         *          in="path",
         *          description="ID de l'utilisateur",
         *          required=true,
         *          @OA\Schema(type="integer")
         *      ),
         *      @OA\Response(
         *          response="200",
         *          description="Get one user by id",
         *          @OA\JsonContent(
         *              type="object",
         *              ref="#/components/schemas/User"
         *          )
         *      )
         * )
         */
        public function user(int $id)
        {
            # description="Retourne tous les utilisateurs du site"
            
            // try {

            //     $user = $this->test_service->find($id);
            //     return $this->respone(false, $user);
            // } catch (\Exception $e) {

            //     return $this->respone(true, [
            //         "message" => $e->getMessage(),
            //         "code" => $e->getCode()
            //     ]);
            // }
        }
    }

    // header("Access-Control-Allow-Origin: *");
    // header("Content-Type: application/json");

    // header("Access-Control-Allow-Origin: *");
    // header("Access-Control-Allow-Headers: access");
    // header("Content-Type: application/json; charset=UTF-8");
    // header("Access-Control-Allow-Methods: POST");
    // header("Access-Control-Max-Age: 3600");
    // header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With, Signature");
