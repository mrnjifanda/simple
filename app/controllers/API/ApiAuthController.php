<?php

    namespace Controller\API;

    use Service\Users\UserService;
    use OpenApi\Annotations as OA;

    /**
     * @OA\Tag(
     *     name="Auth",
     *     description="Authetification endpoints",
     * )
    */
    class ApiAuthController extends ApiController
    {

        /**
         * @var UserService
         */
        protected $user_service;

        public function __construct() {

            parent::__construct();
            $this->user_service = new UserService($this->auth, true);
        }

        /**
         * @OA\Post(
         *      path="/login",
         *      summary="Login user.",
         *      description="Auth: Login.",
         *      operationId="id_auth_login_api",
         *      security={
         *          {"Signature":{}},
         *      },
         *      tags={"Auth"},
         *      @OA\RequestBody(
         *          required=true,
         *          description="Auth Login",
         *          @OA\JsonContent(
         *              required={"username", "password"},
         *              @OA\Property(property="username", type="string", example="contact@easytalk.com", @OA\Schema(type="string")),
         *              @OA\Property(property="password", type="string", example="Password@123", @OA\Schema(type="string")),
         *          ),
         *      ),
         *      @OA\Response(
         *          response="200",
         *          description="Your authentication was successful",
         *          @OA\JsonContent(
         *              @OA\Property(property="error", type="boolean", example="false", @OA\Schema(type="boolean")),
         *              @OA\Property(property="status", type="string", example="Ok", @OA\Schema(type="string")),
         *              @OA\Property(property="status_code", type="integer", example="200", @OA\Schema(type="integer")),
         *              @OA\Property(
         *                  property="datas",
         *                  type="array",
         *                  @OA\Items(
         *                      @OA\Property(
         *                          property="token",
         *                          type="object",
         *                          @OA\Property(property="token", type="string", example="eyJ0eXAiOiJKV1QiLCOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vMTjAuMC4xIX0.CKKZTshof4vk5jq_jC3knHYhIgohBPLEYuI", @OA\Schema(type="string")),
         *                          @OA\Property(property="tokend_expired", type="integer", example="1650529730", @OA\Schema(type="integer")),
         *                      ),
         *                      @OA\Property(
         *                          property="user",
         *                          type="object",
         *                          ref="#/components/schemas/User"
         *                      ),
         *                  ),
         *              ),
         *          )
         *      ),
         *      @OA\Response(
         *          response="400",
         *          description="Request Not Signed",
         *          @OA\JsonContent(
         *              @OA\Property(property="error", type="boolean", example="true", @OA\Schema(type="boolean")),
         *              @OA\Property(property="status", type="string", example="Request Not Signed", @OA\Schema(type="string")),
         *              @OA\Property(property="status_code", type="integer", example="422", @OA\Schema(type="integer")),
         *              @OA\Property(property="message", type="string", example="Please add the signature in the header", @OA\Schema(type="string")),
         *          )
         *      ),
         *      @OA\Response(
         *          response="401",
         *          description="Unauthorized Error Occur",
         *          @OA\JsonContent(
         *              @OA\Property(property="error", type="boolean", example="true", @OA\Schema(type="boolean")),
         *              @OA\Property(property="status", type="string", example="Unauthorized Error Occur", @OA\Schema(type="string")),
         *              @OA\Property(property="status_code", type="integer", example="401", @OA\Schema(type="integer")),
         *              @OA\Property(property="message", type="string", example="Resource protect by authentication", @OA\Schema(type="string")),
         *          )
         *      ),
         *      @OA\Response(
         *          response="422",
         *          description="Unprocessable Entity",
         *          @OA\JsonContent(
         *              @OA\Property(property="error", type="boolean", example="true", @OA\Schema(type="boolean")),
         *              @OA\Property(property="status", type="string", example="Unprocessable Entity", @OA\Schema(type="string")),
         *              @OA\Property(property="status_code", type="integer", example="422", @OA\Schema(type="integer")),
         *              @OA\Property(property="message", type="string", example="A validation error occurred", @OA\Schema(type="string")),
         *              @OA\Property(property="datas", type="string", example="Your credentials are incorrect", @OA\Schema(type="string")),
         *          )
         *      ),
         *      @OA\Response(
         *          response="520",
         *          description="Web server is returning an unknown error",
         *          @OA\JsonContent(
         *              @OA\Property(property="error", type="boolean", example="true", @OA\Schema(type="boolean")),
         *              @OA\Property(property="status", type="string", example="Web server is returning an unknown error", @OA\Schema(type="string")),
         *              @OA\Property(property="status_code", type="integer", example="520", @OA\Schema(type="integer")),
         *              @OA\Property(property="message", type="string", example="An error occurred, please try again later", @OA\Schema(type="string")),
         *          )
         *      )
         * )
         */
        public function login()
        {

            $this->validate()->login();
            $id_user = $this->auth->login($this->post['username'], $this->post['password'], $this->post['remember'] ?? null);
            if (is_int($id_user)) {

                $encode = $this->jwt_code->encode(['id' => $id_user]);
                return $this->success([
                    'token' => [
                        'token' => $encode,
                        'tokend_expired' => time()
                    ],
                    'user' => $this->user_service->find($id_user)
                ]);
            }

            return $this->unprocessable('Your credentials are incorrect');
        }

        /**
         * @OA\Post(
         *      path="/forget-password",
         *      summary="Send email forget password.",
         *      description="Auth: Forget password.",
         *      operationId="id_auth_forget_password_api",
         *      security={
         *          {"Signature":{}},
         *      },
         *      tags={"Auth"},
         *      @OA\RequestBody(
         *          required=true,
         *          description="Send email forget password.",
         *          @OA\JsonContent(
         *              required={"email"},
         *              @OA\Property(property="email", type="string", example="contact@easytalk.com", @OA\Schema(type="string")),
         *          ),
         *      ),
         *      @OA\Response(
         *          response="200",
         *          description="Your authentication was successful",
         *          @OA\JsonContent(
         *              @OA\Property(property="error", type="boolean", example="false", @OA\Schema(type="boolean")),
         *              @OA\Property(property="status", type="string", example="Ok", @OA\Schema(type="string")),
         *              @OA\Property(property="status_code", type="integer", example="200", @OA\Schema(type="integer")),
         *              @OA\Property(property="message", type="string", example="Reset instructions have been sent", @OA\Schema(type="string")),
         *          )
         *      ),
         *      @OA\Response(
         *          response="400",
         *          description="Request Not Signed",
         *          @OA\JsonContent(
         *              @OA\Property(property="error", type="boolean", example="true", @OA\Schema(type="boolean")),
         *              @OA\Property(property="status", type="string", example="Request Not Signed", @OA\Schema(type="string")),
         *              @OA\Property(property="status_code", type="integer", example="422", @OA\Schema(type="integer")),
         *              @OA\Property(property="message", type="string", example="Please add the signature in the header", @OA\Schema(type="string")),
         *          )
         *      ),
         *      @OA\Response(
         *          response="401",
         *          description="Unauthorized Error Occur",
         *          @OA\JsonContent(
         *              @OA\Property(property="error", type="boolean", example="true", @OA\Schema(type="boolean")),
         *              @OA\Property(property="status", type="string", example="Unauthorized Error Occur", @OA\Schema(type="string")),
         *              @OA\Property(property="status_code", type="integer", example="401", @OA\Schema(type="integer")),
         *              @OA\Property(property="message", type="string", example="Resource protect by authentication", @OA\Schema(type="string")),
         *          )
         *      ),
         *      @OA\Response(
         *          response="422",
         *          description="Unprocessable Entity",
         *          @OA\JsonContent(
         *              @OA\Property(property="error", type="boolean", example="true", @OA\Schema(type="boolean")),
         *              @OA\Property(property="status", type="string", example="Unprocessable Entity", @OA\Schema(type="string")),
         *              @OA\Property(property="status_code", type="integer", example="422", @OA\Schema(type="integer")),
         *              @OA\Property(property="message", type="string", example="A validation error occurred", @OA\Schema(type="string")),
         *              @OA\Property(property="datas", type="string", example="Your credentials are incorrect", @OA\Schema(type="string")),
         *          )
         *      ),
         *      @OA\Response(
         *          response="520",
         *          description="Web server is returning an unknown error",
         *          @OA\JsonContent(
         *              @OA\Property(property="error", type="boolean", example="true", @OA\Schema(type="boolean")),
         *              @OA\Property(property="status", type="string", example="Web server is returning an unknown error", @OA\Schema(type="string")),
         *              @OA\Property(property="status_code", type="integer", example="520", @OA\Schema(type="integer")),
         *              @OA\Property(property="message", type="string", example="An error occurred, please try again later", @OA\Schema(type="string")),
         *          )
         *      )
         * )
         */
        public function forgetPassword()
        {

            $this->validate()->forgetPassword();
            $user = $this->user_service->forgetPassword($this->post['email']);
            if ($user) {

                return $this->success(__('mails.forgetPassword.successSend'));
            }
    
            return $this->unknown();
        }
    }
