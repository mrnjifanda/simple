<?php

namespace Controller;

use App\Html\Form\AuthForm;

class AuthsController extends Controller
{

    public function __construct()
    {

        parent::__construct();
        $this->auth->openUser();
    }

    protected function AuthRender (string $view, array $params = [], string $layout = 'auth'): void
    {

        $form = new AuthForm($_POST);
        $this->render($view, array_merge($params, compact('form')), $layout);
    }
}
