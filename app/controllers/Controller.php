<?php

namespace Controller;

use App\Helpers\Cookies;
use App\Helpers\Sessions;
use Traits\API\ApiTraits;
use App\Helpers\Validator;
use Traits\API\ResponseTrait;

class Controller
{
    use ResponseTrait;
    use ApiTraits;

    private $viewPath = VIEWS;
    private $basePath;
    private $layoutPath;

    protected $sessions;
    protected $cookies;
    protected $router;

    protected $errors = [
        "error" => ["type" => "warning", "message" => "Erreur dans un ou plusieurs champs"]
    ];

    private $validation;
    private $isvalid;
    private $data;

    public function __construct()
    {

        $this->initConfig();
        $this->sessions = Sessions::$sessions;
        $this->cookies = Cookies::$cookies;

        $this->basePath = $this->viewPath . 'base' . DIRECTORY_SEPARATOR;
        $this->layoutPath = $this->basePath . 'layouts' . DIRECTORY_SEPARATOR;
    }

    protected function getErrors(): ?array
    {
        return $this->errors;
    }

    protected function addKey(string $key, $value): void
    {
        $this->errors[$key] = $value;
    }

    protected function setErrors(string $message, ?string $type = null): void
    {
        $this->errors['error']['message'] = $message;
        if ($type) {
            $this->errors['error']['type'] = $type;
        }
    }

    protected function unsetErrors(): void
    {
        $this->errors['error'] = false;
    }

    public function validation(array $data, array $fields): bool
    {
        $this->data = $data;
        $this->validation = new Validator($data, $this->app);

        foreach ($fields as $field => $rules) {
            $rule = explode('|', $rules);

            foreach ($rule as $option) {

                if ($option == 'optional') {

                    if (empty($this->data[$field]) || empty($this->data[$field]['name'])) {
                        break 1;
                    }
                } else {
                    if (strpos($option, ':')) {

                        $explode = explode(':', $option);

                        if (count($explode) === 3) {
                            list($method, $param, $message) = $explode;

                            if ($method === 'file') {
                                $param = (int) $param;
                                $message = explode(',', $message);
                            }
                            $this->validation->$method($field, $param, $message);
                        } else {

                            list($method, $param) = $explode;
                            $this->validation->$method($field, $param);
                        }
                    } else {

                        $this->validation->$option($field);
                    }
                }

                if (!$this->validation->isVaLid($field)) {
                    break 1;
                }
            }
        }

        $this->isvalid = $this->validation->getErrors();
        return $this->isvalid === null ? true : false;
    }

    protected function response (bool $loop = false, int $code = 422): string
    {

        $errors = $this->getErrors();
        if ($errors['error'] !== false) {

            if ($loop) {
                foreach ($this->isvalid as $key => $value) {
                    $this->errors['errorForm'][$key] = $value;
                }
            }

            $this->app->header_status($code);
        }

        return json_encode($this->getErrors());
    }
        
    /**
     * render
     *
     * @param  string $view
     * @param  array $variables
     * @param  string $layout
     * @return void
     */
    protected function render (string $view, array $variables = [], string $layout = 'app'): void
    {

        $app = $this->app;
        $sessions = $this->sessions;
        $cookies = $this->cookies;
        $auth = $this->auth;

        $routes = function(string $name, array $params = []) {

            return $this->routes->url($name, $params);
        };

        if (!empty($variables)) {

            extract($variables);
        }

        if ($app->is_ajax()) {

            require_once $this->viewPath . str_replace('.', DIRECTORY_SEPARATOR, $view) . '.php';
        } else {

            ob_start();
            require_once $this->viewPath . str_replace('.', DIRECTORY_SEPARATOR, $view) . '.php';
            $pageContent = ob_get_clean();
            require_once $this->layoutPath . $layout . '.php';
        }
    }
}
