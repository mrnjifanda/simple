<?php
    use App\Html\Text;

    $user = $auth->lockUser();

    // $session->setFlash('success', 'Success Flash');
    // $session->setFlash('danger', 'Danger Flash');
    // $session->setFlash('warning', 'Warning Flash');
    // $session->setFlash('info', 'Info Flash');

    $pageTitle = 'Tableau de Bord';

    echo Text::titleH1('dashboard', $pageTitle, null, [$pageTitle]);
    require_once 'admin/dashboard.php';