<?php

$pageTitle = __('auth.login');
echo $form->open($routes('login-view'), $pageTitle),
     $form->username('username', __('auth.username')),
     $form->password('password', __('auth.password')),
     $form->checkbox(__('auth.remember'), null, $routes('forget-password-view'), __('auth.forgot-password')),
     $form->close($pageTitle);
