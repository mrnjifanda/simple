<?php

$pageTitle = 'Réinitialiser votre mot de passe';

echo $form->open($routes('reset-password-post', ['username' => $username, 'token' => $token] ), 'Nouveau mot de passe'),
        $form->password('password', 'Nouveau mot de passe'),
        $form->password('password-confirm', 'Confirmer votre mot de passe'),
        $form->checkbox('Réinitialiser votre mot de passe', true, $routes('login-view'), 'Connexion'),
     $form->close('Nouveau mot de passe');
