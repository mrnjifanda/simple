<?php

    $pageTitle = 'Activation de votre compte';

    echo $form->open($routes('activate-account-view', ['token' => $token]), 'Activer votre compte'),
            $form->username('username', 'Nom d\'utilisateur'),
            $form->password('password', 'Mot de passe'),
            $form->password('password-confirm', 'Comfirmation du mot de passe'),
            $form->checkbox('J\'accepte les conditions de confidenrialité', true),
         $form->close('Activer');