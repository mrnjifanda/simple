<?php
$pageTitle = 'Mot de passe oublié ?';

echo $form->open($routes('forget-password-view'), $pageTitle),
        $form->username('email', 'Votre email', 'email'),
        $form->checkbox('Recevoir les instructions', true, $routes('login-view'), 'Connexion'),
     $form->close($pageTitle);
