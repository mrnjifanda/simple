<?php
    use App\Html\Text;

    $content = '';
    $pageTitle = 'Mon compte';

    if ($user->getRole() === 'admin') {

        echo Text::content(['users', $pageTitle, $content, 'none; height: 95%'], [['account', $pageTitle], $pageTitle]);
    } else {

        echo Text::content(['user', $pageTitle, $content, 'none; height: 95%'], [['account', $pageTitle], $pageTitle]);
    }

