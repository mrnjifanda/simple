<?php

    use App\Html\Text;

    $pageTitle = 'Tableau de Bord';
    echo Text::titleH1('dashboard', $pageTitle, null, [$pageTitle]);
?>

<style>
    .card-group {}

    .card-group>.card {
        border-left: 0;
    }

    .card {
        border: 1px solid #e7eaed;
        width: 100%;
    }

    .card.card-style-2 i {
        font-size: 30px;
    }
</style>

<style>
    .jifi-tabs-systems.jifi-tabs-style-2 .jifi-tabs {
        width: auto;
        justify-content: left;
    }

    .jifi-tabs-systems.jifi-tabs-style-2 .jifi-tabs-content .jifi-tab-content {
        padding: 0;
    }
</style>

<div class="content-page js-scroll">

    <div class="p-1">
        <div class="card">
            <div class="card-body">
                <div class="jifi-tabs-systems jifi-tabs-style-2 mt-0">
                    <ul class="jifi-tabs">
                        <li class="jifi-tabs-li active">
                            <a class="jifi-js-tabs" href="#etablissements"><?= Text::i('building') ?> Etablissements</a>
                        </li>

                        <li class="jifi-tabs-li">
                            <a class="jifi-js-tabs" href="#enseignants"><?= Text::i('user') ?> Enseignants</a>
                        </li>

                        <li class="jifi-tabs-li">
                            <a class="jifi-js-tabs" href="#eleves"><?= Text::i('users') ?> Eleves</a>
                        </li>
                    </ul>

                    <div class="jifi-tabs-content">
                        <div class="jifi-tab-content active" id="etablissements">

                            <div class="card-group flex justify-between">

                                <div class="card card-style-2 border-top-left-radius-0 border-top-right-radius-0 border-bottom-right-radius-0 flex justify-center align-center p-2">
                                    <?= Text::i('calendar-heart mr-1 text-theme', null, true) ?>
                                    <div class="d-flex flex-column justify-content-around">
                                        <small>Start date</small>
                                        <h5 class="mb-0 text-bold text-black text-font-15">26 Jul 2018</h5>
                                    </div>
                                </div>

                                <div class="card card-style-2 border-radius-0 flex justify-center align-center p-2">
                                    <?= Text::i('currency-usd mr-1 text-success', null, true) ?>
                                    <div class="d-flex flex-column justify-content-around">
                                        <small>Revenue</small>
                                        <h5 class="mr-2 mb-0 text-bold text-black text-font-15">577 545 Fcfa</h5>
                                    </div>
                                </div>

                                <div class="card card-style-2 border-radius-0 flex justify-center align-center p-2 ">
                                    <?= Text::i('eye mr-1 text-danger', null, true) ?>
                                    <div class="d-flex flex-column justify-content-around">
                                        <small>Total views</small>
                                        <h5 class="mr-2 mb-0 text-bold text-black text-font-15">9833550</h5>
                                    </div>
                                </div>

                                <div class="card card-style-2 border-radius-0 flex justify-center align-center p-2 ">
                                    <?= Text::i('download mr-1 text-warning', null, true) ?>
                                    <div class="d-flex flex-column justify-content-around">
                                        <small>Downloads</small>
                                        <h5 class="mr-2 mb-0 text-bold text-black text-font-15">2233783</h5>
                                    </div>
                                </div>

                                <div class="card card-style-2 border-top-left-radius-0 border-top-right-radius-0 border-bottom-left-radius-0 flex justify-center align-center p-2">
                                    <?= Text::i('flag mr-1 text-blue', null, true) ?>
                                    <div class="d-flex flex-column justify-content-around">
                                        <small>Flagged</small>
                                        <h5 class="mr-2 mb-0 text-bold text-black text-font-15">3497843</h5>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="jifi-tab-content" id="enseignants">

                        </div>

                        <div class="jifi-tab-content" id="eleves">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="p-1"></div>

</div>
