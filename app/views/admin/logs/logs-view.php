<?php

use App\Html\Text;
use App\Logs\Logs;

$pageTitle =  "Details log | $type | $day";

$content = '<div class="float-right"><a href="#">imprimer</a></div>
            <ul class="mt-1">
                <li class="pb-1">Nom: <strong class="text-black">' . $userDoAction->getNom() . '</strong></li>
                <li class="pb-1">Prenom: <strong class="text-black">' . $userDoAction->getPrenom() . '</strong></li>
                <li class="pb-1">Username: <strong class="text-black">' .  $userDoAction->getUsername() . '</strong></li>
                <li class="pb-1">Action: <strong class="text-black">' . $logs->action . '</strong></li>
                <li>Table: <strong class="text-black">' . $logs->table . '</strong></li>
            </ul>';

$dataContent = Logs::showDetailHTML($logs->data, 'Données');

if ($logs->action === 'UPDATE') {
    $newData = $app->find($logs->table, null, '*', ['id' => $logs->id_item]);

    if($newData) {
        $dataContent .= Logs::showDetailHTML($newData, 'Nouvelles données');
    }
    
}

$content .= $dataContent;
echo Text::content(['database', $pageTitle, $content, 'none; height: 95%'], [['logs/database', 'logs'], $pageTitle]);
