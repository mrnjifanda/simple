<?php

    use App\Html\Text;
    use App\Logs\Logs;

    $pageTitle = 'Logs | Base de données';
    echo Text::titleH1('database', $pageTitle, null, [['#', 'Logs'], 'Base de données']);
?>

<div class="content-page js-scroll">

    <div class="row">
        <div class="col-md-3 col-xs-12">
            <ol class="list-style list-rectangle">
                <?php foreach ($years as $year) : 
                    
                    $months = Logs::getMontsYears($year);
                ?>
                    <li class="">
                        <a href="#<?= $year; ?>" class="js-slide"><?= $year; ?></a>
                        <ol class="list-style list-rectangle" style="display: none;">
                            <?php foreach ($months as $month):
    
                                $days = Logs::getDayMonts($month);
                            ?>
                                <li>
                                    <a href="#" class="js-slide"><?= $month; ?></a>
                                    <ol class="list-style list-rectangle" style="display: none;">
                                        <?php foreach ($days as $day): ?>
                                            <li><a href="<?= $routes('database-day', ['day' => str_remove($day, '.txt')]) ?>"  class="js-app-link" data-type="show-page" data-title="Base de données" data-container=".content-log-page"><?= str_remove($day, '.txt'); ?></a></li>
                                        <?php endforeach; ?>
                                    </ol>
                                </li>
                            <?php endforeach; ?>
                        </ol>
                    </li>
                <?php endforeach; ?>
            </ol>
        </div>

        <div class="col-md-9 col-xs-12 plr-0">
            <div class="content-log-page">
                <?= $content; ?>
            </div>
        </div>

    </div>

</div>
