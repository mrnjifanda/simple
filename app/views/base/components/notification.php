<ul>
    <li class="header-li-icon">
        <a href="#" class="header-icon js-dropdown-link"><i class="fa fa-tasks"></i><span class="notifis-badge">4</span></a>

        <ul class="dropdown-content">
            <div class="dropdown-arrow"></div>
            <div class="dropdown-background"></div>
            <li><p class="dropdown-content-header">04 Informations Générales</p></li>

            <li>
                <a href="#" class="dropdowwn-link">
                    <span class="photo"><img alt="avatar" src="/images/avatar.jpg"></span>
                    <span class="subject">
                        <span class="from">Zac Snider</span>
                        <span class="time">A l'instant</span>
                    </span>
                    <span class="message">Lorem ipsum dolor sit amet ...</span>
                </a>
            </li>

            <li>
                <a href="index.html#" class="dropdowwn-link">
                    <span class="photo"><img alt="avatar" src="/images/avatar.jpg"></span>
                    <span class="subject">
                        <span class="from">Zac Snider</span>
                        <span class="time">30min</span>
                    </span>
                    <span class="message">Hi mate, how is everything?</span>
                </a>
            </li>

            <li>
                <a href="index.html#" class="dropdowwn-link">
                    <span class="photo"><img alt="avatar" src="/images/avatar.jpg"></span>
                    <span class="subject">
                        <span class="from">Zac Snider</span>
                        <span class="time">5h</span>
                    </span>
                    <span class="message">Hi mate, how is everything?</span>
                </a>
            </li>

            <li><a href="#" class="dropdowwn-link text-center">Voir tous</a></li>
        </ul>
    </li>

    <li class="header-li-icon">
        <a href="#" class="header-icon js-dropdown-link"><i class="fa fa-envelope-o"></i><span class="notifis-badge">4</span></a>

        <ul class="dropdown-content">
            <div class="dropdown-arrow"></div>
            <li><p class="dropdown-content-header">05 nouveaux messages</p></li>

            <li>
                <a href="index.html#" class="dropdowwn-link">
                    <span class="photo"><img alt="avatar" src="/images/avatar.jpg"></span>
                    <span class="subject">
                        <span class="from">Zac Snider</span>
                        <span class="time">A l'instant</span>
                    </span>
                    <span class="message">Hi mate, how is everything?</span>
                </a>
            </li>

            <li>
                <a href="index.html#" class="dropdowwn-link">
                    <span class="photo"><img alt="avatar" src="/images/avatar.jpg"></span>
                    <span class="subject">
                        <span class="from">Zac Snider</span>
                        <span class="time">30min</span>
                    </span>
                    <span class="message">Hi mate, how is everything?</span>
                </a>
            </li>

            <li>
                <a href="index.html#" class="dropdowwn-link">
                    <span class="photo"><img alt="avatar" src="/images/avatar.jpg"></span>
                    <span class="subject">
                        <span class="from">Zac Snider</span>
                        <span class="time">5h</span>
                    </span>
                    <span class="message">Hi mate, how is everything?</span>
                </a>
            </li>

            <li><a href="index.html#" class="dropdowwn-link text-center">Voir tous</a></li>
        </ul>
    </li>

    <li class="header-li-icon">
        <a href="#" class="header-icon js-dropdown-link"><i class="fa fa-bell-o"></i><span class="notifis-badge bg-danger">4</span></a>

        <ul class="dropdown-content dropdown-danger">
            <div class="dropdown-arrow"></div>
            <li><p class="dropdown-content-header">05 nouvelles notifications</p></li>

            <li>
                <a href="index.html#" class="dropdowwn-link">
                    <span class="photo"><img alt="avatar" src="/images/avatar.jpg"></span>
                    <span class="subject">
                        <span class="from">Zac Snider</span>
                        <span class="time">A l'instant</span>
                    </span>
                    <span class="message">Hi mate, how is everything?</span>
                </a>
            </li>

            <li>
                <a href="index.html#" class="dropdowwn-link">
                    <span class="photo"><img alt="avatar" src="/images/avatar.jpg"></span>
                    <span class="subject">
                        <span class="from">Zac Snider</span>
                        <span class="time">30min</span>
                    </span>
                    <span class="message">Hi mate, how is everything?</span>
                </a>
            </li>

            <li>
                <a href="index.html#" class="dropdowwn-link">
                    <span class="photo"><img alt="avatar" src="/images/avatar.jpg"></span>
                    <span class="subject">
                        <span class="from">Zac Snider</span>
                        <span class="time">5h</span>
                    </span>
                    <span class="message">Hi mate, how is everything?</span>
                </a>
            </li>

            <li><a href="index.html#" class="dropdowwn-link text-center">Voir tous</a></li>
        </ul>
    </li>
</ul>