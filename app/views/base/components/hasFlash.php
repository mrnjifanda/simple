<?php
    if ($sessions->hasFlashes()) {
        echo '<script> const getFlashes = ' . json_encode($sessions->getFlashes()) . ';</script>';
    }
