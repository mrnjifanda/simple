<?php

    $pathLangFile = BASE . 'lang' . DIRECTORY_SEPARATOR . getLang() . DIRECTORY_SEPARATOR;
    $site = require_once $pathLangFile . 'site.php';
    $auth = require_once $pathLangFile . 'auth.php';
    $mails = require_once $pathLangFile . 'mails' . DIRECTORY_SEPARATOR . 'mails.php';
    $validations = require_once $pathLangFile . 'validations' . DIRECTORY_SEPARATOR . 'validations.php';

    return [
        "site" => $site,
        "auth" => $auth,
        "mails" => $mails,
        "validations" => $validations
    ];
