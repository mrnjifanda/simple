<?php

    return [
        'slogan' => 'est le meilleur logiciel de gestion et de communication des établissements',
        'male' => 'male',
        'feminine' => 'feminine',
        'asterisk' => 'Les champs suivis d\'un astérisque <b>*</b> sont obligatoires',
        'default-format-date' => 'd M Y à H:i:s'
    ];