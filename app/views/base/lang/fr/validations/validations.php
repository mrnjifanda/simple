<?php

    return [
        'required' => 'Champ obligatoire.',
        'email' => 'Syntaxe de l\'email invalide.',
        'alpha' => 'Syntaxe invalide (Alphanumérique).',
        'date' => 'Syntaxe de la date invalide.',
        'phone' => 'Numéro de téléphone invalide',
        'numeric' => 'Uniquement des valeurs numériques (nombres).',
        'confirm' => 'Confirmation invalide',
        'uniq' => 'Cette valeur existe déja',
        'exist' => 'Cette valeur n\'existe pas',
        'min' => 'Taille invalide (minimum @value@)',
        'max' => 'Taille invalide (maximum @value@)',
        'format' => 'Syntaxe invalide (format @value@)'
    ];
