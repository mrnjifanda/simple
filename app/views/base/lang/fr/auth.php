<?php

    return [
        'login' => 'Connexion',
        'password' => 'Mot de passe',
        'remember' => 'Se souvenir de moi',
        'forgot-password' => 'Mot de passe oublié',
        'username' => 'Email ou nom d\'utilisateur'
    ];