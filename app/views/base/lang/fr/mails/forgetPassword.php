<?php

    $forgetPassword = [
        'subject' => 'Votre lien de réinitialisation de mot de passe easyTalk est prêt',
        'successSend' => 'Les instructions de réinitialisation ont été envoyées'
    ];