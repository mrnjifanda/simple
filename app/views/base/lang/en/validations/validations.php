<?php

    return [
        'required' => 'Required field.',
        'email' => 'Invalid email syntax.',
        'alpha' => 'Invalid syntax (Alphanumeric).',
        'date' => 'Invalid date syntax.',
        'phone' => 'Invalid phone number.',
        'numeric' => 'Only numeric values (numbers).',
        'confirm' => 'Confirmation invalid.',
        'uniq' => 'This value already exists.',
        'exist' => 'This value does not exist.',
        'min' => 'Invalid size (minimum @value@)',
        'max' => 'Invalid size (maximum @value@)',
        'format' => 'Invalid syntax (format @value@)'
    ];
