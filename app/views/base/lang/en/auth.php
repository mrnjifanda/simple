<?php

    return [
        'login' => 'Login',
        'password' => 'Password',
        'remember' => 'Remember me',
        'forgot-password' => 'Forgot your password',
        'username' => 'Email or username'
    ];