<?php

    return [
        'slogan' => 'is the best management and communication software for establishments',
        'male' => 'masculin',
        'feminine' => 'féminin',
        'asterisk' => 'Fields followed by an asterisk <b>*</b> are required',
        'default-format-date' => 'd M Y H:i:s'
    ];