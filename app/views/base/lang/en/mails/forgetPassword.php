<?php

    $forgetPassword = [
        'subject' => 'Your easyTalk password reset link is ready',
        'successSend' => 'Reset instructions have been sent'
    ];