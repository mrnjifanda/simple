<?php

    $childs = ['register', 'forgetPassword'];

    try {

        foreach ($childs as $child) {

            require_once BASE . 'lang' . DIRECTORY_SEPARATOR . getLang() . DIRECTORY_SEPARATOR . 'mails' . DIRECTORY_SEPARATOR . $child . '.php';
        }
    } catch (\Throwable $th) {
        
        //throw $th;
    }

    return compact('register', 'forgetPassword');
