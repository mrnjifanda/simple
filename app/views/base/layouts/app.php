<?php

    use App\Html\A;
    use Service\Nav;
    use App\Html\Tabs;
    use App\Html\Text;
    use App\Html\Assets;
    use App\Html\Components;

    $nav = new Nav();
?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="theme-color" content="#254f9c">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="description" content="<?= $pageDescription ?? ''; ?>">

    <?php

        Components::show('hasFlash', ['sessions' => $this->sessions]);
        echo
            Assets::link('/images/favicon.ico', ['shortcut icon' => 'rel', 'image/x-icon' => 'type']),
            Assets::link('/vendor/icons/font-awesome/css/font-awesome.min.css'),
            Assets::link('/vendor/icons/mdi/css/materialdesignicons.min.css'),
            Assets::manifest('/assets/app', $app->getIsDev()),
            $pageCss ?? '',
            $pageScript ?? '';
    ?>

    <title><?= $pageTitle ?? null; ?> | easyTalk</title>
</head>

<body id="js-body">
    <div class="loader-page">
        <div class="app-preloader js-loader-page">
            <div>
                <?= Assets::img('/loader/preloader.gif', 'App loader') ?>
                <p>Chargement</p>
            </div>
        </div>

        <div class="content-spinner-loader big-load">
            <?= Text::i('refresh fa-pulse') ?>
        </div>

        <div class="small-load js-small-load" style="--n: 5">
            <div class="dot" style="--i: 0"></div>
            <div class="dot" style="--i: 1"></div>
            <div class="dot" style="--i: 2"></div>
            <div class="dot" style="--i: 3"></div>
            <div class="dot" style="--i: 4"></div>
        </div>
    </div>

    <header class="header">
        <a href="#" class="logo">easy<span class="color-theme">Talk</span></a>
        <nav>
            <div>
                <div class="hamburger js-hamburger"><?= Text::i('bars'); ?></div>
                <?php Components::show('notification'); ?>
            </div>

            <ul>
                <li class="header-li-icon">
                    <a href="#" class="header-icon js-dropdown-link"><?= Text::i('gears'); ?></a>

                    <ul class="dropdown-content dropdown-setting dropdown-right width-100">
                        <div class="dropdown-arrow"></div>
                        <li>
                            <p class="dropdown-content-header">Parametres</p>
                        </li>
                        <li>
                            <div class="dropdowwn-link">
                                <h4 class="text-center"><?= Text::i('language'); ?> Langue</h4>
                                <div class="mt-1 flex justify-around">
                                    <a href="/changer-langue/fr"><img src="/images/fr.png" alt="" title="Français"></a>
                                    <a href="/changer-langue/en"><img src="/images/en.png" alt="" title="English"></a>
                                </div>
                            </div>
                        </li>
                        <li><?= A::a('account', Text::i('user') . ' Mon compte', 'modal', 'dropdowwn-link'); ?></li>
                        <li><?= A::a('configurations/application', Text::i('wrench') . ' Config', 'full-screen', 'dropdowwn-link'); ?></li>
                        <li><a href="#" id="app-full-screen" class="dropdowwn-link"><?= Text::i('fullscreen', null, true) . ' Plein écran'; ?></a></li>
                        <li class="flex text-center text-bold">
                            <?=
                                A::form($routes('logout'), Text::i('power-off'), 'dropdowwn-link bg-danger text-white'),
                                A::form($routes('lock'), Text::i('lock'), 'js-app-link dropdowwn-link', 'lock-screen')
                            ?>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>
    </header>

    <aside class="sidebar">
        <div class="account">
            <?= Assets::img('/users/default.png', 'App loader') ?>
            <div class="account-info text-white">
                <p>Adminstrateur</p>
                <a href="#" class=""><?= Text::i('circle text-theme') ?> En ligne</a>
            </div>
        </div>

        <form action="#" id="form-search" method="post">
            <div class="position-relative">
                <input type="search" class="input" name="search" id="search" ng-model="search" placeholder="Recherche ...">
                <button type="submit"><?= Text::i('search'); ?></i></button>
            </div>

            <div class="checkbox-search">
                <?=
                    $nav->FormCheckbox('Tous', 'all-search', 'tous', true),
                    $nav->searchBLog(),
                    $nav->FormCheckbox('Elèves', 'students-search"', 'eleves'),
                    $nav->FormCheckbox('Encadreurs', 'encadreurs-search"', 'encadreurs');
                ?>
            </div>
        </form>

        <p class="menu-principal">Menu Principal</p>

        <ul class="nav">
            <?php
                echo $nav->tBord();
                Components::show('asideBar', ['role' => $user->getRole(), 'nav' => $nav]);
                echo $nav->contacts();
            ?>
        </ul>
    </aside>

    <section class="content" id="js-show-nav">
        <?= $pageContent; ?>
    </section>

    <aside class="aside-bottom">
        <div class="controls-pages">
            <?=
                Text::i('folder js-ab-open'),
                Text::i('expand js-ab-full');
            ?>
        </div>

        <div class="ab-content plr-5px">
            <?= 
                Tabs::open(),
                    Tabs::tab('all-items-1', 'book', 'Tous les articles', true, null, true),
                    Tabs::tab('all-items-2', 'book', 'Tous les articles 2'),
                    Tabs::o_content(),
                    Tabs::content('all-items-1', '<h1>14 All Items 1</h1>', true),
                    Tabs::content('all-items-2', '<h1>14 All Items 2</h1>'),
                Tabs::close();
            ?>
        </div>
    </aside>

    <footer class="footer">
        <p>
            <strong><?= $user->getUsername() ?></strong> | <strong>2.0</strong> | <a href="http://">Conditions générales d'utilisation</a>
        </p>
        <?= Text::i('sort-up') ?>
    </footer>

    <!-- <script src="http://localhost:3000/socket.io/socket.io.js"></script> -->
    <!-- <script>
        var socket = io.connect('http://localhost:8000');
        var socket = io();
        socket.emit('chat message', 'input.value');
    </script> -->
</body>
</html>
