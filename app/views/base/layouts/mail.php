<div data-template-type="html" style="height: auto; padding-bottom: 149px;" class="ui-sortable">
    <!--[if !mso]><!-->
    <!--<![endif]-->
    <!-- ====== Module : Intro ====== -->
    <table bgcolor="#F5F5F5" align="center" class="full" border="0" cellpadding="0" cellspacing="0" data-thumbnail="http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2020/03/22/9jhlVUzudcB8NtbnMg67SvA5/StampReady/thumbnails/thumb-1.png" data-module="Module-1" data-bgcolor="M1 Bgcolor 1">
        <tbody>
            <tr>
                <td>
                    <table bgcolor="#304050" align="center" width="750" class="margin-full ui-resizable" style="background-size: cover; background-position: center center; border-radius: 6px 6px 0px 0px; background-image: url(&quot;http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2020/03/22/9jhlVUzudcB8NtbnMg67SvA5/StampReady/img/module01-bg01.png&quot;);" border="0" cellpadding="0" cellspacing="0" background="http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2020/03/22/9jhlVUzudcB8NtbnMg67SvA5/StampReady/img/module01-bg01.png" data-bgcolor="M1 Bgcolor 2" data-background="M1 Background 1">
                        <tbody>
                            <tr>
                                <td>
                                    <table width="600" align="center" class="margin-pad" border="0" cellpadding="0" cellspacing="0">
                                        <tbody>
                                            <tr>
                                                <td height="70" style="font-size:0px">&nbsp;</td>
                                            </tr>
                                            <!-- image -->
                                            <tr>
                                                <td>
                                                    <table align="center" class="res-full" border="0" cellpadding="0" cellspacing="0">
                                                        <tbody>
                                                            <tr>
                                                                <td>
                                                                    <table align="center" border="0" cellpadding="0" cellspacing="0">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td>
                                                                                    <img width="65" style="max-width: 65px; width: 100%; display: block; line-height: 0px; font-size: 0px; border: 0px;" src="http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2020/03/22/9jhlVUzudcB8NtbnMg67SvA5/StampReady/img/module01-img02.png">
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <!-- image end -->
                                            <tr>
                                                <td height="45" style="font-size:0px">&nbsp;</td>
                                            </tr>
                                            <!-- title -->
                                            <tr>
                                                <td class="res-center" style="text-align: center; color: white; font-family: 'Raleway', Arial, Sans-serif; font-size: 26px; letter-spacing: 1.5px; word-break: break-word; font-weight: 300; padding-left: 1.5px;" data-color="M1 Title 1" data-size="M1 Title 1" data-max="36" data-min="16">
                                                    INFORMATIONS EN TEMPS RÉEL
                                                </td>
                                            </tr>
                                            <!-- title end -->
                                            <tr>
                                                <td height="12" style="font-size:0px">&nbsp;</td>
                                            </tr>
                                            <!-- title -->
                                            <tr>
                                                <td class="res-center" style="text-align: center; color: white; font-family: 'Raleway', Arial, Sans-serif; font-size: 35px; letter-spacing: 3px; word-break: break-word; font-weight: 800; padding-left: 3px;" data-color="M1 Title 2" data-size="M1 Title 2" data-max="45" data-min="25">
                                                    easyTalk
                                                </td>
                                            </tr>
                                            <!-- title end -->
                                            <tr>
                                                <td height="30" style="font-size:0px" class="">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table align="center" border="0" cellpadding="0" cellspacing="0">
                                                        <tbody>
                                                            <tr>
                                                                <td>
                                                                    <table align="center" border="0" cellpadding="0" cellspacing="0">
                                                                        <!-- link -->
                                                                        <tbody>
                                                                            <tr>
                                                                                <td class="res-center" style="text-align: center;">
                                                                                    <a href="https://example.com" style="color: white; font-family: 'Nunito', Arial, Sans-serif; font-size: 16px; letter-spacing: 0.7px; text-decoration: none; word-break: break-word;" data-color="M1 Link 2" data-size="M1 Link 2" data-max="26" data-min="6">
                                                                                        Support
                                                                                    </a>
                                                                                </td>
                                                                            </tr>
                                                                            <!-- link end -->
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                                <td style="padding: 0 12px; color: #798999;">
                                                                    •
                                                                </td>
                                                                <td>
                                                                    <table align="center" border="0" cellpadding="0" cellspacing="0">
                                                                        <!-- link -->
                                                                        <tbody>
                                                                            <tr>
                                                                                <td class="res-center" style="text-align: center;">
                                                                                    <a href="https://example.com" style="color: white; font-family: 'Nunito', Arial, Sans-serif; font-size: 16px; letter-spacing: 0.7px; text-decoration: none; word-break: break-word;" data-color="M1 Link 3" data-size="M1 Link 3" data-max="26" data-min="6">
                                                                                        Information
                                                                                    </a>
                                                                                </td>
                                                                            </tr>
                                                                            <!-- link end -->
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                                <td style="padding: 0 12px; color: #798999;">
                                                                    •
                                                                </td>
                                                                <td>
                                                                    <table align="center" border="0" cellpadding="0" cellspacing="0">
                                                                        <!-- link -->
                                                                        <tbody>
                                                                            <tr>
                                                                                <td class="res-center" style="text-align: center;">
                                                                                    <a href="https://example.com" style="color: white; font-family: 'Nunito', Arial, Sans-serif; font-size: 16px; letter-spacing: 0.7px; text-decoration: none; word-break: break-word;" data-color="M1 Link 4" data-size="M1 Link 4" data-max="26" data-min="6">
                                                                                        Communication
                                                                                    </a>
                                                                                </td>
                                                                            </tr>
                                                                            <!-- link end -->
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="70" style="font-size:0px">&nbsp;</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                        <div class="ui-resizable-handle ui-resizable-s" style="z-index: 90;"></div>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>

    <!-- ====== Module : Texts ====== -->
    <table bgcolor="#F5F5F5" align="center" class="full selected-table" border="0" cellpadding="0" cellspacing="0" data-thumbnail="http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2020/03/22/9jhlVUzudcB8NtbnMg67SvA5/StampReady/thumbnails/thumb-2.png" data-module="Module-2" data-bgcolor="M2 Bgcolor 1">
        <tbody>
            <tr>
                <td>
                    <table bgcolor="white" width="750" align="center" class="margin-full ui-resizable" border="0" cellpadding="0" cellspacing="0" data-bgcolor="M2 Bgcolor 2">
                        <tbody>
                            <tr>
                                <td>
                                    <table width="600" align="center" style="padding: 0 20px" class="margin-pad" border="0" cellpadding="0" cellspacing="0">
                                        <tbody>
                                            <tr>
                                                <td height="70" style="font-size:0px">&nbsp;</td>
                                            </tr>
                                            <!-- HEADING -->
                                            <!-- subtitle -->
                                            <tr>
                                                <td class="res-center" style="text-align: center; color: #506070; font-family: 'Raleway', Arial, Sans-serif; font-size: 14px; letter-spacing: 1px; word-break: break-word; font-weight: 800;" data-color="M2 Subtitle 1" data-size="M2 Subtitle 1" data-max="24" data-min="5">
                                                    <?= $mail_title ?? 'easyTalk' ?>
                                                </td>
                                            </tr>
                                            <!-- subtitle end -->
                                            <tr>
                                                <td height="13" style="font-size:0px">&nbsp;</td>
                                            </tr>
                                            <!-- title -->
                                            <tr>
                                                <td class="res-center selected-element" style="text-align: center; color: #405060; font-family: 'Raleway', Arial, Sans-serif; font-size: 22px; letter-spacing: 0.7px; word-break: break-word" data-color="M2 Title 1" data-size="M2 Title 1" data-max="32" data-min="12" contenteditable="true">
                                                    <?= $mail_description ?? 'Plateforme de gestion et de communication' ?>
                                                </td>
                                            </tr>
                                            <!-- title end -->
                                            <tr>
                                                <td height="15" style="font-size:0px">&nbsp;</td>
                                            </tr>
                                            <!-- image -->
                                            <tr>
                                                <td>
                                                    <table align="center" class="res-full" border="0" cellpadding="0" cellspacing="0">
                                                        <tbody>
                                                            <tr>
                                                                <td>
                                                                    <table align="center" border="0" cellpadding="0" cellspacing="0">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td>
                                                                                    <img width="130" style="max-width: 130px; width: 100%; display: block; line-height: 0px; font-size: 0px; border: 0px;" src="http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2020/03/22/9jhlVUzudcB8NtbnMg67SvA5/StampReady/img/ui-line-2.png">
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <!-- image end -->
                                            <!-- HEADING end -->
                                            <tr>
                                                <td height="30" style="font-size:0px">&nbsp;</td>
                                            </tr>
                                            <!-- paragraph -->
                                            <tr>
                                                <td class="res-center" style="text-align: center; color: #607080; font-family: 'Nunito', Arial, Sans-serif; font-size: 16px; letter-spacing: 0.4px; line-height: 23px; word-break: break-word" data-color="M2 Paragraph 1" data-size="M2 Paragraph 1" data-max="26" data-min="6">
                                                    <?= $content; ?>
                                                </td>
                                            </tr>
                                            <!-- paragraph end -->
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                        <div class="ui-resizable-handle ui-resizable-s" style="z-index: 90;"></div>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>

    <!-- ====== Module : Footer ====== -->
    <table bgcolor="#F5F5F5" align="center" class="full" border="0" cellpadding="0" cellspacing="0" data-thumbnail="http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2020/03/22/9jhlVUzudcB8NtbnMg67SvA5/StampReady/thumbnails/thumb-20.png" data-module="Module-20" data-bgcolor="M20 Bgcolor 1">
        <tbody>
            <tr>
                <td>
                    <table style="border-radius: 0 0 6px 6px;" bgcolor="white" width="750" align="center" class="margin-full ui-resizable" border="0" cellpadding="0" cellspacing="0" data-bgcolor="M20 Bgcolor 2">
                        <tbody>
                            <tr>
                                <td>
                                    <table width="600" align="center"  style="padding: 0 20px"  class="margin-pad" border="0" cellpadding="0" cellspacing="0">
                                        <tbody>
                                            <tr>
                                                <td height="40" style="font-size:0px">&nbsp;</td>
                                            </tr>
                                            <!-- column x2 -->
                                            <tr>
                                                <td>
                                                    <table class="full" align="center" border="0" cellpadding="0" cellspacing="0">
                                                        <tbody>
                                                            <tr>
                                                                <td valign="top">
                                                                    <!-- left column -->
                                                                    <table width="290" align="left" class="res-full" border="0" cellpadding="0" cellspacing="0">
                                                                        <!-- title -->
                                                                        <tbody>
                                                                            <tr>
                                                                                <td class="res-center" style="text-align: left; color: #405060; font-family: 'Raleway', Arial, Sans-serif; font-size: 22px; letter-spacing: 0.7px; word-break: break-word" data-color="M20 Title 1" data-size="M20 Title 1" data-max="32" data-min="12">
                                                                                    Contacts
                                                                                </td>
                                                                            </tr>
                                                                            <!-- title end -->
                                                                            <tr>
                                                                                <td height="15" style="font-size:0px">&nbsp;</td>
                                                                            </tr>
                                                                            <!-- list -->
                                                                            <tr>
                                                                                <td>
                                                                                    <table align="left" class="res-full" border="0" cellpadding="0" cellspacing="0">
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <table align="center" style="border-radius: 0px;" border="0" cellpadding="0" cellspacing="0">
                                                                                                        <!-- list -->
                                                                                                        <tbody>
                                                                                                            <tr>
                                                                                                                <!-- image -->
                                                                                                                <td width="27">
                                                                                                                    <table align="left" class="res-full" border="0" cellpadding="0" cellspacing="0">
                                                                                                                        <tbody>
                                                                                                                            <tr>
                                                                                                                                <td>
                                                                                                                                    <table align="center" border="0" cellpadding="0" cellspacing="0">
                                                                                                                                        <tbody>
                                                                                                                                            <tr>
                                                                                                                                                <td>
                                                                                                                                                    <img width="16" style="max-width: 16px; width: 100%; display: block; line-height: 0px; font-size: 0px; border: 0px;" src="http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2020/03/22/9jhlVUzudcB8NtbnMg67SvA5/StampReady/img/module20-img01.png">
                                                                                                                                                </td>
                                                                                                                                            </tr>
                                                                                                                                        </tbody>
                                                                                                                                    </table>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </tbody>
                                                                                                                    </table>
                                                                                                                </td>
                                                                                                                <!-- image end -->
                                                                                                                <td>
                                                                                                                    <table class="full" align="center" border="0" cellpadding="0" cellspacing="0">
                                                                                                                        <!-- link -->
                                                                                                                        <tbody>
                                                                                                                            <tr>
                                                                                                                                <td class="res-left" style="text-align: left;">
                                                                                                                                    <a href="https://example.com" style="color: #607080; font-family: 'Nunito', Arial, Sans-serif; font-size: 16px; letter-spacing: 0.7px; text-decoration: none; word-break: break-word;" data-color="M20 Link 1" data-size="M20 Link 1" data-max="26" data-min="6">
                                                                                                                                        youtube.com pages
                                                                                                                                    </a>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <!-- link end -->
                                                                                                                        </tbody>
                                                                                                                    </table>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <!-- list end -->
                                                                                                            <tr>
                                                                                                                <td height="10" style="font-size:0px">&nbsp;</td>
                                                                                                            </tr>
                                                                                                            <!-- list -->
                                                                                                            <tr>
                                                                                                                <!-- image -->
                                                                                                                <td width="27">
                                                                                                                    <table align="left" class="res-full" border="0" cellpadding="0" cellspacing="0">
                                                                                                                        <tbody>
                                                                                                                            <tr>
                                                                                                                                <td>
                                                                                                                                    <table align="center" border="0" cellpadding="0" cellspacing="0">
                                                                                                                                        <tbody>
                                                                                                                                            <tr>
                                                                                                                                                <td>
                                                                                                                                                    <img width="16" style="max-width: 16px; width: 100%; display: block; line-height: 0px; font-size: 0px; border: 0px;" src="http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2020/03/22/9jhlVUzudcB8NtbnMg67SvA5/StampReady/img/module20-img02.png">
                                                                                                                                                </td>
                                                                                                                                            </tr>
                                                                                                                                        </tbody>
                                                                                                                                    </table>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </tbody>
                                                                                                                    </table>
                                                                                                                </td>
                                                                                                                <!-- image end -->
                                                                                                                <td>
                                                                                                                    <table class="full" align="center" border="0" cellpadding="0" cellspacing="0">
                                                                                                                        <!-- link -->
                                                                                                                        <tbody>
                                                                                                                            <tr>
                                                                                                                                <td class="res-left" style="text-align: left;">
                                                                                                                                    <a href="https://example.com" style="color: #607080; font-family: 'Nunito', Arial, Sans-serif; font-size: 16px; letter-spacing: 0.7px; text-decoration: none; word-break: break-word;" data-color="M20 Link 2" data-size="M20 Link 2" data-max="26" data-min="6">
                                                                                                                                        twitter.com twitts
                                                                                                                                    </a>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <!-- link end -->
                                                                                                                        </tbody>
                                                                                                                    </table>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <!-- list end -->
                                                                                                            <tr>
                                                                                                                <td height="10" style="font-size:0px">&nbsp;</td>
                                                                                                            </tr>
                                                                                                            <!-- list -->
                                                                                                            <tr>
                                                                                                                <!-- image -->
                                                                                                                <td width="27">
                                                                                                                    <table align="left" class="res-full" border="0" cellpadding="0" cellspacing="0">
                                                                                                                        <tbody>
                                                                                                                            <tr>
                                                                                                                                <td>
                                                                                                                                    <table align="center" border="0" cellpadding="0" cellspacing="0">
                                                                                                                                        <tbody>
                                                                                                                                            <tr>
                                                                                                                                                <td>
                                                                                                                                                    <img width="16" style="max-width: 16px; width: 100%; display: block; line-height: 0px; font-size: 0px; border: 0px;" src="http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2020/03/22/9jhlVUzudcB8NtbnMg67SvA5/StampReady/img/module20-img03.png">
                                                                                                                                                </td>
                                                                                                                                            </tr>
                                                                                                                                        </tbody>
                                                                                                                                    </table>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </tbody>
                                                                                                                    </table>
                                                                                                                </td>
                                                                                                                <!-- image end -->
                                                                                                                <td>
                                                                                                                    <table class="full" align="center" border="0" cellpadding="0" cellspacing="0">
                                                                                                                        <!-- link -->
                                                                                                                        <tbody>
                                                                                                                            <tr>
                                                                                                                                <td class="res-left" style="text-align: left;">
                                                                                                                                    <a href="https://example.com" style="color: #607080; font-family: 'Nunito', Arial, Sans-serif; font-size: 16px; letter-spacing: 0.7px; text-decoration: none; word-break: break-word;" data-color="M20 Link 3" data-size="M20 Link 3" data-max="26" data-min="6">
                                                                                                                                        youtube.com videos
                                                                                                                                    </a>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <!-- link end -->
                                                                                                                        </tbody>
                                                                                                                    </table>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <!-- list end -->
                                                                                                        </tbody>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <!-- list end -->
                                                                        </tbody>
                                                                    </table>
                                                                    <!-- left column end -->
                                                                    <!--[if (gte mso 9)|(IE)]>
                                                </td>
                                                <td>
                                                   <![endif]-->
                                                                    <table width="20" align="left" class="res-full" border="0" cellpadding="0" cellspacing="0">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td height="20" style="font-size:0px">&nbsp;</td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                    <!--[if (gte mso 9)|(IE)]>
                                                </td>
                                                <td valign="top">
                                                   <![endif]-->
                                                                    <!-- right column -->
                                                                    <table width="290" align="right" class="res-full" border="0" cellpadding="0" cellspacing="0">
                                                                        <!-- image -->
                                                                        <tbody>
                                                                            <tr>
                                                                                <td>
                                                                                    <table align="right" class="res-full" border="0" cellpadding="0" cellspacing="0">
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <table align="center" border="0" cellpadding="0" cellspacing="0">
                                                                                                        <tbody>
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                    <img width="60" style="max-width: 60px; width: 100%; display: block; line-height: 0px; font-size: 0px; border: 0px;" src="http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2020/03/22/9jhlVUzudcB8NtbnMg67SvA5/StampReady/img/module20-img04.png">
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </tbody>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <!-- image end -->
                                                                            <tr>
                                                                                <td height="20" style="font-size:0px">&nbsp;</td>
                                                                            </tr>
                                                                            <!-- paragraph -->
                                                                            <tr>
                                                                                <td class="res-center" style="text-align: right; color: #607080; font-family: 'Nunito', Arial, Sans-serif; font-size: 16px; letter-spacing: 0.4px; line-height: 23px; word-break: break-word" data-color="M20 Paragraph 1" data-size="M20 Paragraph 1" data-max="26" data-min="6">
                                                                                    All Rights Reserved @ Company
                                                                                </td>
                                                                            </tr>
                                                                            <!-- paragraph end -->
                                                                            <tr>
                                                                                <td height="4" style="font-size:0px">&nbsp;</td>
                                                                            </tr>
                                                                            <!-- link -->
                                                                            <tr>
                                                                                <td class="res-center" style="text-align: right;">
                                                                                    <a href="https://example.com" style="color: #00bb9d; font-family: 'Nunito', Arial, Sans-serif; font-size: 16px; letter-spacing: 0.7px; text-decoration: none; word-break: break-word;" data-color="M20 Link 4" data-size="M20 Link 4" data-max="26" data-min="6">
                                                                                        Read Terms &amp; Conditions
                                                                                    </a>
                                                                                </td>
                                                                            </tr>
                                                                            <!-- link end -->
                                                                        </tbody>
                                                                    </table>
                                                                    <!-- right column end -->
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <!-- column x2 end -->
                                            <tr>
                                                <td height="50" style="font-size:0px">&nbsp;</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                        <div class="ui-resizable-handle ui-resizable-s" style="z-index: 90;"></div>
                    </table>
                </td>
            </tr>
            <tr>
                <td height="35" style="font-size:0px">&nbsp;</td>
            </tr>
        </tbody>
    </table>
</div>
