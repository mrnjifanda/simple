<?php
    use App\Html\Assets;
    use App\Html\Components;
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <?php
        Components::show('hasFlash', ['sessions' => $this->sessions]);
        echo
            Assets::link('/images/favicon.ico', ['shortcut icon' => 'rel', 'image/x-icon' => 'type']),
            Assets::link('/vendor/icons/font-awesome/css/font-awesome.min.css'),
            Assets::manifest('/assets/auth/app', $app->getIsDev());
    ?>

    <title><?= $pageTitle ?? ''; ?> | easyTalk</title>
</head>

<body style="background-image: url('/images/login-bg.jpeg');">
    <aside class="from-jititech js-loader-page">
        <div class="fixed-center">
            <?= Assets::img('/logo/easytalk/logo-100.png', 'Logo easyTalk', ['Logo easyTalk' => 'alt']) ?>
            <div class="loader"><span>Loading...</span></div>
        </div>
        <div class="jifitech">
            <?= Assets::img('/logo/jifitech/jifitech-25.png', 'Logo Jifitech', ['Logo Jifitech' => 'alt']) ?>
            <p>Jifitech</p>
        </div>
    </aside>

    <section id="js-content-data">
        <?= $pageContent; ?>
    </section>
</body>
</html>
