<?php

namespace Model\Users;

use Model\Admin\Zones\RegionsModel;
use Model\Model;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema()
 */
class User extends Model
{

    protected $table = 'users';
    protected $models_attributes = ['region'];

    /**
     * $role
     * 
     * @OA\Property(property="role", type="string", description="User role")
     * @var string
     */
    public $role;

    /**
     * $identifiant
     * 
     * @OA\Property(property="identifiant", type="string", description="User Itentifiant")
     * @var string
     */
    public $identifiant;
    
    /**
     * $username
     * 
     * @OA\Property(property="username", type="string", nullable=true, description="User Username")
     * @var string|null
     */
    public $username;

    /**
     * $password
     * 
     * @OA\Property(property="password", type="string", nullable=true, description="User Password")
     * @var string|null
     */
    public $password;

    /**
     * $nom
     * 
     * @OA\Property(property="nom", type="string", description="User Last name")
     * @var string
     */
    public $nom;

    /**
     * $prenom
     * 
     * @OA\Property(property="prenom", type="string", description="User Firts name")
     * @var string
     */
    public $prenom;

    /**
     * $date_naissance
     * 
     * @OA\Property(property="date_naissance", type="string", description="User Date of Birth")
     * @var string
     */
    public $date_naissance;

    /**
     * $lieu_naissance
     * 
     * @OA\Property(property="lieu_naissance", type="string", description="User Place of Birth")
     * @var string
     */
    public $lieu_naissance;

    /**
     * $sexe
     * 
     * @OA\Property(property="sexe", type="string", description="User sex")
     * @var string
     */
    public $sexe;

    /**
     * $region_origine
     * 
     * @OA\Property(property="region_origine", type="int", description="Id of User region of origin")
     * @var int
     */
    public $region_origine;

    /**
     * $nationalite
     * 
     * @OA\Property(property="nationalite", type="int", description="Id of User nationality")
     * @var int
     */
    public $nationalite;

    /**
     * $pays_de_residense
     * 
     * @OA\Property(property="pays_de_residense", type="int", description="Id of User country of residence")
     * @var int
     */
    public $pays_de_residense;

    /**
     * $addresse
     * 
     * @OA\Property(property="addresse", type="string", description="User Address")
     * @var string
     */
    public $addresse;

    /**
     * $cni
     * 
     * @OA\Property(property="cni", type="string", description="User Identity card number")
     * @var string
     */
    public $cni;

    /**
     * $matricule
     * 
     * @OA\Property(property="matricule", type="string", description="User Matricule")
     * @var string
     */
    public $matricule;

    /**
     * $phone
     * 
     * @OA\Property(property="phone", type="string", description="User Phone")
     * @var string
     */
    public $phone;

    /**
     * $email
     * 
     * @OA\Property(property="email", type="string", description="User Email")
     * @var string
     */
    public $email;

    /**
     * $maladies
     * 
     * @OA\Property(property="maladies", type="string", description="All user illnesses. Diseases must be separated by commas (,)")
     * @var string
     */
    public $maladies;

    /**
     * $autres_informations
     * 
     * @OA\Property(property="autres_informations", type="string", description="All other user information. Informations must be separated by commas (,)")
     * @var string
     */
    public $autres_informations;

    /**
     * $status
     * 
     * @OA\Property(property="status", type="string", description="User status")
     * @var string
     */
    public $statut;

    /**
     * $lasted_at
     * 
     * @OA\Property(property="lasted_at", type="string", format="date-time", nullable=true, description="User Date of last login")
     * @var \DateTimeInterface|null
     */
    public $lasted_at;

    /**
     * $remember_token
     * 
     * @OA\Property(property="remember_token", type="string", nullable=true, description="User remember token")
     * @var string|null
     */
    public $remember_token;

    /**
     * $confirmed_at
     * 
     * @OA\Property(property="confirmed_at", type="string", format="date-time", nullable=true, description="Account confirmation date")
     * @var \DateTimeInterface|null
     */
    public $confirmed_at;

    /**
     * $confirmation_token
     * 
     * @OA\Property(property="confirmation_token", type="string", nullable=true, description="Account confirmation token")
     * @var string|null
     */
    public $confirmation_token;

    /**
     * $reset_token
     * 
     * @OA\Property(property="reset_token", type="string", nullable=true, description="Account reset token")
     * @var string|null
     */
    public $reset_token;

    /**
     * $reset_ad
     * 
     * @OA\Property(property="reset_ad", type="string", format="date-time", nullable=true, description="Account reset date")
     * @var \DateTimeInterface|null
     */
    public $reset_at;

    /**
     * $region
     * 
     * @OA\Property(property="region", type="object", ref="#/components/schemas/RegionsModel", nullable=true, description="Region user")
     * @var \RegionsModel|bool
     */
    public function getRegionAttribute()
    {
        return $this->hasOne(RegionsModel::class, 'region_origine');
    }

    /*********************************************************************************************************************
     * All Models Getters
     *********************************************************************************************************************/
    public function getNom(): string
    {
        return strtoupper($this->nom);
    }

    public function getPrenom(): string
    {
        return strtoupper($this->prenom);
    }

    public function getRole(): string
    {
        return $this->role;
    }

    public function getIdentifiant(): string
    {
        return $this->identifiant;
    }

    public function getUsername(): ?string
    {
        return $this->username ?? null;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function getResteToken(): ?string
    {
        return $this->reset_token;
    }

    public function getResetAt(): ?string
    {
        return $this->reset_at;
    }
}
