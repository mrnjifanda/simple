<?php

namespace Model\API;

use Model\Model;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema()
 */
class BasicAuth extends Model
{

    protected $has_year = false;

    public function __construct()
    {

        parent::__construct();
    }

    /**
     * $api_username
     * 
     * @OA\Property(property="api_username", type="string", description="Username API Key")
     * @var string
     */
    public $api_username;

    /**
     * $api_password
     * 
     * @OA\Property(property="api_password", type="string", description="Password API Secret")
     * @var string
     */
    public $api_password;

    /**
     * $api_role
     * 
     * @OA\Property(property="api_role", type="string", description="API Role")
     * @var string|null
     */
    public $api_role;

    /**
     * $status
     * 
     * @OA\Property(property="status", type="string", description="Status API access")
     * @var string|null
     */
    public $status;
}
