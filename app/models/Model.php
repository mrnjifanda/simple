<?php

namespace Model;

use App\Helpers\Date;
use Database\Table;
use Traits\ModelTrait;
use OpenApi\Annotations as OA;

class Model extends Table
{

    use ModelTrait;

    private $defaultFormatDate = 'site.default-format-date';
    protected $models_attributes = [];

    public function __construct()
    {

        $this->setAllAttributes();
    }

    /**
     * $id
     * 
     * @OA\Property(property="id", type="int", description="User id")
     * @var int
     */
    public $id;

    /**
     * supprimer
     *
     * Permet de savoir si un element est supprimer dans la base de donnees
     * @var null|int
     */
    public $supprimer;
        
    /**
     * updated_at
     *
     * Date de dernier mise a jour de l'element
     * @var null|string
     */
    public $updated_at;

    /**
     * created_at
     *
     * Date de creation d'element
     * @var string
     */
    public $created_at;

    /**
     * name
     * 
     * @var null|string
     */
    public $name;
    
    /**
     * slug
     *
     * @var null|string
     */
    public $slug;

    /**
     * year
     *
     * Annee scollaire dans laquelle l'element a ete cree
     * @var int
     */
    public $year;

    /**
     * hasValue
     * 
     * Cette methode permet de verifie si une propriete n'est pas vide
     * 
     * @param  string $proprety
     * @return string|int|null
     */
    public function hasValue (string $proprety)
    {

        return $this->$proprety ?? null;
    }
    
    /**
     * getProprety
     * 
     * Recupere le contenu d'une propriete
     * 
     * @param  string $proprety
     * @return string|null
     */
    public function getProprety(string $proprety): ?string
    {

        return $this->hasValue($proprety);
    }
    
    /**
     * getId
     *
     * @return int
     */
    public function getId(): int
    {

        return $this->id;
    }
    
    /**
     * getName
     *
     * @return string
     */
    public function getName(): string
    {

        return ucfirst($this->name);
    }
    
    /**
     * getSlug
     *
     * @return string
     */
    public function getSlug(): string
    {

        return $this->slug;
    }
    
    /**
     * getDelete
     *
     * @return null|int
     */
    public function getDelete(): ?int
    {

        return $this->hasValue('supprimer');
    }
    
    /**
     * getUpdated
     *
     * @return null|string
     */
    public function getUpdated(): ?string
    {

        return $this->hasValue('updated_at');
    }
    
    /**
     * getCreated
     *
     * @return null|string
     */
    public function getCreated(): ?string
    {

        return $this->hasValue('created_at');
    }
        
    /**
     * getYear
     *
     * @return null|int
     */
    public function getYear(): ?int
    {

        return $this->hasValue('year');
    }

    /**
     * getDate
     * 
     * Afficher la date de creation au format choisi
     * 
     * @param  string|null $format Format avec lequel on veut afficher la date
     * @return string|null
     */
    public function getDate(?string $format = null): string
    {

        return $this->hasValue('created_at') ? Date::format($this->getCreated(), __($format ?? $this->defaultFormatDate)) : null;
    }
    
    /**
     * getUpdate
     * 
     * Afficher la date de modification au format choisi
     * @param  string|null $format Format avec lequel on veut afficher la date
     * @return string|null
     */
    public function getUpdate(?string $format = null): ?string
    {

        return $this->hasValue('updated_at') ? Date::format($this->getUpdated(), __($format ?? $this->defaultFormatDate)) : null;
    }
}
