<?php
namespace App\Html;

    class Configurations {

        public static function addItems (
            string $action,
            ?string $title = 'Nouvelle année scolaire',
            ?string $label = 'Entrez la nouvelle année scolaire'): string
        {
            $exemples = 'Exemple : 2019 - 2020';
            $class = $action === 'years' ? '' : 'class="flex-row justify-around"';
            if ($action === 'classe') {
                $exemples = 'Tle C';
                $second = '<div>
                    <div>
                        <label>Scolarité de la classe :<span class="color-red">*</span></label>
                        <input type="number" class="form-control mb-10" placeholder="90000" name="add-cout">
                    </div>
                </div>';
            } else if ($action === 'module') {
                $exemples = '1ere Sequence';
                $second ='<div>
                    <div>
                        <label>Période du module :<span class="color-red">*</span></label>
                        <input type="text" class="form-control mb-10" placeholder="du 03 Sept au 21 Oct" name="add-cout">
                    </div>
                </div>';
            } else if ($action === 'matiere') {
                $exemples = 'Anglais';
                $second ='<div>
                    <div>
                        <label>Groupe :<span class="color-red">*</span></label>
                        <select class="form-control mb-10">
                            <option value="Scientifique">Scientifique</option>
                            <option value="Littéraire">Littéraire</option>
                            <option value="Autre">Autre</option>
                        </select>
                    </div>
                </div>';
            } else {
                $second = '';
            }
           return <<<HTML
            <section class="section">
                <div class="title-section">
                    <h1>$title</h1>
                </div>

                <div>
                    <form action="$action" id="form-$action" method="POST">
                        <div $class>
                            <div>
                                <div>
                                    <label>$label :<span class="color-red">*</span></label>
                                    <input class="form-control mb-10" placeholder="$exemples" name="add-$action">
                                </div>
                            </div>
                            $second
                        </div>
                        <div class="text-center mb-10">
                            <button type="submit" class="submit">Ajouter</button>
                        </div>
                    </form>
                </div>
            </section>
HTML;
        }

        public static function editItems (
            string $action,
            ?string $title = 'Modifier une Classe',
            ?array $label = ['Nom de classe', 'Scolarité de la classe']): string
        {

           return <<<HTML
            <section class="section">
                <div class="title-section">
                    <h1>$title</h1>
                </div>

                <div>
                    <form action="$action" method="POST">
                        <div class="flex-row justify-around">
                            <div>
                                <label>$label[0] :<span class="color-red">*</span></label>
                                <input type="text" class="form-control mb-10" placeholder="Tle C" name="add-classe">
                            </div>
                            <div>
                                <label>$label[1] :<span class="color-red">*</span></label>
                                <input type="text" class="form-control mb-10" placeholder="90000" name="add-cout">
                            </div>
                        </div>
                        <div class="text-center mb-10">
                            <button type="submit" class="submit">Modifier</button>
                        </div>
                    </form>
                </div>
            </section>
HTML;
        }
    }