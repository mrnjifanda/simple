<?php
    namespace App\Html;

    class Components extends Html
    {
        public static function show(string $name, array $params = []): void
        {

            echo component(static::$components . $name . '.php', $params);
        }
    }
