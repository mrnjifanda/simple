<?php
namespace App\Html;

class Assets extends Html {
    
    /**
     * link
     *
     * @param  string $href
     * @param  array $attr
     * @return string
     */
    public static function link(string $href, array $attr = ['stylesheet' => 'rel']): string
    {
        return '<link' . self::attribute($attr) . ' href="' . $href . '">';
    }
    
    /**
     * css
     *
     * @param  string $href
     * @param  array $attr
     * @return string
     */
    public static function css (string $href, array $attr = []):  string
    {
        return self::link($href, array_merge(['stylesheet' => 'rel'], $attr));
    }
    
    /**
     * js
     *
     * @param  string $src
     * @param  array $attr
     * @return string
     */
    public static function js (string $src, array $attr = []):  string
    {
        return ' <script src="' . $src . '"' . self::attribute($attr) . ' defer></script>';
    }
    
    /**
     * img
     *
     * @param  string $src
     * @param  string $title
     * @param  array $attr
     * @return string
     */
    public static function img (string $src, string $title, array $attr = []):  string
    {
        return '<img src="/images' . $src . '" title="' . $title . '"' . self::attribute($attr) . '>';
    }

    /**
     * manifest
     * 
     * Get CSS and JS By Manifest
     * 
     * @param  string $filename
     * @param  bool $dev
     * @return null|string
     */
    public static function manifest(string $filename, bool $dev): ?string
    {
        $files = '';
        if ($dev) {

            if (is_file(parent::$public . $filename . '.css')) $files .= self::css($filename . '.css');
            if (is_file(parent::$public . $filename . '.js')) $files .= self::js($filename . '.js');
            return $files;
        }

        $props = explode('/', $filename);
        $file = array_pop($props);
        $path = parent::$public . DIRECTORY_SEPARATOR . implode('/', $props) . DIRECTORY_SEPARATOR . 'manifest.json';

        if (!is_file($path)) return null;

        $manifest = json_decode(file_get_contents($path));
        if (isset($manifest->{$file . '.css'})) $files .= self::css(str_replace('/public', '', $manifest->{$file . '.css'}));
        if (isset($manifest->{$file . '.js'})) $files .= self::js(str_replace('/public', '', $manifest->{$file . '.js'}));

        return $files;
    }
}
