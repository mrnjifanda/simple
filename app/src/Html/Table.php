<?php
namespace App\Html;

    class Table extends Html {

        public static function open (array $ths, ?string $class = null, ?string $filter = null): string
        {
            $th = '';
            foreach ($ths as $key => $value) {
                $th .= '<th class="table-color column' . $key . '" data-column="column' . $key . '">' . $value . '</th>';
            }

            $search = $filter ? '<div class="m-2"><input type="search" placeholder="Search..." class="input search-filter" data-table="' . $filter .'"/></div>' : null;

            return
            $search . 
            '<div class="p-1">
                <table' . ($class ? ' class="' . $class . '"' : null) . ($filter ? ' id="' . $filter . '"' : null) . ' >
                    <thead>
                        <tr>
                            <th class="table-number">N°</th>'
                            . $th .
                            '</tr>
                        </thead>
                        <tbody>';
        }

        public static function n (int $number): string
        {
            return '<td class="table-number">' . $number . '</td>';
        }

        public static function td (int $comumn, string $value): string
        {
            return '<td class="table-color column' . $comumn . '" data-column="column' . $comumn . '">' . $value . '</td>';
        }

        public static function tr (string ...$tds): string
        {

            $tr = '<tr>' . self::n(array_shift($tds));
            foreach ($tds as $key => $td) {

                $tr .= self::td($key, $td);
            }
            return $tr . '</tr>';
        }

        public static function empty(int $col, string $content = '...'): string
        {

            $tr = '<tr>' . self::n(0);
            for ($i=0; $i < $col; $i++) {
 
                $tr .= self::td($i, $content);
            }
            return $tr;
        }

        public static function close (): string
        {

            return '</tbody></table></div>';
        }
    }