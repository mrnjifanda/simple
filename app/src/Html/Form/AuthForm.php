<?php

namespace App\Html\Form;

class AuthForm extends Form
{

    public function __construct (array $data = [])
    {
        parent::__construct($data);
    }

    protected function template (string $content, string $name, ?string $label = null, ?bool $required = true, ?string $class = null, ?int $col = null, bool $icon = true): string
    {
        return '
            <div class="mb-2">
                <div class="input-group' . ($icon ? ' has-icon' : null) . '">' . $content . '</div>
                <div class="msgError"></div>
            </div>';
    }

    public function open (string $action, ?string $type = 'form', ?bool $obliger = true, ?string $json = null): string
    {
        return '
        <form action="' . $action . '" data-type="json" class="fixed-center submit-form" method="POST">
            <h2>' . $type . '</h2>
            <div class="submit-form-content">';
    }

    public function close (?string $name = 'Envoyer', ?string $icon = 'send', ?bool $reset = false): string
    {
        return '
                <button type="submit">' . $name . '</button><hr>
                <p><strong>easyTalk</strong> ' . __('site.slogan') . '.</p>
            </div>
        </form>';
    }

    public function checkbox (string $label, ?bool $required = null, ?string $url = null, ?string $text = null): string
    {
        return '
        <div class="forget-password">
            <label for="remember">
                <input type="checkbox" name="remember" id="remember"' . $this->required($required) .'> ' . $label . '
            </label>
            ' . ($url ? '<a href="' . $url . '" class="js-app-link">' . $text . '</a>' : null) .'
        </div>';
    }
}
