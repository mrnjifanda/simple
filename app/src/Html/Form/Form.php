<?php

    namespace App\Html\Form;

use App\Html\Html;
use App\Html\Text;

class Form extends Html {

    protected $data;
    protected $icons = [
        'user' => '<svg width="18" height="18" viewBox="0 0 18 18" xmlns="http://www.w3.org/2000/svg" role="icon" aria-hidden="true"><g fill-rule="evenodd"><path d="M13.689 11.132c1.155 1.222 1.953 2.879 2.183 4.748a1.007 1.007 0 01-1 1.12H3.007a1.005 1.005 0 01-1-1.12c.23-1.87 1.028-3.526 2.183-4.748.247.228.505.442.782.633-1.038 1.069-1.765 2.55-1.972 4.237L14.872 16c-.204-1.686-.93-3.166-1.966-4.235a7.01 7.01 0 00.783-.633zM8.939 1c1.9 0 3 2 4.38 2.633a2.483 2.483 0 01-1.88.867c-.298 0-.579-.06-.844-.157A3.726 3.726 0 017.69 5.75c-1.395 0-3.75.25-3.245-1.903C5.94 3 6.952 1 8.94 1z"></path><path d="M8.94 2c2.205 0 4 1.794 4 4s-1.795 4-4 4c-2.207 0-4-1.794-4-4s1.793-4 4-4m0 9A5 5 0 108.937.999 5 5 0 008.94 11"></path></g></svg>',
        'lock' => '<svg width="16" height="16" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg" role="icon"><path d="M4 5v-.8C4 1.88 5.79 0 8 0s4 1.88 4 4.2V5h1.143c.473 0 .857.448.857 1v9c0 .552-.384 1-.857 1H2.857C2.384 16 2 15.552 2 15V6c0-.552.384-1 .857-1H4zM3 15h10V6H3v9zm5.998-3.706L9.5 12.5h-3l.502-1.206A1.644 1.644 0 016.5 10.1c0-.883.672-1.6 1.5-1.6s1.5.717 1.5 1.6c0 .475-.194.901-.502 1.194zM11 4.36C11 2.504 9.657 1 8 1S5 2.504 5 4.36V5h6v-.64z"></path></svg>',
        'show' => '<svg width="16" height="16" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg" focusable="false" role="img" aria-hidden="true"><path d="M15.98 7.873c.013.03.02.064.02.098v.06a.24.24 0 01-.02.097C15.952 8.188 13.291 14 8 14S.047 8.188.02 8.128A.24.24 0 010 8.03v-.059c0-.034.007-.068.02-.098C.048 7.813 2.709 2 8 2s7.953 5.813 7.98 5.873zm-1.37-.424a12.097 12.097 0 00-1.385-1.862C11.739 3.956 9.999 3 8 3c-2 0-3.74.956-5.225 2.587a12.098 12.098 0 00-1.701 2.414 12.095 12.095 0 001.7 2.413C4.26 12.043 6.002 13 8 13s3.74-.956 5.225-2.587A12.097 12.097 0 0014.926 8c-.08-.15-.189-.343-.315-.551zM8 4.75A3.253 3.253 0 0111.25 8 3.254 3.254 0 018 11.25 3.253 3.253 0 014.75 8 3.252 3.252 0 018 4.75zm0 1C6.76 5.75 5.75 6.76 5.75 8S6.76 10.25 8 10.25 10.25 9.24 10.25 8 9.24 5.75 8 5.75zm0 1.5a.75.75 0 100 1.5.75.75 0 000-1.5z"></path></svg>',
        'hide' => '<svg width="16" height="16" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg" focusable="false" role="img" aria-hidden="true"><path d="M5.318 13.47l.776-.776A6.04 6.04 0 008 13c1.999 0 3.74-.956 5.225-2.587A12.097 12.097 0 0014.926 8a12.097 12.097 0 00-1.701-2.413l-.011-.012.707-.708c1.359 1.476 2.045 2.976 2.058 3.006.014.03.021.064.021.098v.06a.24.24 0 01-.02.097C15.952 8.188 13.291 14 8 14a7.03 7.03 0 01-2.682-.53zM2.04 11.092C.707 9.629.034 8.158.02 8.128A.24.24 0 010 8.03v-.059c0-.034.007-.068.02-.098C.048 7.813 2.709 2 8 2c.962 0 1.837.192 2.625.507l-.78.78A6.039 6.039 0 008 3c-2 0-3.74.956-5.225 2.587a12.098 12.098 0 00-1.701 2.414 12.11 12.11 0 001.674 2.383l-.708.708zM8.362 4.77L7.255 5.877a2.262 2.262 0 00-1.378 1.378L4.77 8.362A3.252 3.252 0 018.362 4.77zm2.86 2.797a3.254 3.254 0 01-3.654 3.654l1.06-1.06a2.262 2.262 0 001.533-1.533l1.06-1.06zm-9.368 7.287a.5.5 0 01-.708-.708l13-13a.5.5 0 01.708.708l-13 13z"></path></svg>'
    ];

    public function __construct (array $data = [])
    {

        $this->data = $data;
    }

    protected function inputContent (string $name, string $type = 'text', ?string $placeholder = null, ?bool $required = true)
    {

        return '<input
            type="' . $type . '"
            name="' . $name . '"
            class="input ' . $name . '"'
            . self::attr($this->value($name), 'value')
            . self::attr($placeholder, 'placeholder')
            . $this->required($required) . '
        >';
    }

    protected function value (string $field, $value = null): ?string
    {

        return $value ?? $this->data[$field] ?? null;
    }

    protected function required (?bool $required): ?string
    {

        return $required ? ' required=""' : null;
    }

    protected function template (string $content, string $name, ?string $label = null, ?bool $required = true, ?string $class = null, ?int $col = null, bool $icon = false): string
    {

        return
        '<div class="mb-1 ' . $class . ($col ? ' col-lg-' . $col . ' col-md-' . $col . ' col-sm-12' : null) . '">
            <div class="input-group' . ($icon ? '  has-icon' : null) . '">
                <label class="label" for="' . $name . '">' . $label . ' :' . ($required ? '<i class="text-danger">*</i>' : null) . '</label>
                ' . $content . '
            </div>
            <div class="msgError"></div>
        </div>';
    }

    public function open (string $action, ?string $type = 'form', ?bool $obliger = true, ?string $json = null): string
    {

        return
            '<form
                action="' . $action . '"
                class="easyTalk-form js-app-form"' .
                self::dataset($type, 'type') .
                self::dataset($json, 'json') . '
                method="POST">' .
                ($obliger ? '<div class="text-danger ml-2 mt-2 mb-1"><i>' . __('site.asterisk') . '</i></div>' : null);
    }

    public function close (?string $name = 'Envoyer', ?string $icon = 'send', ?bool $reset = false): string
    {

        return '
            <div class="text-center mt-1">
            ' . ($reset ? '<button type="reset" class="btn btn-warning mr-2">Réinitialisé</button>' : null) . '
                <button type="submit" class="btn btn-sm btn-primary">' . Text::i($icon) . ' ' . $name . '</button>
            </div>
        </form>';
    }

    public function hidden (string $name, ?string $value = null, bool $required = true): string
    {

        return '<input
            type="hidden"
            name="' . $name . '"'
            . self::attr($this->value($name, $value), 'value')
            . $this->required($required) . '>';
    }

    public function input (string $name, ?string $label = null, string $type = 'text', ?string $placeholder = null, ?bool $required = true, ?int $col = null): string
    {

        $content = '<input
            type="' . $type . '"
            name="' . $name . '"
            class="input ' . $name . '"'
            . self::attr($this->value($name), 'value')
            . self::attr($placeholder, 'placeholder')
            . $this->required($required) . '
        >';
        return $this->template($content, $name, $label, $required, null, $col);
    }

    public function sex (string $name, ?int $col = null): string
    {

        $sexes = ['M' => __('site.male'), 'F' => __('site.feminine')];
        $content = 
        '<select
            name="' . $name . '"
            class="input ' . $name . '"' .
            $this->required(true) . '>'
        . '<option> -- Sexe -- </option>';

        foreach ($sexes as $key => $sexe) {
            $content .= '<option value="' . $key . '">' . $sexe . '</option>';
        }

        return $this->template($content . '</select>', $name, 'Sexe', true, null, $col);
    }

    public function username (string $name, string $placeholder, string $type = 'text', ?string $label = null, ?bool $required = true, ?int $col = null)
    {
        $content = $this->inputContent($name, $type, $placeholder, $required) . '<div class="input-icon">' . $this->icons['user'] . '</div>';
        return $this->template($content, $name, $label, $required, null, $col);
    }

    public function password (string $name, string $placeholder, ?string $label = null, ?bool $required = true, ?int $col = null)
    {
        $content =
            $this->inputContent($name, 'password', $placeholder, $required) . '<div class="input-icon">' . $this->icons['lock'] . '</div><a class="show-hide-password">' . $this->icons['show'] . '</a>';
        return $this->template($content, $name, $label, $required, null, $col);
    }

    public function arraySelect (string $label, string $name, $objects = null, ?string $class = null, ?string $target = null, ?string $url = null, ?bool $required = true, ?int $col = null): ?string
    {
        $content =
            '<select
                name="' . $name . '"
                class="input ' . $name . '"' .
                $this->required($required) .
                self::dataset($url, 'url') .
                self::dataset($target, 'target') . '>'
            . '<option> -- ' . $label . ' -- </option>';

        if ($objects) {
            foreach ($objects as $object) {
                $content .= '<option value="' . $object->getId() . '"' . ($this->value($name) && $this->value($name) == $object->getId() ? ' selected' : null) . '>' . $object->getName() . '</option>';
            }
        }
        return $this->template($content . '</select>', $name, $label, $required, $class, $col);
    }

    public function o_group (?string $label = null, ?string $icon = 'null'): string
    {
        return
        '<fieldset form="">
            <legend>' . ($icon ? Text::i($icon) . ' ' : null) . $label . '</legend>'; 
    }

    public function c_group (?string $label = null): string
    {
        return '</fieldset>';
    }

    public function textarea (string $name, ?string $label = null, ?string $placeholder = null, ?int $rows = 5, ?bool $required = true, ?int $col = null): string
    {
        $content = '<textarea
            name="' .$name . '"
            class="input ' .$name . '"
            id="' . $name . '"
            rows="' . $rows . '" ' . 
            self::attr($placeholder, 'placeholder') .
            $this->required($required) . '
            style="height: auto">' . $this->value($name) . '</textarea>';
        return $this->template($content, $name, $label, $required, null, $col);      
    }

    public function dropdown (string $label, string $name,  string $url): ?string
    {
        return
        '<div>
            <div class="mb-1">
                <label for="" class="label">' . $label . ' :<i class="text-danger">*</i></label>
                <div class="dropdown-smal">
                    <a href="#" class="btn btn-default btn-sm js-dropdown-link">Choisir <i class="fa fa-angle-down"></i></a>

                    <ul class="dropdown-menu js-stop-propagation plr-1" style="padding: 5px; left: 0px; top: 30px;">
                        <li class="flex justify-between input-filter">
                            <input type="search" class="input js-keyup"><span class="">0</span>
                        </li>

                        <li class="p-1"><label><input type="checkbox" name=""> S.V.T</label></li>
                        <li class="p-1"><label><input type="checkbox" name=""> S.V.T</label></li>
                        <li class="p-1"><label><input type="checkbox" name=""> S.V.T</label></li>
                    </ul>
                </div>
            </div>
        </div>';
    }
}
