<?php
namespace App\Html;

class Text extends Html {

    public static function excerpt (string $content, int $limit = 60)
    {
        if (mb_strlen($content) <= $limit) {
            return $content;
        }
        $lastSpace = mb_strpos($content, ' ', $limit);
        return mb_substr($content, 0, $lastSpace) . '...';
    }

    public static function random (int $lenght): string
    {
        $alphabet = "0123456789azertyuiopqsdfghjklmwxcvbnAZERTYUIOPQSDFGHJKLMWXCVBN";
        return substr(str_shuffle(str_repeat($alphabet, $lenght)), 0, $lenght);
    }

    public static function i (string $icon, ?int $size = null, ?bool $mdi = null): string
    {
        if ($mdi) {
            return '<i class="mdi mdi-' . ($size ? $icon . ' mdi-' . $size . 'x' : $icon) . '"></i> ';
        }
        return '<i class="fa fa-' . ($size ? $icon . ' fa-' . $size . 'x' : $icon) . '" aria-hiddent="true"></i> ';
    }

    public static function mdi (string $icon, ?int $size = null): string
    {
        return self::i($icon, $size, true);
    }


    public static function message (string $message, ?string $type = null): string
    {
        return '<div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="message-box">
                    <div class="message-box-content message-box-' . $type .'">
                        <div class="message-box-icon" title="' . $type .'"><i></i></div>

                        <div class="message-box-text">
                            <p>' . $message .'</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>';
    }

    public static function ContactsBankPage (string $url, ?string $icon = null, ?string $message = 'Nouveau message'): string
    {
        return
        '<div class="flex-center-content">
            <a href="#" class="add-new-item text-white">'. $icon . '</a>
            <p class="mt-1">' . $message. '</p>
        </div>';
    }

    public static function titleH1 (string $icon, string $name, ?bool $full = null, ?array $lis = null): string
    {
        if ($full) {
            return '<div class="full-screen-header">
                <h1 class="content-title pl-2 js-full-screen-title">' . self::i($icon) . ' ' . $name . '</h1>
                <div class="full-screen-options">'
                    . self::i('minus-circle js-minus')
                    . self::i('times-circle js-times text-danger ml-1') . '
                </div>
            </div>';
        }

        if ($lis) {
            $show = '<li><a href="/dashboard">' . self::i('dashboard') . ' Accueil</a></li> ';
            foreach ($lis as $key => $li) {
                if (count($lis) == $key+1) {
                    $show .= '<li class="active">' . $li . '</li>';
                    break;
                }

                $show .= is_array($li) ? '<li><a href="/' . $li[0] . '"> ' . $li[1] . '</a></li> ' : '<li>' . $li . '</li> ';
            }
        }

        return '<div class="content-title">
            <h1>' . self::i($icon) . ' ' . $name . '</h1>
            ' . ($lis ? '<ol class="breadcrumb">' . $show . '</ol>' : null) . '
        </div>';
    }

    public static function modal (string $icon, string $title, string $content, ?string $big = null): string
    {
        return
        '<div class="easytalk-modal-item" role="modal">
            <div class="easytalk-modal-content"' . ($big ? ' style="max-width: ' . $big . ';"' : null) . '>
                ' . self::titleH1($icon, $title) . '
                <div>' . $content . '</div>
                <a href="#close-modal" class="easytalk-modal-close js-easytalk-modal-close">Fermer</a>
            </div>
        </div>';
    }

    public static function modal404(): ?string
    {
        $content = '<div class="errors">
            <img src="/images/404-icon.png" title="Erreur 404">
            <p class="text-white mtb-1" style="font-size: 11px">Une erreur est survenue</p>
            <p>PAGE INDISPONIBLE</p>
        </div>';
        return self::modal('exclamation-triangle', 'PAGE INDISPONIBLE', $content, '250px');
    }

    public static function content(array $content, array $navigation)
    {
        if (isAjax()) {
            $width = (count($content) === 4 ? ['width' => $content[3]] : []);
            $echoContent = array_merge(['icon' => $content[0], 'title' => $content[1], 'content' => $content[2]], $width);
            return json_encode($echoContent);
        }
        return self::titleH1($content[0], $content[1], null, $navigation) . divPage($content[2]);
    }
}