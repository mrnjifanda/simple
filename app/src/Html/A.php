<?php
namespace App\Html;

    class A extends Html {

        public static function form (string $action, string $label, ?string $class = null, ?string $type = null): string
        {
            return
            '<form
                class="btn-link-with-form"
                action="' . $action . '"
                method="post"' .
                self::dataset($type, 'type') .
            '>
                <button' . self::class($class) . '>' . $label . '</button>
            </form>';
        }

        public static function a (string $href, string $name, ?string $type = null, ?string $class = null, ?string $id = null): string
        {
            return '<a href="/' . $href . '" class="js-app-link ' . $class . '"' . ($type ? ' data-type="' . $type . '"' : null) . ($id ? ' data-id="' . $id . '"' : null) . '>' . $name . '</a>';
        }

        public static function noLink (string $label, string $title): string
        {
            return '<span class="btn btn-default btn-sm" title="' . $title . '">' . Text::i('question-circle') . $label . '</span>';
        }

        public static function aIcon (string $href, string $name, string $icon, ?string $class = null, ?string $data = null): string
        {
            return '<a href="/' . $href . '" class="link ' . $class . '" ' . $data . '>' . Text::i($icon . ' link-icon') .' ' . $name . '</a>';
        }

        public static function view (string $href, int $id, ?string $class = null): string
        {
            return self::a($href . '/view/' . $id, Text::i('eye'), 'modal', 'btn btn-xs btn-info mr-1" title="Voir le contenu');
        }

        public static function edit (string $href, int $id, ?string $class = null): string
        {
            return self::a($href . '/edit/' . $id, Text::i('pencil'), 'modal', 'btn btn-xs btn-warning mr-1" title="Modifier');
        }

        public static function delete (string $href, int $id, ?string $class = null): string
        {
            return '<a href="/' . $href  . '/delete/' . $id . '" data-href="/' . $href  . '/delete/' . $id . '" class="js-delete btn btn-xs btn-danger mr-1 ' . $class . '" data-id="' . $id . '" title="Supprimer">' . Text::i('trash') . '</a>';
        }
        
        /**
         * actions
         *
         * @param  string $name
         * @param  string $url
         * @param  int $id
         * @return string
         */
        public static function actions (string $name, string $url, int $id): string
        {
            return '
            <a href="#" class="js-slide" style="display: block">' . $name . '</a>
            <div class="actions-table" style="display: none">' . A::view($url, $id) . A::edit($url, $id) . A::delete($url, $id) . '<div>';
        }

        public static function signaler (string $href, ?string $class = null): string
        {
            return '<a href="/easyTalk/' . $href . '" class="btn btn-warning ' . $class . '">' . Text::i('fa-warning') . '</a>';
        }



        /**
         * Pages Controls
         */

        public static function minus (): string
        {
            return '<span class="link small js-dossier">' . Text::i('minus link-icon') . '</span>';
        }

        public static function dossier (): string
        {
            return '<span class="link small js-dossier">' . Text::i('folder link-icon') . Text::i('minus link-icon') . '</span>';
        }

        public static function uppage (string $href, ?string $class = null): string
        {
            return '<a href="' . $href . '" class="link ' . $class . '">' . Text::i('level-down link-icon') . '</a>';
        }







        



        

        public static function tabs (string $href, string $name, ?string $class = null): string
        {
            return '<a class="tabs-classe color-white ' . $class . '" data-href="easyTalk-' . $href . '">' . $name . '</a>';
        }
    }