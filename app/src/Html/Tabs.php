<?php
    namespace App\Html;


    class Tabs {

        public static function open (?string $class = null, ?string $transition = 'fade'): string
        {
            return '<div class="jifi-tabs-systems ' . $class . '" data-transition="' . $transition . '">
                        <ul class="jifi-tabs">';
        }

        public static function tab (string $id, string $icon, string $name, ?bool $active = null, ?string $url = null, ?bool $close = null): string
        {
            return
            '<li class="jifi-tabs-li' . ($active ? ' active' : null) . '">
                <a 
                    class="jifi-js-tabs' . ($close ? ' pr-40px' : null) . '" 
                    href="#' . $id . '"
                    ' . ($url ? ' data-page="/' . $url . '" data-load="' . ($active ? ' load' : null) . '"' : null) . '
                >' . Text::i($icon . ' link-icon') . ' ' . $name . '</a>
                ' . ($close ? Text::i('times') : null) . '
            </li>';
        }

        public static function o_content (?string $class = null): string
        {
            return '</ul>
                        <div class="jifi-tabs-content ' . $class . '">';
        }

        public static function content (string $id, $content = null, ?bool $active = false): string
        {
            return
            '<div class="jifi-tab-content' . ($active ? ' active' : null) . '" id="' . $id . '">
                ' . $content . '
            </div>';
        }

        public static function ajaxTabs (array $arrayTabs, $firtsContent): string
        {
            $firtTab = array_shift($arrayTabs);
            $tabs = self::tab($firtTab[0], $firtTab[1], $firtTab[2], true);
            $tabsContents = self::content($firtTab[0], $firtsContent, true);

            foreach ($arrayTabs as $tab) {
                $tabs .= self::tab($tab[0], $tab[1], $tab[2], false, $tab[3]);
                $tabsContents .= self::content($tab[0]);
            }

            return
            self::open() .
                $tabs .
                self::o_content() .
                $tabsContents .
            self::close();
        }

        public static function close (): string
        {
            return '</div>
                        </div>';
        }
    }