<?php
    namespace App\Html;

    class Html
    {
        protected static $public = ROOT . DIRECTORY_SEPARATOR . 'public';
        protected static $base = ROOT . 'app' . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'base' . DIRECTORY_SEPARATOR;
        protected static $components = ROOT . 'app' . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'base' . DIRECTORY_SEPARATOR . 'components' . DIRECTORY_SEPARATOR;
        
        /**
         * attribute
         * 
         * Permet d'ajouter plusieurs attribus a un element HMTL
         *
         * @param  array $attributes
         * @return string
         */
        protected static function attribute (array $attributes): ?string
        {
            if (empty($attributes)) return null;

            $string = '';
            foreach ($attributes as $attribute => $value) {
                $string .= self::attr($attribute, $value);
            }

            return $string;
        }
        
        /**
         * dataset
         *
         * Ajouter des data attributes
         * 
         * @param  null|string $data
         * @param  string $attr
         * @return string
         */
        protected static function dataset (?string $data = null, string $attr): ?string
        {
            return $data ? ' data-' . $attr . '="' . $data . '"' : null;
        }
        
        /**
         * class
         * 
         * Ajouter une class a un element HTML
         *
         * @param  null|string $class
         * @return string
         */
        protected static function class (?string $class = null): ?string
        {
            return $class ? ' class="' . $class . '"' : null;
        }
        
        /**
         * attr
         * 
         * Permet d'ajouter un attribu a un element HTML
         * 
         * @param  null|string $value
         * @param  string $attribu
         * @return null|string
         */
        protected static function attr (?string $value = null, string $attribu): ?string
        {

            $value = in_array($attribu, ['placeholder']) ? ucfirst($value) : $value;
            return $value ? ' ' . $attribu . '="' . $value . '"' : null;
        }
        
        /**
         * o_row
         * 
         * Permet de cree une nouvelle colone, toujour finir par c_row
         *
         * @param  array $attr
         * @return string
         */
        public static function o_row (array $attr = []): string
        {

            return '<div class="row"' . self::attribute($attr) . '>'; 
        }
        
        /**
         * c_row
         * 
         * Permet de fermet une nouvelle colone
         *
         * @return string
         */
        public static function c_row (): string
        {
            return '</div>';
        }
    }
