<?php

    namespace App\Queues;

    use PhpAmqpLib\Connection\AMQPStreamConnection;
    use PhpAmqpLib\Message\AMQPMessage;
    use Service\Queues\QueuesService;

    class Queues {

        private static $amqp = [];

        public static function getAmqp(?string $queue_name = null): array
        {

            $amqp = self::$amqp;
            if ($queue_name) {

                return isset($amqp[$queue_name]) ? $amqp[$queue_name] : [];
            }
            return self::$amqp;
        }

        public static function setAmqp($queue_name, $connection, $channel, $exchange): void
        {

            self::$amqp[$queue_name] = [
                'queue_name' => $queue_name,
                'connection' => $connection,
                'channel' => $channel,
                'exchange' => $exchange
            ];
        }

        public static function amqp(string $queue_name, string $exchange): array
        {

            $amqp = self::getAmqp($queue_name);
            if (empty($amqp)) {

                #Connexion
                $connection = new AMQPStreamConnection(ENV['RABBIT_HOST'], ENV['RABBIT_PORT'], ENV['RABBIT_USER'], ENV['RABBIT_PASSWORD'], ENV['RABBIT_VHOST']);
                $channel = $connection->channel();

                # Send message in queue
                $channel->queue_declare($queue_name, false, false, false, false);
                $channel->exchange_declare($exchange, 'direct', false, true, false);
                $channel->queue_bind($queue_name, $exchange);
                self::setAmqp($queue_name, $connection, $channel, $exchange);

                return self::getAmqp($queue_name);
            }

            return $amqp;
        }

        public static function push(array $datas, string $queue_name, string $exchange = 'subscribers')
        {

            $message = new AMQPMessage(json_encode($datas), [
                'content_type' => 'application/json',
                'delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT
            ]);

            extract(self::amqp($queue_name, $exchange));
            $channel->basic_publish($message, $exchange);

            # Close connexion
            $channel->close();
            $connection->close();
        }

        public static function consume(string $queue_name, string $exchange = 'subscribers')
        {

            extract(self::amqp($queue_name, $exchange));
            $consumerTag = 'local.imac.consumer';
            define('CURRENT_QUEUE_NAME', $queue_name);
            $channel->basic_consume($queue_name, $consumerTag, false, false, false, false, function (AMQPMessage $message)
            {

                QueuesService::run($message->body, CURRENT_QUEUE_NAME);
                $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);
                if ($message->body === 'quit') {
    
                    $message->delivery_info['channel']->basic_cancel($message->delivery_info['consumer_tag']);
                }
            });

            register_shutdown_function(function ($channel, $connection) {

                $channel->close();
                $connection->close();
            }, $channel, $connection);

            while (count($channel->callbacks)) {

                $channel->wait();
            }
        }
    }