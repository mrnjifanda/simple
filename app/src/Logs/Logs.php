<?php
namespace App\Logs;

use App\Helpers\Date;

class Logs
{
    private static $path = ROOT . 'logs' . DIRECTORY_SEPARATOR;
    private static $extension_file = '.txt';
    public static $type = 'databases';

    public static function getYears(): ?array
    {
        return getContentFolder(self::$path . static::$type);
    }

    public static function getMontsYears(string $year): ?array
    {
        return getContentFolder(self::$path . static::$type . DIRECTORY_SEPARATOR . $year);
    }

    public static function getDayMonts(string $monts): ?array
    {
        list($year) = explode('-', $monts);
        return getContentFolder(self::$path . static::$type . DIRECTORY_SEPARATOR . $year . DIRECTORY_SEPARATOR . $monts);
    }

    public static function getContentDay(string $fileDay): ?array
    {
        list($year, $monts, $day) = explode('-', $fileDay);
        $monts = $year . '-' . $monts;
        $pathFile = self::$path . static::$type . DIRECTORY_SEPARATOR .  $year . DIRECTORY_SEPARATOR . $monts . DIRECTORY_SEPARATOR . $fileDay . static::$extension_file;

        if (file_exists($pathFile)) {
            $contentFile = file_get_contents($pathFile);
            return explode("\n", $contentFile);
        }
        
        return null;
    }

    public static function getLastDay (): ?string
    {
        $years = self::getYears();
        if (empty($years)) {
            return null;
        }

        $months = self::getMontsYears($years[0]);
        if (empty($months)) {
            return null;
        }

        $days = self::getDayMonts($months[0]);
        if (empty($days)) {
            return null;
        }

        return str_remove($days[0], static::$extension_file);
    }

    public static function getDetailLog($day, $key): ?object
    {
        $dataDay = self::getContentDay($day);
        return $dataDay ? json_decode($dataDay[$key]) : null;
    }

    public static function arrayTitle (string $dayFile): string
    {
        list($year, $month, $day) = explode('-', $dayFile);
        return '<h4 class="pl-1 pt-1">' . $year . ' > ' . $month . ' > <strong class="text-theme">' . $dayFile . '</strong></h4>';
    }

    public static function showDetailHTML ($datas, string $titleData): string
    {
        $content= '<div class="logs-style"><h2 class="partie-title mt-2 mb-1">' . $titleData . '</h2><ul><pre>{';

        foreach ($datas as $key => $log) {
            if (is_numeric($key)) { continue; }
            $content .= "<li class='ml-2'>$key: <strong>$log</strong></li>";
        }

        return $content . '}</pre></ul><div>';
    }

    public static function getFileContent(): array
    {
        list($year, $month, $day) = explode('-', Date::toDay('Y-m-d'));

        $yearFolder = static::$path . static::$type . DIRECTORY_SEPARATOR . $year;
        $monthFolder = $yearFolder . DIRECTORY_SEPARATOR . $year . '-' . $month;
        $dayFile = $monthFolder . DIRECTORY_SEPARATOR . $year . '-' . $month . '-' . $day . static::$extension_file;

        if (!is_dir($yearFolder)) {
            mkdir($yearFolder);
        }

        if (!is_dir($monthFolder)) {
            mkdir($monthFolder);
        }

        $content = '';

        if (file_exists($dayFile)) {
            $content = file_get_contents($dayFile);
            $content .= "\n";
        }
        
        return ['file' => $dayFile, 'content' => $content];
    }

    public static function setFileLogs(array $content): void
    {
        $files = self::getFileContent();
        file_put_contents($files['file'], $files['content'] . json_encode($content));
    }
}