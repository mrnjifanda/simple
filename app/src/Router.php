<?php

namespace App;

use App\App;
use App\Users\Auth;
use App\Http\Request;
use App\Helpers\Cookies;
use App\Helpers\Sessions;
use Service\YearsService;
use Traits\PrefixTrait;

class Router
{
    /**
     * @var string
     */
    private $viewPath;

    /**
     * @var AltoRouter
     */
    private $router;

    /**
     * @var App
     */
    private $app;

    /**
     * @var string
     */
    private $uri;
    
    /**
     * redirectRoute
     *
     * @var array
     */
    private $redirectRoute = [];

    /**
     * @var this
     */
    public static $routes;

    use PrefixTrait;

    public function __construct(string $viewPath, App $app, string $uri)
    {

        $this->app = $app;
        $this->uri = $uri;
        $this->viewPath = $viewPath;
        $this->router = new \AltoRouter();
    }

    /**
     * hasLastslash
     *
     * @param  string $url
     * @return bool
     */
    private function hasLastslash (string $url): bool
    {
        
        return ($url !== '/' && $url[-1] === '/') ? true : false;
    }

    /**
     * removeLastslash
     *
     * @param  string $url
     * @return string
     */
    private function removeLastslash (string $url): string
    {
        
        return $this->hasLastslash($url) ? substr($url, 0, -1) : $url;
    }
    
    /**
     * get
     *
     * @param  string $url
     * @param  mixed $view
     * @param  string|null $name
     * @return self
     */
    public function get(string $url, $view, ?string $name = null): self
    {

        $this->router->map('GET', $this->urlPrefix($url), $view, $name);
        return $this;
    }
    
    /**
     * post
     *
     * @param  string $url
     * @param  mixed $view
     * @param  string|null $name
     * @return self
     */
    public function post(string $url, $view, ?string $name = null): self
    {

        $this->router->map('POST', $this->urlPrefix($url), $view, $name);
        return $this;
    }

    /**
     * delete
     * 
     * @param string $url
     * @param mixed $view
     * @param string|null $name
     * 
     * @return self
     */
    public function delete(string $url, $view, ?string $name = null): self
    {

        // return $this->post($url, $view, $name);
        $this->router->map('DELETE', $this->urlPrefix($url), $view, $name);
        return $this;
    }

    /**
     * patch
     * 
     * @param string $url
     * @param mixed $view
     * @param string|null $name
     * 
     * @return self
     */
    public function patch(string $url, $view, ?string $name = null): self
    {

        // return $this->post($url, $view, $name);
        $this->router->map('PATCH', $this->urlPrefix($url), $view, $name);
        return $this;
    }

    /**
     * put
     * 
     * @param string $url
     * @param mixed $view
     * @param string|null $name
     * 
     * @return self
     */
    public function put(string $url, $view, ?string $name = null): self
    {

        // return $this->post($url, $view, $name);
        $this->router->map('PUT', $this->urlPrefix($url), $view, $name);
        return $this;
    }

    /**
     * matchs
     *
     * @param  string $url
     * @param  mixed $view
     * @param  string|null $name
     * @return self
     */
    public function matchs(string $url, $view, ?string $name = null): self
    {
        $this->router->map('POST|GET|PATCH|PUT|DELETE', $this->urlPrefix($url), $view, $name);
        return $this;
    }

    /**
     * mix
     * Generation de l'url en post et get
     * @param  string $url
     * @param  mixed $view
     * @param  string|null $name
     * @return self
     */
    public function mix(string $url, $view, ?string $name = null): self
    {

        $this->get($url, $view . 'View', ($name ? $name . '-view' : null));
        $this->post($url, $view . 'Post', ($name ? $name . '-post' : null));
        $this->Put($url, $view . 'Put', ($name ? $name . '-put' : null));
        $this->delete($url, $view . 'Delete', ($name ? $name . '-delete' : null));
        return $this;
    }

    /**
     * redirect
     *
     * @param  string $from
     * @param  string $to
     * @param  int|null $code
     * @return self
     */
    public function redirect(string $from, string $to, ?int $code = 301): self
    {

        $this->redirectRoute[$from] = [
            'from' => $from,
            'to' => $to,
            'code' => $code
        ];
        return $this;
    }

    /**
     * url
     *
     * @param  string $name
     * @param  array $params
     * @return string
     */
    public function url(string $name, array $params = []): ?string
    {

        return $this->router->generate($name, $params);
    }

    /**
     * run
     *
     * @return self
     */
    public function run(): self
    {

        $router = $this;
        $match = $router->router->match();
        self::$routes = $router;
        $app = $router->app;

        $uri = $this->uri;
        $redirectRoute = $this->redirectRoute;
        if (!empty($redirectRoute) && array_key_exists($uri, $redirectRoute)) {
            
            $app->redirect($redirectRoute[$uri]['to']);
        }

        $request = Request::getInstance();
        if (is_array($match)) {

            $target = $match['target'];
            $params = $match['params'];

            $session = Sessions::getInstance();
            $cookies = Cookies::getInstance();
            $auth = new Auth();
            $auth->remember(); # Reconnect from Cookies
    
            if ($session->read('auth')) { # S'il y'a un utilisateur connecté on cree la variable globale YEAR.

                define('YEAR', $session->read('activeYear') ?? YearsService::getLastYear($app)->getId());
            }

            if (is_callable($target)) {

                $argc = array_values($params);
                call_user_func_array($target, $argc);
            } else if (strpos($match['target'], '#')) {

                list($controller, $method) = explode('#', $match['target']);
                if (is_callable([$controller, $method], true)) {
                    
                    $controller = new $controller();
                    $argc = array_values($params);
                    call_user_func_array([$controller, $method], $argc);
                }
            } else if (file_exists($this->viewPath . $target . '.php')) {

                if ($app->is_ajax()) {

                    require_once $this->viewPath . $target . '.php';
                } else {

                    ob_start();
                    require_once $this->viewPath . $target . '.php';
                    $pageContent = ob_get_clean();
                    require_once  $this->viewPath . 'views' .  DIRECTORY_SEPARATOR . 'default.php';
                }
            } else {

                $app->header_status(500);
                if (!$app->is_ajax()) {

                    require_once $this->viewPath . 'views' .  DIRECTORY_SEPARATOR . 'errors' . DIRECTORY_SEPARATOR . '500.php';
                }
            }
        } else {

            $app->header_status(404);
            if (!$app->is_ajax()) {

                require_once $this->viewPath . 'views' .  DIRECTORY_SEPARATOR . 'errors' . DIRECTORY_SEPARATOR . '404.php';
            }
        }

        return $this;
    }
}
