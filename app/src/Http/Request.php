<?php
namespace App\Http;

class Request {

    const INT = 1;
    const STRING = 2;

    private $get;
    private $post;
    private $header;
    
    private static $instance;

    public function __construct () {
    
        $this->get = $_GET ?? [];
        $this->header = getallheaders();

        $input = file_get_contents("php://input");
        $body = $input ? json_decode($input, true) : [];
        $this->post = array_merge($_POST, $_FILES, $body);
    }

    /**
     * Verouille la page si la request de l'utilisateur n'est signée
     * 
     * @param string $separator
     * 
     * @return null|string
     */
    public function verify_signiature(string $separator = ','): ?string
    {

        if (isset(ENV['API_SIGNATURE'])) {

            $signiature = $this->signature();
            $signiatures = explode($separator, ENV['API_SIGNATURE']);
            if (in_array($signiature, $signiatures)) {

                return $signiature;
            }
        }
        return null;
    }

    // public function get (string $name, $default = null, $type = self::STRING)
    // {
    //     if (!isset($this->get[$name])) {
    //         return $default; 
    //     }

    //     if ($type === self::INT && !filter_var($this->get[$name], FILTER_VALIDATE_INT)) {
    //         throw new InvalidParameterException($name, $type);
    //     }
    //     if ($type === self::INT) {
    //         return (int)$this->get[$name];
    //     }

    //     return (string)$this->get[$name];
    // }

    public static function getInstance (): Request
    {
        if (is_null(self::$instance)) {
            
            self::$instance = new Request();
        }

        return self::$instance;
    }

    public function get(?string $key = null)
    {

        return $key ? isValue($key, $this->get) : $this->get ; 
    }

    public function post(?string $key = null)
    {

        return $key ? isValue($key, $this->post) : $this->post; 
    }

    public function request(?string $key = null)
    {

        $request = array_merge($this->post(), $this->get());
        return $key ? isValue($key, $request) : $request; 
    }

    public function header(?string $key = null)
    {

        return $key ? isValue($key, $this->header) : $this->header;
    }

    public function authorisation(string $type = 'bearer'): ?string
    {

        $authorization = $this->header('Authorization');
        $datas = explode(' ', $authorization);
        if (isset($datas[0], $datas[1]) && strtolower($datas[0]) === strtolower($type)) {

            return $datas[1];
        }
        return null;
    }

    public function signature(): ?string
    {

        return $this->header('Signature');
    }

    public function bearer(): ?string
    {

        return $this->authorisation('bearer');
    }
}