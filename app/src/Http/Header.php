<?php

namespace App\Http;

class Header
{    
    /**
     * codes
     *
     * Codes et messages d'erreurs des entetes
     * @var array
     */
    protected $codes = [
        200 => 'ok',
        204 => 'No Content',
        301 => 'Moved Permanently',
        302 => 'Found (Redirect)',
        400 => 'Request Not Signed',
        401 => 'Unauthorized Error Occur',
        402 => 'Payment Required',
        403 => 'Forbidden',
        404 => 'Not Found',
        419 => 'Authentication Timeout',
        422 => 'Unprocessable Entity',
        500 => 'Internal Server Error',
        520 => 'Web server is returning an unknown error'
    ];


    /**
     * header_codes
     *
     * @return array
     */
    public function header_codes(): array
    {

        return $this->codes;
    }


    /**
     * header_code_message
     * 
     * Affiche le message d'erreur en fonction du code d'erreur
     * @param  int $code
     * @return null|string
     */
    public function header_code_message(int $code): ?string
    {

        return $this->header_codes()[$code] ?? null;
    }


    /**
     * header_status
     * 
     * Changer le Status de la reponse header
     * @param  int $code
     * @return void
     */
    public function header_status(int $code): void
    {

        $message = $this->header_code_message($code);
        if ($message) {

            header('HTTP/1.0 ' . $code . ' ' . $message, true, $code);
        }
    }


    /**
     * is_ajax
     *
     * Verifi si une requette est fait en ajax ou bloquante
     * @return bool
     */
    public function is_ajax(): bool
    {

        return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest';
    }


    /**
     * setCode
     * 
     * Cette methode permet d'ajouter un status code
     * @param  array $codes Tableau de la forme [code => "message"]
     * @return void
     */
    public function setCode(array $codes): void
    {

        $this->codes = array_merge($this->codes, $codes);
    }


    /**
     * notFound
     *
     * Affiche la page de 404
     * @return void
     */
    public function notFound(): void
    {

        $this->header_status(404);
        if (!$this->is_ajax()) {

            require_once ROOT . 'app' .  DIRECTORY_SEPARATOR . 'views' .  DIRECTORY_SEPARATOR . 'errors' . DIRECTORY_SEPARATOR . '404.php';
            exit();
        }
    }


    /**
     * redirect
     *
     * @param  string $url
     * @param  null|int $header_code Code de l'entente a renvoyer
     * @return void
     */
    public function redirect(string $url, ?int $header_code = null)
    {

        if ($header_code) $this->header_status($header_code);
        if ($this->is_ajax()) {

            // return $url;
            echo "redirect:$url";
        } else {

            header('Location: ' . $url);
        }
        exit();
    }
}
