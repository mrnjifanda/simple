<?php

namespace App\Http;

class JsonResponse extends Header
{

    /**
     * @param int $code
     * @param array $datas
     * @param null|string $message
     * 
     * @return string
     */
    public function response (int $code, $datas = [], ?string $message = null): string
    {

        $response = [
            "error" => true,
            "status" => $this->header_code_message($code),
            "status_code" => $code,
        ];

        if ($message) {

            $response['message'] = $message;
        }

        if (!empty($datas)) {

            $response['datas'] = $datas;
        }

        header("Content-Type: application/json; charset=UTF-8");
        $this->header_status($code);
        return print_r(json_encode($response, JSON_UNESCAPED_UNICODE));
        exit();
    }


    /**
     * notFound
     *
     * Affiche la page de 404
     * @return string
     */
    public function notFound(): void
    {

        $this->response(404);
        exit();
    }


    /**
     * notSigned
     * 
     * Affiche la page de 400
     * @return void
     */
    public function notSigned(): void
    {

        $this->response(400, [], 'Please add the signature in the header');
        exit();
    }

    /**
     * unauthorized
     * 
     * Affiche la page de 401
     * @return void
     */
    public function unauthorized(string $message = null, $datas = []): void
    {

        $this->response(401, $datas, $message);
        exit();
    }

    public function unprocessable($datas = [], ?string $message = null): void
    {

        $this->response(422, $datas, $message);
        exit();
    }

    public function unknown(string $message = 'An error occurred, please try again later')
    {

        $this->response(520, [], $message);
        exit();
    }


    /**
     * success
     * 
     * @param $datas
     * 
     * @return void
     */
    public function success($datas = [], ?string $message = null): void
    {

        if (is_string($datas)) {

            $message = $datas;
            $datas = [];
        }
        $this->response(200, $datas, $message);
        exit();
    }
}
