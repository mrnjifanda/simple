<?php

namespace App;

use App\Http\Header;
use \PDO;

class App extends Header
{

    /**
     * @var string
     */
    private $host;

    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $database;
    
    /**
     * @var array
     */
    private $env = [];

    /**
     * @var PDO
     */
    private $pdo;

    /**
     * @var App
     */
    public static $app;

    public function __construct(array $env)
    {

        if (!self::$app) {

            $this->env = $env;
            $this->host = $this->env['DB_HOST'];
            $this->username = $this->env['DB_USERNAME'];
            $this->password = $this->env['DB_PASSWORD'];
            $this->database = $this->env['DB_DATABASE'];

            self::$app = $this;
        }
    }

    /**
     * getDbInfos
     * 
     * @return array
     */
    public function getDbInfos(): array
    {
        
        return [
            'host' => $this->host,
            'username' => $this->username,
            'password' => $this->password,
            'database' => $this->database,
            'env' => $this->env
        ];
    }

    /**
     * getIsDev
     * 
     * @return bool
     */
    public function getIsDev(): bool
    {

        return isset($this->env['APP_ENV']) && $this->env['APP_ENV'] === 'dev' ? true : false;
    }

    /**
     * getPDO
     * 
     * @return PDO
     */
    public function getPDO(): PDO
    {

        if ($this->pdo === null) {

            $this->pdo = new PDO('mysql:dbname=' . $this->database . ';charset=utf8;host=' . $this->host, $this->username, $this->password, [
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            ]);
        }
        return $this->pdo;
    }

    /**
     * lastId
     * 
     * @return int
     */
    public function lastId(): int
    {

        return $this->getPDO()->lastInsertId();
    }

    /**
     * query
     * 
     * @param string $query
     * @param array|null $params
     * 
     * @return mixed
     */
    public function query(string $query, ?array $params = [])
    {

        try {

            $req = $this->getPDO()->prepare($query);
            $this->getPDO()->beginTransaction();
            $req->execute($params);
            $this->getPDO()->commit();
            return $req;
        } catch (\Throwable $th) {

            throw $th;
            // throw new DatabaseError($th->getMessage(), $th->getCode());
        }
    }

    /**
     * fetch
     * 
     * @param string $query
     * @param array|null $params
     * @param string|null $classMaping
     * 
     * @return mixed
     */
    public function fetch(string $query, ?array $params = [], ?string $classMaping = null)
    {
    
        $req = $this->query($query, $params);
        $fetch = $classMaping ? $req->fetchObject($classMaping) : $req->fetch(PDO::FETCH_OBJ);
        return $fetch ?? null;
    }

    /**
     * fetchAll
     * 
     * @param string $query
     * @param array|null $params
     * @param string|null $classMaping
     * 
     * @return mixed
     */
    public function fetchAll(string $query, ?array $params = [], ?string $classMaping = null)
    {

        $req = $this->query($query, $params);
        $fetch = $classMaping ? $req->fetchAll(PDO::FETCH_CLASS, $classMaping) : $req->fetchAll(PDO::FETCH_OBJ);
        return $fetch ?? null;
    }

    /**
     * fetchArray
     * 
     * @param string $query
     * @param string $method
     * @param array|null $params
     * 
     * @return array|null
     */
    public function fetchArray(string $query, string $method, ?array $params = []): ?array
    {

        return $this->query($query, $params)->$method() ?? null;
    }

    /**
     * count
     * 
     * @param string $query
     * @param array|null $params
     * 
     * @return int|null
     */
    public function count(string $query, ?array $params = []): ?int
    {

        return $this->query($query, $params)->fetch(PDO::FETCH_NUM)[0] ?? 0;
    }



    /**
     * Simples actions MySQL
     */

    private function select (?string $select = "*"): string
    {
        return "SELECT " . (!$select || $select === "*" ? $select : "id, " . $select);
    }

    private function from (string $table): string
    {
        return " FROM {$table}";
    }

    private function where (array $wheres, string $set = 'WHERE'): array
    {
        $string = '';
        $separateur = $set === 'WHERE' ? " AND" : " ,";
        if (!empty($wheres)) {
            $lastKey = array_key_last($wheres);
            foreach (array_keys($wheres) as $where) {
                $value = $wheres[$where];
                if ($lastKey === $where) {

                    if ($value === "NULL" || $value === "NOT NULL") {
                        $string .= ' ' . $where . ' IS ' . $value;
                        unset($wheres[$where]);
                    } else {
                        $string .= ' ' . $where . ' = :' . $where;
                    }

                } else {
                    if ($value === "NULL" || $value === "NOT NULL") {
                        $string .= ' ' . $where . ' IS ' . $where . $separateur;
                        unset($wheres[$where]);
                    } else {
                        $string .= ' ' . $where . ' = :' . $where . $separateur;
                    }
                }
            }
        }

        return [
            'string' => $string !== '' ? " {$set} {$string}" : null,
            'array' => $wheres
        ];
    }

    private function customSQL (?string $customSQL = null): ?string
    {
        return $customSQL ? " " . $customSQL  : null;
    }

    private function sqlSelect(string $method, string $table, ?string $classMaping = null, array $where = [], string $select = "*", ?string $customSQL = null)
    {
        $where = $this->where(array_merge($where, ['supprimer' => 'NULL']));
        return
        $this->$method(
            $this->select($select) . $this->from($table) . $where['string'] . $this->customSQL($customSQL),
            $where['array'],
            $classMaping
        );
    }

    private function sqlInsert(string $table, array $set, bool $year = true)
    {
        $set = $this->where($set, "SET");
        return
        $this->query(
            "INSERT INTO {$table}" . $set['string'] . ($year ? ', year = ' . YEAR : null) . ", created_at = NOW()",
            $set['array']
        );
    }

    private function sqlUpdate(string $table, array $set, array $wheres)
    {
        $set = $this->where($set, "SET");
        $wheres = $this->where($wheres);
        return
        $this->query(
            "UPDATE {$table}" . $set['string'] . ", updated_at = NOW()" . $wheres['string'],
            array_merge($set['array'], $wheres['array'])
        );
    }

    public function all(string $table, string $classMaping, string $select = "*", array $where = [], ?string $customSQL = null)
    {
        return $this->sqlSelect('fetchAll', $table, $classMaping, $where, $select, $customSQL);
    }

    public function find(string $table, ?string $classMaping = null, string $select = "*", array $where = [], ?string $customSQL = null)
    {
        return $this->sqlSelect('fetch', $table, $classMaping, $where, $select, $customSQL);
    }

    public function findArray(string $table, array $where, ?string $method = 'fetch')
    {
        $where = $this->where($where);
        return
        $this->fetchArray(
            $this->select() . $this->from($table) . $where['string'],
            $method,
            $where['array']
        );
    }

    public function insert(string $table, array $set, bool $year = true)
    {
        $this->sqlInsert($table, $set, $year);
    }

    public function update(string $table, array $set, array $wheres)
    {
        $this->sqlUpdate($table, $set, $wheres);
    }

    public function delete(string $table, array $wheres)
    {
        $this->sqlUpdate($table, ['supprimer' => 1], $wheres);
    }

    public function slimCount(string $table, array $where = [])
    {
        $where = $this->where($where);
        return
        $this->count(
            "SELECT COUNT(id) " . $this->from($table) . $where['string'],
            $where['array']
        );
    }
}
