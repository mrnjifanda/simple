<?php

namespace App\Users;

use App\App;
use Model\Users\User;

class Users
{
    public static function getUser(App $app, int $user, string $select = '*'): ?User
    {
        return $app->find('users', User::class, $select, ['id' => $user]);
    }

    public static function getUsernameById(App $app, int $id): string
    {
        $user = self::getUser($app, $id, 'username');
        return $user ? $user->getUsername() : null;
    }
}
