<?php

namespace App\Users;

use App\Helpers\Encode;
use App\App;
use App\Html\Text;
use App\Helpers\Date;

use Model\Users\User;
use App\Helpers\Cookies;
use App\Helpers\Sessions;

class Auth
{
    private $app;
    private $sessions;
    private $cookies;
    public static $auth;

    public function __construct()
    {
        if (!$this->app) {
            $this->app = App::$app;
        }

        if (!$this->sessions) {
            $this->sessions =  Sessions::$sessions;
        }

        if (!$this->cookies) {
            $this->cookies =  Cookies::$cookies;
        }

        if (!static::$auth) {
            static::$auth = $this;
        }
    }
    
    /**
     * findUser
     *
     * @param  array $where
     * @param  string $select
     * @return User
     */
    private function findUser(array $where = [], string $select = "*"): ?User
    {
        return $this->app->find("users", User::class, $select, $where);
    }

    /**
     * Verifi s'il y'a un utilisater en session et return cet utilisateur
     *
     * @return User
     */
    public function user(): ?User
    {

        $token = $this->sessions->read('auth');
        if (is_null($token)) {
            return null;
        }

        $decode = new Encode();
        $datas = $decode->decode($token);
        if (!isset($datas->id)) {
            return null;
        }

        return $this->findUser(['id' => $datas->id]);
    }

    /**
     * Verouille la page s'il y'a pas d'utilisateur connecté
     *
     * @param string $redirect_url
     * @return User|null
     */
    public function requireUser (string $redirect_url = '/login'): ?User
    {
        $user = $this->user();
        if (!$user) {
            $this->sessions->setFlash('danger', 'Connectez-vous pour continuer.');
            $this->app->redirect($redirect_url, 403);
            return null;
        }
        return $user;
    }

    /**
     * Verouille la page si un utilisateur est connecté
     *
     * @param string|null $redirect_url
     * @return void
     */
    public function openUser(?string $redirect_url = '/'): void
    {
        if ($this->user()) {
            $this->sessions->setFlash('warning', 'Vous êtes déjà connecté.');
            $this->app->redirect($redirect_url, 403);
        }
    }

    /**
     * Ajouter la derniere date de connexion
     *
     * @return void
     */
    public function addLastConnexion(): void
    {
        $user = $this->user();

        if ($this->user()) {
            $this->app->query('UPDATE users SET lasted_at = NOW() WHERE id = :id', ['id' => $user->getId()]);
        }
    }

    /**
     * login
     *
     * @param string $password
     * @param string $remember
     * @return null|int
     */
    public function login(string $username, string $password, ?string $remember = null): ?int
    {

        $user = $this->app->fetch("SELECT id, password FROM users WHERE (username = :username OR email = :username) AND confirmed_at IS NOT NULL", ['username' => $username], User::class);
        if (!$user) {

            return null;
        }

        if (password_verify($password, $user->getPassword())) {

            $id = $user->getId();
            if ($remember) {
                $token = Text::random(250);
                $this->app->query("UPDATE users SET remember_token = :token WHERE id = :id", ['token' => $token, 'id' => $id]);
                $this->cookies->write('remember', $id . '==' . $token . sha1($id . 'easyTalkappjifitech'), 60 * 60 * 24 * 7);
            }
            return $id;
        }

        return null;
    }

    /**
     * Reconnexion automatique s'il y'a un cookie
     *
     * @return void
     */
    public function remember(): void
    {
        $remember_token = $this->cookies->read('remember');
        if (!$this->user() && $remember_token) {

            $parts = explode('==', $remember_token);
            $user_id = $parts[0];
            $user = $this->findUser(['id' => $user_id], "remember_token");
            if ($user) {
                $expected = $user_id . '==' . $user->getProprety('remember_token') . sha1($user_id . 'easyTalkappjifitech');

                if ($expected === $remember_token) {

                    $this->sessions->write('auth', $user_id);
                    $this->cookies->write('remember', $expected, 60 * 60 * 24 * 7);
                } else {

                    $this->cookies->delete('remember');
                }
            } else {

                $this->cookies->delete('remember');
            }
        }
    }

    /**
     * Verifier la validité d'un token reset_token
     *
     * @param string $username
     * @param string $token
     * @return void
     */
    public function verifyResetToken(string $username, string $token): void
    {
        $user = $this->app->fetch("SELECT id, reset_token, reset_at FROM users WHERE username = :username AND confirmed_at IS NOT NULL AND supprimer IS NULL", ['username' => $username], User::class);
        if (!$user || $user->getResteToken() !== $token) {
            $this->sessions->setFlash('warning', 'Utilisateur ou token invalide');
            $this->app->redirect('/login', 403);
        }

        $validDate = Date::addMinute($user->getResetAt(), 20); # Add 20 minutes

        if (Date::compareToDay($validDate)) {
            $this->sessions->setFlash('warning', 'Le token n\'est plus invalide');
            $this->app->redirect('/login', 403);
        }
    }

    /**
     * sendForgetPassword
     *
     * @param mixed $email
     * @return void
     */
    public function sendForgetPassword(string $email): ?array
    {

        $user = $this->app->fetch("SELECT id, username FROM users WHERE email = :email AND confirmed_at IS NOT NULL AND supprimer IS NULL", ['email' => $email], User::class);
        if (!$user) {

            return null;
        }

        $token = Text::random(150);
        $params = [ 'token' => $token, 'id' => $user->getId() ];
        $this->app->query("UPDATE users SET reset_token = :token, reset_at = NOW() WHERE id = :id", $params);
        return array_merge($params, ['username' => $user->getUsername()]);
    }

    /**
     * resetPassword
     *
     * @param string $password
     * @param string $username
     * @param bool $reset
     * @return void
     */
    public function resetPassword(string $password, string $username, bool $reset = false): void
    {
        $password = password_hash($password, PASSWORD_BCRYPT);

        if ($reset) {
            $this->app->query('UPDATE users SET password = :password, reset_token = NULL, reset_at = NULL WHERE username = :username', ['password' => $password, 'username' => $username]);
        } else {
            $this->app->query('UPDATE users SET password = :password WHERE username = :username', ['password' => $password, 'username' => $username]);
        }
    }

    public function requireRoles(string ...$roles): ?User
    {
        $user = $this->user();
        if (!$user || !in_array($user->getRole(), $roles)) {
            $this->app->redirect('/', 403);
        }
        return $user;
    }

    public function activeAccount(string $username, string $password, string $token)
    {
        $user = $this->findUser(['confirmation_token' => $token], 'id');

        if ($user) {
            $password = password_hash($password, PASSWORD_BCRYPT);
            $id = $user->getId();

            $this->app->query(
                "UPDATE users SET username = :username, password = :password, confirmation_token = NULL, confirmed_at = NOW() WHERE id = :id",
                [
                    'id' => $id,
                    'username' => $username,
                    'password' => $password,
                ]
            );

            $this->sessions->write('auth', $id);
        }
    }
}
