<?php

namespace App\Helpers;

use App\App;
use Traits\API\ResponseTrait;

class Validator
{

    use ResponseTrait;

    private $data;
    private $app;
    private $lang;
    private $errors = [];
    private $value = null;

    public function __construct(array $data, ?App $app = null, string $lang = 'fr')
    {
        $this->data = $data;
        $this->app = $app;
        $this->lang = $lang;
    }

    private function getMessages(string $method): string
    {

        $messages = [
            'required' => __('validations.required'),
            'min' => __('validations.min'),
            'max' => __('validations.max'),
            'email' => __('validations.email'),
            'alpha' => __('validations.alpha'),
            'date' => __('validations.date'),
            'phone' => __('validations.phone'),
            'numeric' => __('validations.numeric'),
            'confirm' => __('validations.confirm'),
            'uniq' => __('validations.uniq'),
            'exist' => __('validations.exist'),
            'format' => __('validations.format')
        ];

        $message = $messages[$method];
        if (strpos($message, '@value@')) {

            $message = str_replace('@value@', $this->value, $message);
        }

        return $message;
    }

    private function getField(string $field): ?string
    {
        return $this->data[$field] ?? null;
    }

    private function addError(string $field, string $method, ?string $message = null): void
    {
        $this->errors[$field] = $message ?? $this->getMessages($method);
    }

    public function required(string $field, ?string $message = null): void
    {
        if (empty($this->data[$field])) {
            $this->addError($field, 'required', $message);
        }
    }

    public function min(string $field, int $min, ?string $message = null): void
    {
        if (strlen($this->getField($field)) < $min) {
            $this->value = $min;
            $this->addError($field, 'min', $message);
        }
    }

    public function max(string $field, int $max, ?string $message = null): void
    {
        if (strlen($this->getField($field)) > $max) {
            $this->value = $max;
            $this->addError($field, 'max', $message);
        }
    }

    public function lenght(string $field, int $min, int $max): void
    {
        $this->min($field, $min);

        if ($this->isVaLid($field)) {
            $this->max($field, $max);
        }
    }

    public function email(string $field, ?string $message = null): void
    {
        if (!filter_var($this->getField($field), FILTER_VALIDATE_EMAIL)) {
            $this->addError($field, 'email', $message);
        }
    }

    public function alpha(string $field, ?string $message = null): void
    {
        if (!preg_match('/^[a-zA-Z0-9]+$/', $this->getField($field))) {
            $this->addError($field, 'alpha', $message);
        }
    }

    public function date(string $field, ?string $message = null): void
    {
        if (!preg_match("#(\d{4})/(\d{2})/(\d{2})#", $this->getField($field))) {
            $this->addError($field, 'date', $message);
        }
    }

    public function phone(string $field, ?string $message = null): void
    {
        if (!preg_match('/^[0-9\-\+]+$/', $this->getField($field))) {
            $this->addError($field, 'phone', $message);
        }
    }

    public function numeric(string $field, ?string $message = null): void
    {
        if (!is_numeric($this->getField($field))) {
            $this->addError($field, 'numeric', $message);
        }
    }

    public function confirm(string $field, ?string $message = null): void
    {
        $value = $this->getField($field);
        if (empty($value) || $value != $this->getField($field . '-confirm')) {
            $this->addError($field . '-confirm', 'confirm', $message);
        }
    }

    public function uniq(string $field, string $table, string $champ)
    {
        $uniq = $this->app->fetch("SELECT id FROM $table WHERE $champ = :field", ['field' => $this->getField($field)]);

        if ($uniq) {
            $this->addError($field, 'uniq');
        }
    }

    public function exist(string $field, string $table, string $champ)
    {
        $exist = $this->app->fetch("SELECT id FROM $table WHERE $champ = :field", ['field' => $this->getField($field)]);

        if (!$exist) {
            $this->addError($field, 'exist');
        }
    }

    public function format(string $field, string $regex, ?string $message): void
    {
        if (!preg_match($regex, $this->getField($field))) {
            $this->value = $regex;
            $this->addError($field, 'format', $message);
        }
    }

    /**
     * file
     *
     * @param  string $field
     * @param  int $size ko
     * @param  array $extension
     * @return void
     */
    public function file(string $field, ?int $size = 2048, ?array $extension = ['jpg', 'jpeg', 'gif', 'png']): void
    {
        if (!isset($this->data[$field]['name']) || !isset($this->data[$field]['size']) || !isset($this->data[$field]['tmp_name'])) {
            $this->errors[$field] = 'Invalid file';
        } elseif ($this->data[$field]['size'] > $size * 1024) {
            $this->errors[$field] = 'File size greater than the limit (' . $size . ' Ko)';
        } elseif (!in_array(strtolower(substr(strrchr($this->data[$field]['name'], '.'), 1)), $extension)) {
            $this->errors[$field] = 'Unsupported file (' . implode(', ', $extension) . ')';
        }
    }

    public function inarray(string $field, string $array, ?string $message = null)
    {
        $array = explode(',', $array);

        if (!in_array($this->getField($field), $array)) {
            $this->addError($field, 'exist', $message);
        }
    }

    public function isVaLid(?string $field = null): bool
    {
        return $field ? empty($this->errors[$field]) : empty($this->errors);
    }

    public function getErrors(): ?array
    {
        return empty($this->errors) ? null : $this->errors;
    }

    public function validate(array $fields)
    {

        foreach ($fields as $field => $rules) {

            $rule = explode('|', $rules);
            foreach ($rule as $option) {

                if ($option == 'optional') {

                    if (empty($this->data[$field]) || empty($this->data[$field]['name'])) {

                        break 1;
                    }
                } else {

                    if (strpos($option, ':')) {

                        $explode = explode(':', $option);
                        if (count($explode) === 3) {

                            list($method, $param, $message) = $explode;
                            if ($method === 'file') {

                                $param = (int) $param;
                                $message = explode(',', $message);
                            }
                            $this->$method($field, $param, $message);
                        } else {

                            list($method, $param) = $explode;
                            $this->$method($field, $param);
                        }
                    } else {

                        $this->$option($field);
                    }
                }

                if (!$this->isVaLid($field)) {

                    break 1;
                }
            }
        }

        $isValid = $this->getErrors();
        if (is_array($isValid)) {

            $this->unprocessable($isValid);
        }
    }

    // preg_match('/^[a-zA-Z0-9ŸÜÛÙŒÔÏÎËÊÈÉÇÆÂÀÿüûùœôïîëêèéçæâà]+$/'
}
