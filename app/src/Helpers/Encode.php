<?php
    namespace App\Helpers;

    use Firebase\JWT\JWT;
    use Firebase\JWT\Key;

    class Encode
    {

        protected $secrect;
        protected $issued_at;
        protected $iss;
        protected $expire;

        protected $token;
        protected $jwt;

        public function __construct()
        {

            $this->issued_at = time();
            $this->expire = $this->issued_at + 3600;
            $this->secrect = ENV['JWT_SECRET'];
            $this->iss = ENV['APP_URL'];
        }

        public function encode(array $data, string $alg = 'HS256'): string
        {

            $this->token = [
                "iss" => $this->iss,
                "aud" => $this->iss,
                "iat" => $this->issued_at,
                "exp" => $this->expire,
                "data" => $data
            ];

            $this->jwt = JWT::encode($this->token, $this->secrect, $alg);
            return $this->jwt;
        }

        public function decode($token, string $alg = 'HS256')
        {
            try {

                $decode = JWT::decode($token,  new Key($this->secrect, $alg));
                return $decode->data;
            } catch (\Exception $e) {
                return [
                    "message" => $e->getMessage()
                ];
            }
        }
    }