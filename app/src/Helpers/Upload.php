<?php

namespace App\Helpers;

use App\Helpers\Date;

class Upload
{
    
    private static $images = ROOT . 'public' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR;

    private static function extension (string $extension): string
    {

        return strtolower(substr(strrchr($extension, '.'), 1));
    }

    /**
     * verifyFile
     * 
     * Validatation sur l'image
     * @param  array $file ['name, 'size', 'tmp_name']
     * @param  array $extension_valides ['jpg', 'jpeg', 'gif', 'png']
     * @param  int $taille_max (octet)
     * @return null|string
     */
    private static function verifyFile (array $file, array $extension_valides = ['jpg', 'jpeg', 'gif', 'png'],  int $taille_max = 10485760): ?string
    {

        if (!isset($file['name']) || !isset($file['size']) || !isset($file['tmp_name'])) {
            return 'Fichier invalide';
        }

        if ($file['size'] > $taille_max) {
            return 'Taille du fichier supérieure a la limite';
        }

        if (!in_array(self::extension($file['name']), $extension_valides)) {

            return 'Fichier non supporté (' . implode(', ', $extension_valides) .')';
        }

        return null;
    }


    
    /**
     * upload
     *
     * @param  array $file
     * @param  string $filename
     * @param  string $paph
     * @return string|null
     */
    private static function upload (array $file, string $filename, string $path): array
    {

        $filename = $filename . '-' . Date::toDay('YdmHis') . '.' . self::extension($file['name']);
        move_uploaded_file($file['tmp_name'], $path . DIRECTORY_SEPARATOR . $filename);

        return [
            'path' => $path . DIRECTORY_SEPARATOR . $filename,
            'filename' => $filename
        ];
    }
    
    

    /**
     * imageFolderUpload
     * 
     * Permet d'uploder des fichiers dans le dossier public/images
     * @param  array $file ['name, 'size', 'tmp_name']
     * @param  string $return_name
     * @param  string $return_paph
     * @param  array $miniatures [... [width1, height1], [width2, height2]]
     * @param  array $extension_valides ['jpg', 'jpeg', 'gif', 'png']
     * @param  int $taille_max (octet)
     * @return null|string
     */
    public static function inImageFolder (array $file, string $filename, string $paph, array $miniatures, array $extension_valides = ['jpg', 'jpeg', 'png'], int $taille_max = 2097152): ?string
    {
        $upload = self::verifyFile($file, $extension_valides, $taille_max);

        if ($upload) {
            return $upload;
        }

        imagesFolder($paph . DIRECTORY_SEPARATOR . 'upload');
        $uploadFilename = self::upload($file, $filename, static::$images . $paph . DIRECTORY_SEPARATOR . 'upload');

        
        foreach ($miniatures as $miniature) {
            imagesFolder($paph . DIRECTORY_SEPARATOR . 'thumbs-' . $miniature[0] . 'x' . $miniature[1]);
            self::miniature(
                $uploadFilename['path'],
                $paph . DIRECTORY_SEPARATOR . 'thumbs-' . $miniature[0] . 'x' . $miniature[1],
                $uploadFilename['filename'],
                $miniature[0],
                $miniature[1]
            );
        }
        return $uploadFilename['filename'];
    }


    
    /**
     * miniature
     *
     * @param  string $filename
     * @param  string $thumbPath
     * @param  string $thumbName
     * @param  int $width
     * @param  int $height
     * @return bool
     */
    public static function miniature (string $filename, string $thumbPath, string $thumbName, int $width = 100, int $height = 100): bool
    {
        $infoImages = getimagesize($filename);
        $NouvelleImage = ImageCreateTrueColor($width, $height);
        $file_extension = '.' . self::extension($filename);
        $valides_extensions = [".jpeg", '.jpg', '.png', '.gif'];

        if (!in_array($file_extension, $valides_extensions)) {
            return false;
        }

        if ($file_extension == ".jpg" || $file_extension == ".jpeg") {
            $ImageChoisie = ImageCreateFromJpeg($filename);
        }
        
        if ($file_extension == ".png") {
            $ImageChoisie = ImageCreateFromPng($filename);
            imagealphablending($NouvelleImage, false);
            imagesavealpha($NouvelleImage, true);
        }

        if ($file_extension == ".gif") {
            $ImageChoisie = ImageCreateFromGif($filename);
        }

        ImageCopyResampled($NouvelleImage, $ImageChoisie, 0, 0, 0, 0, $width, $height, $infoImages[0], $infoImages[1]);

        imagesFolder($thumbPath);
        switch ($file_extension) {
            case ".jpg":
                imagejpeg($NouvelleImage, $thumbPath . "/" . $thumbName . $file_extension, 90);
                break;

            case ".jpeg":
                imagejpeg($NouvelleImage, $thumbPath . "/" . $thumbName . $file_extension, 90);
                break;

            case ".png":
                imagepng($NouvelleImage, static::$images . $thumbPath . "/" . $thumbName);
                // imagepng($NouvelleImage, $thumbPath . "/" . $thumbName . $file_extension);
                break;

            case ".gif":
                imagegif($NouvelleImage, $thumbPath . "/" . $thumbName . $file_extension);
                break;
        }

        imagedestroy($ImageChoisie);
        imagedestroy($NouvelleImage);
        return true;
    }


    // public static function UploadMiniature(array $picture, string $name, string $paph, ?array $sizePictures = [200, 200]): string
    // {
    //         $name = $name . '-' . Text::random(10);
    //         $fullName = $name . '.' . getExtension($picture['name']);
    //         $paph = './images/' . $paph . '/';
    //         $original = $paph . 'originaux/' . $fullName;

    //         self::upload($picture, $name, $paph . 'originaux');
    //         Images::miniature($original, $paph . 'thumbs', $name, $sizePictures[0], $sizePictures[1]);

    //         return $fullName;
    // }
}