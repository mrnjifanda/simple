<?php
namespace App\Helpers;

class Cookies
{
    public static $cookies;
    private static $instance;

    public function __construct()
    {

        if (!self::$cookies) {
            self::$cookies = $this;
        }
    }

    public static function getInstance (): Cookies
    {
        if (is_null(self::$instance)) {
            
            self::$instance = new Cookies();
        }

        return self::$instance;
    }

    public function read (?string $key = 'remember')
    {
        return isset($_COOKIE[$key]) && !empty($_COOKIE[$key]) ? $_COOKIE[$key] : null;
    }

    public function write (string $key = 'remember', $value = null, int $date = 0)
    {
        setcookie($key, $value, time() + $date);
    }
    
    /**
     * delete
     *
     * @param  string|null $key
     * @return void
     */
    public function delete (?string $key = 'remember'): void
    {
        if ($this->read($key)) setcookie($key, null, -1);
    }
}