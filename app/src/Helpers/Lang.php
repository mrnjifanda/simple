<?php

namespace App\Helpers;

class Lang
{        
    /**
     * lang
     *
     * @var mixed
     */
    private $lang;

    /**
     * content
     *
     * @var object
     */
    private $content;

    private static $_instance;


    public function __construct()
    {
        $this->lang = getLang();
        $fileLang = json_encode(require_once BASE . 'lang' . DIRECTORY_SEPARATOR . 'lang.php');
        $this->content = json_decode($fileLang);
    }

    public  static function getInstance(): Lang
    {
        
        if (is_null(self::$_instance)) {

            self::$_instance = new Lang();
        }

        return self::$_instance;
    }

    public function getLang(): string
    {

        return $this->lang;
    }

    public function getContent(): object
    {

        return $this->content;
    }
}
