<?php

    namespace App\Helpers;

    class Pagination {

        /**
         * @var array
         */
        public $paginated = [];

        public $results;

        private $link = [];

        public function __construct($results, array $paginated = [])
        {

            $this->paginated = $paginated;
            $this->results = $results;
        }

        private function getCurrentPage(): int
        {

            return $this->paginated['page'] ?? 1;
        }

        private function getPages (): int
        {

            return ceil($this->paginated['totals_results'] / $this->paginated['per_page']);
        }

        private function getLink(string $link): array
        {

            if (empty($this->link)) {

                $parse_url = parse_url($link);
                $path = $parse_url['path'];
                $query = $parse_url['query'] ?? [];
                $this->link['path'] = $path;
                if (!empty($query)) {

                    parse_str(parse_url($link, PHP_URL_QUERY), $query);
                }
                $this->link['query'] = $query;
            }
            return $this->link;
        }

        private function setLink(int $page, string $link)
        {

            $array_link = $this->getLink($link);
            $query = $array_link['query'];
            $query['page'] = $page;
            return $array_link['path'] . '?' . http_build_query($query);
        }

        public function first (string $link): ?string
        {

            if ($this->getPages() <= 4 || $this->getCurrentPage() <= 4) {

                return null;
            }
            return '<li><a href="' . $this->setLink(1, $link) . '" class="arrow-left"><i class="fa fa-arrow-left"></i><i class="fa fa-arrow-left"></i></a></li>';
        }

        public function latest (string $link): ?string
        {

            $pages = $this->getPages();
            if ($pages <= 4 || ($this->getCurrentPage() + 4) <= $pages) {

                return null;
            }
            return '<li><a href="' . $link . '?page=' . $pages . '" class="arrow-right"><i class="fa fa-arrow-right"></i><i class="fa fa-arrow-right"></i></a></li>';
        }

        public function previous (string $link): ?string
        {
            $currentPage = $this->getCurrentPage();
            if ($currentPage <= 1) {

                return null;
            }
            return '<li><a href="' . $this->setLink(($currentPage - 1), $link) . '" class="arrow-left"><i class="fa fa-arrow-left"></i></a></li>';
        }

        public function next (string $link): ?string
        {

            $currentPage = $this->getCurrentPage();
            if ($currentPage >= $this->getPages()) {

                return null;
            }
            return '<li><a href="' . $this->setLink(($currentPage + 1), $link) . '" class="arrow-right"><i class="fa fa-arrow-right"></i></a></li>';
        }

        public function lists (string $link): string
        {

            $pages = $this->getPages();
            $currentPage = $this->getCurrentPage();
    
            $number = '';
            if($pages !=1){
    
                if($currentPage > 1) {
    
                    if ($currentPage > 4) {

                        $pointPrev = (($currentPage - 3) === 1) ? null : ' ... ';
                        $number .= '<li><a href="' . $this->setLink(1, $link) . '">1</li></a>' . $pointPrev;
                    }
    
                    for($i = $currentPage - 2; $i < $currentPage; $i++) {

                        if($i > 0) {

                            $number .= '<li><a href="' . $this->setLink($i, $link) . '">' . $i . '</a></li>';
                        }
                    }
                }
    
                $number .= '<li><a href="' . $this->setLink($currentPage, $link) . '" class="is-active">' . $currentPage . '</a></li>';
                for($i = $currentPage +  1; $i <= $pages; $i++){
    
                    $number .= '<li><a href="' . $this->setLink($i, $link) . '">' . $i . '</a></li>';
                    if($i >= $currentPage + 2){ break; }
                }
    
                if (($currentPage + 3) <= $pages) {

                    $pointNext = (($currentPage + 3) === $pages) ? null : ' ... ';
                    $number .= $pointNext . '<li><a href="' . $this->setLink($pages, $link) . '">' . $pages . '</a></li>';
                }
            }
            return $number;
        }

        public function pagination (string $link, bool $first_latest_links = false): string
        {

            $first = $first_latest_links ? $this->first($link) : null;
            $latest = $first_latest_links ? $this->latest($link) : null;
            return $first . $this->previous($link) . $this->lists($link) . $this->next($link) . $latest;
        }
    }
