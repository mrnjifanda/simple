<?php
namespace App\Errors;

class EmailError extends Errors {

    public function __construct(string $message)
    {

        parent::__construct('Mail', $message);
    }
}