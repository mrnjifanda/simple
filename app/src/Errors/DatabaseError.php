<?php
namespace App\Errors;

class DatabaseError extends Errors {

    public function __construct(string $message, int $code = 500)
    {

        parent::__construct('Database', $message, $code);
    }
}