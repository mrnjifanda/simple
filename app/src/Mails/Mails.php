<?php

namespace App\Mails;

use App\Errors\EmailError;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;

class Mails
{
    private static $env = ENV;
    
    /**
     * SMTP
     *
     * @param  string $method: isSMTP | isMail
     * @return PHPMailer
     */
    private static function SMTP (string $method = 'isSMTP'): PHPMailer
    {

        $mail = new PHPMailer();
        $mail->SMTPDebug      = 0;
        $mail->$method();
        $mail->Host           = self::$env['MAIL_HOST'];
        $mail->SMTPAuth       = true;
        $mail->Username       = self::$env['MAIL_USERNAME'];
        $mail->Password       = self::$env['MAIL_PASSWORD'];
        $mail->SMTPSecure     = self::$env['MAIL_ENCRYPTION'];
        $mail->Port           = self::$env['MAIL_PORT'];

        return $mail;
    }

    public static function mail(string $sujet, string $message, ?string $email = null, ?string $name = null, array $variables = []): bool
    {

        try {

            $mail = self::SMTP();

            $name = $name ?? self::$env['APP_NAME'];
            $email = $email ?? self::$env['MAIL_USERNAME'];

            $mail->setFrom(setEmail($name), strtoupper($name));
            $mail->addAddress($email, $name);
            $mail->Subject = $sujet;

            $mail->isHTML(true);

            if (file_exists(BASE . 'mails' . DIRECTORY_SEPARATOR . replace($message) . '.php')){

                $mail->MsgHTML(layout('mails.' . $message, $variables, 'mail'));
            } else {
                
                $mail->MsgHTML($message);
            }

            return $mail->send() === true ? true : false;
        } catch (Exception $error) {

        //    dd($error);
        }

        // $mail->AltBody = 'Hi there, we are happy to confirm your booking. Please check the document in the attachment.';
        // add attachment
        // $mail->addAttachment('//confirmations/yourbooking.pdf', 'yourbooking.pdf');
        // send the message
        // if(!$mail->send()){
        //     echo 'Message could not be sent.';
        //     echo 'Mailer Error: ' . $mail->ErrorInfo;
        // } else {
        //     echo 'Message has been sent';
        // }
    }

    public static function multiMail(string $sujet, string $message, $emails)
    {
        $mail = new PHPMailer(true);
        try {
            $mail->SMTPDebug = 0;
            $mail->isSMTP();
            $mail->Host = 'relay-hosting.secureserver.net';
            $mail->Port = 25;
            $mail->SMTPAuth = false;
            $mail->SMTPSecure = false;

            $mail->setFrom('contact@njifanda.com', 'Mr NJIFANDA');
            $mail->addReplyTo('contact@njifanda.com', 'Mr NJIFANDA');

            foreach ($emails as  $email) {
                $mail->addAddress($email->email);
            }

            $mail->isHTML(true);
            $mail->Subject = $sujet;
            $mail->Body = $message;

            $mail->send();
        } catch (Exception $e) {

            $isOk = 'Message pas envoyer';
        }
    }

    public static function SMS (string $destinataire, string $message, ?string $signature = 'Mr NJIFANDA', ?int $phoneCompte = 237695512345, ?string $password = 'MRnjifanda2019SMS')
    {
        $curl = curl_init();

        /*
        $data = [
                'SmsUserName' => $phoneCompte,
                'SmsUserPassword' => $password,
                'SmsSource' => $signature,
                'SmsDestination' => $destinataire,
                'SmsMessage' => $message,
            ];
            $url = 'http://api.sms.svr02.bitang.net/api/SmsApi.asmx?WSDL';
        */
        $data = [
            'username' => $phoneCompte,
            'password' => $password,
            'phone' => $signature,
            'sender' => $destinataire,
            'message' => $message,
        ];

        
        $url = 'http://api.sms.237.b2isms.com/api/httpapi.aspx';

        curl_setopt_array($curl, [
            CURLOPT_URL => $url,
            CURLOPT_POSTFIELDS => http_build_query($data)
        ]);

        $response = curl_exec($curl);
        //$responseArray = [];
        //parse_str($response, $responseArray);
        /*
            if ($response->isOk) {
                return null;
            } else {
                return ['statut' => 'Echec', 'errorCode' => $response->errCode, 'respones' => $response->result];
            }
        */

        return $response;
    }
}