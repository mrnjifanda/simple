<?php

namespace App\Database\Faker;

use Database\Table;

class Faker extends Table {

    public static function create ()
    {
        $faker = \Faker\Factory::create('fr_FR'); // en_US

        $infos = [];
        for ($i = 0; $i < 15; $i++) {
            $infos []= [
                'name' => $faker->unique()->name(),
                'email' => $faker->unique()->email(),
                'text' => $faker->text(),
                'firstNameFemale' => $faker->firstNameFemale(),
                'lastName' => $faker->lastName(),
                'userName' => $faker->unique()->userName(),
                'firstName' => $faker->firstName(),
                'domainName' => $faker->domainName(),
                'firstNameMale' => $faker->firstNameMale(),
                'freeEmailDomain' => $faker->freeEmailDomain(),
                'dateTime' => $faker->dateTime()
            ];
        }

        return $infos;
    }
}
