<?php

namespace App\Database;

use mysqli;
use App\App;

class Database
{

    /**
     * @var App
     */
    protected $app;

    /**
     * @var array
     */
    protected $dbInfos;

    /**
     * @var mysqli
     */
    protected $mysqli;

    /**
     * @var string
     */
    protected $cmd = ROOT . DIRECTORY_SEPARATOR . 'configs' . DIRECTORY_SEPARATOR . 'cmd' . DIRECTORY_SEPARATOR;

    public function __construct(App $app)
    {

        $this->app = $app;
        $this->dbInfos = $this->app->getDbInfos();
    }

    public function getDbInfos(): array
    {

        return $this->dbInfos;
    }

    public function getMysqli(): mysqli
    {

        if (!$this->mysqli) {
            
            $this->mysqli = new mysqli($this->dbInfos['host'], $this->dbInfos['username'], $this->dbInfos['password'], $this->dbInfos['database']);
        }
        return $this->mysqli;
    }

    public function import ()
    {

        $path_db_folder = $this->cmd . 'databases' . DIRECTORY_SEPARATOR . 'db';
        $content_db_folder = getContentFolder($path_db_folder);
        if (empty($content_db_folder)) {
            
            echo "Database not found \n";
            return false;
        }
        
        $db = $path_db_folder . DIRECTORY_SEPARATOR . end($content_db_folder);
        $sqlScript = file($db);
        $query = '';
        foreach ($sqlScript as $line) {

            $startWith = substr(trim($line), 0 ,2);
            $endWith = substr(trim($line), -1 ,1);
            if (empty($line) || $startWith == '--' || $startWith == '/*' || $startWith == '//') {

                continue;
            }

            $query = $query . $line;
            if ($endWith == ';') {

                mysqli_query($this->getMysqli(), $query) or die("Problem in executing the SQL query :\n" . $query);
                $query= '';
            }
        }

        echo "SQL file imported successfully: " . end($content_db_folder);
        return true;
    }

    public function export()
    {
    
        $conn = $this->getMysqli();
        $conn->set_charset("utf8");

        $tables = array();
        $sql = "SHOW TABLES";
        $result = mysqli_query($conn, $sql);
        while ($row = mysqli_fetch_row($result)) {

            $tables[] = $row[0];
        }

        $sqlScript = "";
        foreach ($tables as $table) {

            // Prepare SQLscript for creating table structure
            $query = "SHOW CREATE TABLE $table";
            $result = mysqli_query($conn, $query);
            $row = mysqli_fetch_row($result);
            $sqlScript .= "\n\n" . $row[1] . ";\n\n";
            $query = "SELECT * FROM $table";
            $result = mysqli_query($conn, $query);
            $columnCount = mysqli_num_fields($result);

            // Prepare SQLscript for dumping data for each table
            for ($i = 0; $i < $columnCount; $i ++) {

                while ($row = mysqli_fetch_row($result)) {

                    $sqlScript .= "INSERT INTO $table VALUES(";
                    for ($j = 0; $j < $columnCount; $j ++) {

                        $row[$j] = $row[$j];
                        if (isset($row[$j])) {

                            $sqlScript .= '"' . $row[$j] . '"';
                        } else {

                            $sqlScript .= '""';
                        }

                        if ($j < ($columnCount - 1)) {

                            $sqlScript .= ',';
                        }
                    }
                    $sqlScript .= ");\n";
                }
            }
            
            $sqlScript .= "\n"; 
        }
    
        if(!empty($sqlScript)) {

            $backup_file_name = $this->dbInfos["database"] . '_' . time() . '.sql';
            $fileHandler = fopen($backup_file_name, 'w+');
            $number_of_lines = fwrite($fileHandler, $sqlScript);
            fclose($fileHandler);

            rename($backup_file_name, $this->cmd . 'databases' . DIRECTORY_SEPARATOR . 'db' . DIRECTORY_SEPARATOR . pathinfo($backup_file_name, PATHINFO_BASENAME));
            echo 'Database export successful :' . $backup_file_name;
            return true;
        }

        echo 'Database export failed';
        return false;
    }
}
