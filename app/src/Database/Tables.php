<?php

namespace App\Database;

use App\App;

class Tables extends Database
{

    /**
     * @var string|null
     */
    protected $table;

    /**
     * @var string|null
     */
    protected $model;

    public function __construct(?App $app = null, ?string $table = null, ?string $model = null)
    {

        if ($app) {

            parent::__construct($app);
            $this->table = $table;
            $this->model = $model;
        }
    }

    /**
     * select
     * 
     * @param string $select
     * 
     * @return string
     */
    private function select (string $select = "*"): string
    {

        return "SELECT " . (!$select || $select === "*" ? $select : "id, " . $select);
    }

    /**
     * from
     * 
     * @return string
     */
    private function from (): string
    {

        return " FROM {$this->table}";
    }

    /**
     * where
     * 
     * @param array $wheres
     * @param string|null $delete
     * 
     * @return string|null
     */
    private function where (array $wheres, ?string $delete = null): ?string
    {

        $string = '';
        if (!empty($wheres)) {

            $lastKey = array_key_last($wheres);
            foreach (array_keys($wheres) as $where) {

                if ($lastKey === $where) {

                    $string .= ' ' . $where . ' = :' . $where;
                } else {

                    $string .= ' ' . $where . ' = :' . $where . ' AND';
                }
            }
        }

        if ($delete) {
            return $string !== '' ? " WHERE {$string} AND supprimer IS {$delete}" : " WHERE supprimer IS {$delete}";
        }

        return $string !== '' ? " WHERE {$string}" : null;
    }

    /**
     * query
     * 
     * @param string $method
     * @param string $select
     * @param array $where
     * @param string|null $delete
     * 
     * @return mixed
     */
    public function query(string $method, string $select, array $where = [], ?string $delete)
    {

        return
        $this->app->$method(
            $this->select($select) . $this->from() . $this->where($where, $delete),
            $where,
            $this->model
        );
    }

    /**
     * all
     * 
     * @param string $select
     * @param string|null $delete
     * 
     * @return mixed
     */
    public function all (string $select = "*", ?string $delete = "NULL")
    {

        return $this->query('fetchAll', $select, [], $delete);
    }

    /**
     * finds
     * 
     * @param array $where
     * @param string $select
     * @param string|null $delete
     * 
     * @return mixed
     */
    public function finds (array $where, string $select = "*", ?string $delete = "NULL")
    {

        return $this->query('fetchAll', $select, $where, $delete);
    }

    /**
     * find
     * 
     * @param array $where
     * @param string $select
     * @param string|null $delete
     * 
     * @return mixed
     */
    public function find (array $where, string $select = "*", ?string $delete =  "NULL")
    {

        return $this->query('fetch', $select, $where, $delete);
    }

    /**
     * findById
     * 
     * @param int $id
     * @param string $select
     * @param string|null $delete
     * 
     * @return mixed
     */
    public function findById (int $id, string $select = "*", ?string $delete =  "NULL")
    {

        return $this->find(['id' => $id], $select, $delete);
    }

    /**
     * count
     * 
     * @param string|null $delete
     * 
     * @return int
     */
    public function count (?string $delete = "NULL"): int
    {

        $where = $delete ? " WHERE supprimer IS {$delete}" : null;
        return $this->app->count("SELECT COUNT(id) FROM {$this->table} $where");
    }

    /**
     * countwhere
     * 
     * @param array $where
     * @param string|null $delete
     * 
     * @return int
     */
    public function countwhere (array $where, ?string $delete = "NULL"): int
    {
        return $this->app->count("SELECT COUNT(id) FROM {$this->table} {$this->where($where, $delete)}", $where);
    }

    public function create (array $datas)
    {
        # code...
    }

    public function update (int $id, array $datas)
    {
        # code...
    }

    public function delete (int $id)
    {
        # code...
    }
}
