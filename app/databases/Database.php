<?php

namespace Database;

use App\App;
class Database {

    /**
     * $app
     * 
     * @var App
     */
    protected static $app;

    public static function app(): App
    {

        if (is_null(self::$app)) {

            self::$app = App::$app;
        }
        return self::$app;
    }
}
