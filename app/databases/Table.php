<?php

namespace Database;

use App\Helpers\Pagination;

class Table extends Database {

    /**
     * $table
     * 
     * @var string
     */
    protected $table;

    /**
     * $class
     * 
     * @var string
     */
    protected $class;

    /**
     * $has_year
     * 
     * Check if the table has the year field
     * @var bool
     */
    protected $has_year = true;

    /**
     * getTable
     * 
     * @return string
     */
    protected function getTable(): string
    {

        if (is_null($this->table)) {

            $class_name = explode('\\', get_class($this));
            $true_class_name =  str_replace('Model', '', end($class_name));
            $all_uppercase = preg_replace('/([A-Z])/', '_$1', $true_class_name);
            $this->table = strtolower(substr($all_uppercase, 1));
        }
        return $this->table;
    }

    /**
     * getClass
     * 
     * @return string
     */
    protected function getClass(): string
    {

        if (is_null($this->class)) {

            $this->class = get_class($this);
        }
        return $this->class;
    }

    /**
     * hasYear
     * 
     * @return bool
     */
    protected function getHasYear(): bool
    {

        return $this->has_year;
    }

    /**
     * @param int|string|array $query
     * 
     * @return array
     */
    protected function params(int|string|array $query): array
    {

        if (is_int($query)) {

            $select = '*';
            $where = ['id' => $query];
        } elseif (is_string($query)) {

            $select = $query;
            $where = [];
        } else {

            if (array_key_exists('select', $query)) {

                $select = $query['select'];
                unset($query['select']);
            } else {

                $select = '*';
            }
            $where = $query;
        }

        return [
            'select' => $select,
            'where' => $where
        ];
    }

    protected function query(string $method, $query, ?string $customSQL = null)
    {

        $query = self::params($query);
        return self::app()->$method((new static)->getTable(), (new static)->getClass(), $query['select'], $query['where'], $customSQL);
    }

    public static function create(array $datas): int
    {

        self::app()->insert((new static)->getTable(), $datas, (new static)->getHasYear());
        return self::app()->lastId();
    }

    public static function all(string|array $query = '*')
    {

        return (new static)->query('all', $query);
    }

    public static function find(int|array $query)
    {

        return (new static)->query('find', $query);
    }

    public static function paginated(int $page, string|array $query = '*', int $per_page = 20)
    {

        $instance = new static;

        # Paginated
        $params = $instance->params($query);
        $total_results = $instance->count($params['where']);
        $starting_limit = ($page - 1) * $per_page;

        # Get results
        $results = $instance->query('all', $query, "LIMIT $starting_limit,$per_page");
        return new Pagination($results, [
            'page' => $page,
            'per_page' => $per_page,
            'totals_results' => $total_results
        ]);
    }

    public static function update(int $id, $datas)
    {

        self::app()->update((new static)->getTable(), $datas, ['id' => $id]);
    }

    public static function delete(int $id, array $where = [])
    {

        $where['id'] = $id;
        self::app()->delete((new static)->getTable(), $where);
    }

    public static function count(array $where = []): int
    {

        return self::app()->slimCount((new static)->getTable(), $where);
    }
}
