<?php

namespace Traits;

trait ModelTrait
{

    public function setAllAttributes(): void
    {

        if (!empty($this->models_attributes)) {

            $attributes = $this->models_attributes;
            foreach ($attributes as $attribute) {

                $attribute_method = 'get' . implode('', array_map('ucfirst', explode('_', $attribute))) . 'Attribute';
                $this->$attribute = $this->$attribute_method();
            }
        }
    }

    public function hasOne($classMapping, string $internal_key, string $external_key = 'id')
    {

        $table = (new $classMapping())->getTable();
        return self::app()->find($table, $classMapping, '*', [$external_key => $this->$internal_key]);
    }

    public function hasMany($classMapping, string $external_key, string $internal_key = 'id')
    {

        $table = (new $classMapping())->getTable();
        return self::app()->all($table, $classMapping, '*', [$external_key => $this->$internal_key]);
    }
}
