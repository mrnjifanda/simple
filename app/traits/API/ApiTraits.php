<?php

namespace Traits\API;

use App\App;
use App\Router;
use App\Users\Auth;
use App\Http\Request;
use App\Helpers\Encode;

trait ApiTraits
{

    /**
     * @var App
     */
    protected $app;

    /**
     * @var Auth
     */
    protected $auth;

    /**
     * @var Router
     */
    protected $routes;

    /**
     * @var Request
     */
    protected $request;

    /**
     * @var Encode
     */
    protected $jwt_code;

    /**
     * @var null|array
     */
    protected $post;

    /**
     * @var null|array
     */
    protected $get;

    public function initConfig(): void
    {

        $this->app = App::$app;
        $this->auth = Auth::$auth;
        $this->routes = Router::$routes;
        $this->jwt_code = new Encode();
        $this->request = Request::getInstance();

        $this->post = $this->request->post();
        $this->get = $this->request->get();
    }
}
