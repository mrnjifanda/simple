<?php

namespace Traits\API;

use App\Http\JsonResponse;

trait ResponseTrait {

    /**
     * @var JsonResponse
     */
    private $json_response;

    private function JsonResponse (): JsonResponse
    {

        if (is_null($this->json_response)) {

            $this->json_response = new JsonResponse();
        }
        return $this->json_response;
    }

    public function response (int $code, $datas = [])
    {

        $this->JsonResponse()->response($code, $datas);
        exit();
    }

    public function notFound () {

        $this->JsonResponse()->notFound();
        exit();
    }

    public function notSigned (): void
    {

        $this->JsonResponse()->notSigned();
        exit();
    }

    public function unauthorized(string $message = 'Resource protect by authentication', $datas = ['User not logged in']): void
    {

        $this->JsonResponse()->unauthorized($message, $datas);
        exit();
    }

    public function unprocessable($datas = [], string $message = 'A validation error occurred'): void
    {

        $this->JsonResponse()->unprocessable($datas, $message);
        exit();
    }

    public function unknown(string $message = 'An error occurred, please try again later')
    {

        $this->JsonResponse()->unknown($message);
        exit();
    }

    public function success($datas = []): void
    {

        $this->JsonResponse()->success($datas);
        exit();
    }
}
