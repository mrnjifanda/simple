<?php

namespace Traits;

trait PrefixTrait
{

    /**
     * @var string|null
     */
    private $prefix = null;

    /**
     * @param string $prefix
     * 
     * @return void
     */
    public function prefix(string $prefix): self
    {

        $this->prefix = $prefix;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPrefix(): ?string
    {

        return $this->prefix;
    }

    /**
     * @return void
     */
    public function removePrefix(): self
    {

        $this->prefix = null;
        return $this;
    }

    /**
     * @param string $url
     * 
     * @return string
     */
    public function urlPrefix(string $url): string
    {

        return $this->getPrefix() . $url;
    }
}
