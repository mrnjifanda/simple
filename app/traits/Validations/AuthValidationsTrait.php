<?php

namespace Traits\Validations;

trait AuthValidationsTrait
{

    public function login(): void
    {

        $this->validator()->validate([
            'username' => 'required',
            'password' => 'required|min:8',
            'remember' => 'optional|alpha'
        ]);
    }

    public function forgetPassword(): void
    {

        $this->validator()->validate([
            'email' => 'required|email|exist:users:email',
        ]);
    }
}
