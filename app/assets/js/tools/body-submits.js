import { post } from "./../modules/fetch.js";
import { showNotif } from './../modules/notifications.js';
import { select } from './../modules/dom.js';
import { closeModalItem } from "./../modules/modal.js";
import { addAppFormLoader, removeAppFormLoader, gestionErrors, addElement } from "./../modules/forms/app-form.js";

document.body.addEventListener('submit', async function(e) {
    const element = e.target;

    if (element.classList.contains('js-app-form')) {
        e.preventDefault();

        const data = new FormData(element);

        addAppFormLoader(element);

        const res = await post(element.action, { method: element.method, body: data });

        if (res.ok) {
            
            if (res.add) addElement(res.add);
            if (res.notif) showNotif(res.notif.message, res.notif.type); // Show notifiaction
            if (res.reset) element.reset(); // Reset form
            if (res.closeModal) closeModalItem(select('.jifi-modal-item')); // Close modal
            //if (res.delete) remove(url); // Remove item after delete
            if (res.page) location.href = res.page;

        } else {
            gestionErrors(element, res);
        }

        removeAppFormLoader(element);
    }
});