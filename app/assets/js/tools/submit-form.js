import { showNotif } from "./../modules/notifications.js";
import { post } from "./../modules/fetch.js";
import { addAppFormLoader, gestionErrors, removeAppFormLoader } from "./../modules/forms/app-form.js";

document.body.addEventListener('submit', async function(e) {
    const element = e.target;

    if (element.classList.contains('submit-form')) {
        e.preventDefault();

        const data = new FormData(element);

        addAppFormLoader(element);

        const response = await post(element.action, { method: element.method, body: data }, element.dataset.type);

        if (response.ok) {

            if (response.reset) element.reset();
            if (response.page) location.href = response.page;
            if (response.notif) showNotif(response.notif.message, response.notif.type);

        } else {
            gestionErrors(element, response);
        }

        removeAppFormLoader(element);
    }
});