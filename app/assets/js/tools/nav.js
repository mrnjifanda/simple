
import { slideUp, slideDown } from '../modules/animations.js';
import { addClass, hasClass, next, prev, parents, select, removeClass } from './../modules/dom.js';

export const nav = select('.nav');
export const sousMenuNavs = select('.sous-menu-link', true, nav);
export const navLinks = select('.js-app-link', true, nav);

export function menuNavSlideUp () {
    const activeNav = select('.js-menu-active', false, nav);
    if (activeNav) {
        slideUp(activeNav);
        activeNav.classList.remove('js-menu-active');
    }
}

export function removeActiveNavLinks () {
    const navActiveLinks = select('.js-app-link.active, .sous-menu-link.active', true, nav);
    navActiveLinks.forEach(navActiveLink => {
        removeClass(navActiveLink, 'active');
    });
}

export function arccordeonNav (menu) {
    const a = menu;
    const menuContent = next(a);

    if (hasClass(a, "active")) {
        if (hasClass(menuContent, "sous-menu-content")) {
            if (hasClass(menuContent, "js-menu-active")) {
                slideUp(menuContent);
                menuContent.classList.remove("js-menu-active")
            } else {
                menuNavSlideUp();
                slideDown(menuContent);
                menuContent.classList.add("js-menu-active");
            }
        } else {
            return false;
        }
    } else {
        if (hasClass(menuContent, "js-menu-active")) {
            slideUp(menuContent);
            menuContent.classList.remove("js-menu-active");
        } else {
            menuNavSlideUp();
            slideDown(menuContent);
            menuContent.classList.add("js-menu-active");
        }
    }
}

export function activeTrueNavLink (element) {
    if (!hasClass(element, "active")) {
        const elementParent = parents(element, 2);
        removeActiveNavLinks();
        if (hasClass(elementParent, 'sous-menu-content')) {
            addClass(prev(elementParent), "active");
            addClass(elementParent, "js-menu-active");
        }
    }
    
    addClass(element, 'active');
}

export function initialiseArccordeonNav () {
    sousMenuNavs.forEach(menu => {
        menu.addEventListener('click', function (e) {
            e.preventDefault();
            arccordeonNav(menu);
        });
    });
}

export function activeTrueLinkBeforeLoad () {

    const pathName = window.location.pathname;
    const realActiveLink = select('a[href="' + pathName + '"]');

    if (realActiveLink && !hasClass(realActiveLink, 'active')) {
        const realActiveLinkParent = parents(realActiveLink, 2);
        removeActiveNavLinks();
        if (hasClass(realActiveLinkParent, 'sous-menu-content')) {
            addClass(prev(realActiveLinkParent), 'active');
            addClass(realActiveLinkParent, 'js-menu-active');
            slideDown(realActiveLinkParent);
        }
        addClass(realActiveLink, 'active');
    }
}