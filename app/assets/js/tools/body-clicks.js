import { select, toggleClass, classReplace, hasClass, removeClass, parent, next } from "./../modules/dom.js";
import { slideDown, slideUp, fadeIn, fadeOut } from "./../modules/animations.js";
import { addEventAppLink } from "./../modules/app-link.js";
import { toFullScreen } from "./../modules/functions.js";


/**
 * js-app-link
 */
addEventAppLink();












/*******************
 * 
 * Fonctionnement de l'application
 * 
 *******************/

// Open and close Slide Bar
toggleClass(select('.js-hamburger'), 'body', 'aside-close');

// Open and mimify aside bottom | Put and remove full screen
const btnOpen = select('.js-ab-open');
const btnfull = select('.js-ab-full');

btnOpen.addEventListener('click', () => {
    let item = select('.aside-bottom').classList;

    if (item.contains('full')) {
        item.remove('full');
        select('.js-ab-full').classList.replace('fa-compress', 'fa-expand');
    }

    item.toggle('ab-open');
    classReplace(btnOpen, 'fa-folder', 'fa-minus');
});

btnfull.addEventListener('click', () => {
    select('.aside-bottom').classList.toggle('full');
    classReplace(btnfull, 'fa-expand', 'fa-compress');
});

select('#app-full-screen').addEventListener('click', (e) => {
    e.preventDefault();
    toFullScreen();
});

/** Clique sur les liens de la page **/
document.body.addEventListener('click', async function(e) {
    const element = e.target;

    /** Fermer les Dropdown après un clique quelconque sur le bureau **/
    if (hasClass(this, 'js-dropdown')) {
        if (!hasClass(element, 'js-dropdown-link')) {
            removeClass(this, 'js-dropdown');
            const activeDropdown = select('.js-dropdown-link.active');
            removeClass(activeDropdown, 'active');
            fadeOut(next(activeDropdown));
        }
    }

    /** Slide Up and Down **/
    if (hasClass(element, 'js-slide')) {
        e.preventDefault();
        const a = element;
        const nextElement = next(a);
        const parentElement = parent(a);

        if (getComputedStyle(nextElement).display === "none") {
            parentElement.style.width = parentElement.offsetWidth + 'px';
            slideDown(nextElement);
        } else {
            slideUp(nextElement);
        }
        return false;
    }

    /** Afficher le Dropdown **/
    if (hasClass(element, 'js-dropdown-link')) {
        e.preventDefault();
        e.stopPropagation();

        const a =  (element.classList.contains('js-dropdown-link')) ? element : element.parentNode;
        const nextElement = a.nextSibling.nextSibling;

        if (hasClass(a, 'active')) {
            a.classList.remove('active');
            fadeOut(nextElement);
            this.classList.remove('js-dropdown');
        } else {
            const dropdowns = select('.js-dropdown-link', true);
            dropdowns.forEach(currentDropdown => {
                if (currentDropdown.classList.contains('active')) {
                    currentDropdown.classList.remove('active');
                }
            });

            const dropdownscontents = select('.dropdown-content, .dropdown-menu', true);
            dropdownscontents.forEach(dropdownscontent => {
                if (dropdownscontent.style.display !== "none") {
                    dropdownscontent.style.display = "none";
                }
            });
    
            a.classList.add('active');
            fadeIn(nextElement);
            document.body.classList.add('js-dropdown');
        }
        return false;
    }

    if (hasClass(element, 'has-error')) {
        removeClass(element, 'has-error');
        next(parent(element)).innerText = '';
    }
});