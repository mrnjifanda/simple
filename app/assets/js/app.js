/** Css **/
import "./../css/app.css";

/** PLugins **/
import "./../plugins/confirm/confirm.js"
import "./../plugins/select/select.js"
import "./../plugins/tabs/tabs.js"

/** Modules **/
import { appInitialise } from "./modules/initialise.js";

/** Tools **/
import "./tools/body-clicks.js";
import "./tools/body-submits.js";

if (window.addEventListener) {
    window.addEventListener('load', appInitialise, false);
} else {
    window.attachEvent('onload', appInitialise);
}
