import { fadeIn } from './animations.js';
import { remove, select } from './dom.js';

export function showAlert (message, titre, image) {
    const alert = new Alert(message, titre, image);
    alert.show();
}

export function showNotif (message, type) {
    const notif = new Notif(message, type);
    notif.show();
}

export function closeNotifItem (notif, time = 5, animation = "jifi-notif-end") {
    setTimeout(() => notif.style.animation = ".3s " + animation + " linear", time * 1000);
    setTimeout(() => remove(notif), (time * 1000) + 200);
}

export class Notif {

    /**
     * Initialisation de la class
     * @param {string} message 
     * @param {string} type 
     */
    constructor(message, type = 'info') {
        this.message = message;
        this.type = type;

        switch (this.type) {
            case 'danger':
                this.icon = 'exclamation-circle';
                break;

            case 'success':
                this.icon = 'check-circle';
                break;

            case 'warning':
                this.icon = 'exclamation';
                break;
        
            default:
                this.icon = 'bell';
                break;
        }

        this.div = this.createNotif();
    }

    createNotif () {
        let div = document.createElement('div');
        div.setAttribute('class', 'jifi-notif-item ' + this.type);
        div.setAttribute('role', 'alert');

        div.innerHTML = '<i class="fa fa-' + this.icon + '"></i> ' + this.message + ' <div class="jifi-notif-progress-bar"></div>';
        return div;
    }

    show () {

        if (!select('.jifi-notif-container')) {
            const div = document.createElement('div');
            div.setAttribute('class', 'jifi-notif-container');
            div.setAttribute('role', 'aside');
            document.body.prepend(div);
        }

        const container = select('.jifi-notif-container');
        container.prepend(this.div);
        const notif = container.childNodes[0];
        notif.addEventListener('click', () => closeNotifItem(notif, 0));
        closeNotifItem(notif);
    }
}

export class Alert {
    /**
     * Initialisation de la class
     * @param {string} message 
     * @param {string} titre 
     * @param {string} image 
     */
     constructor(message, titre, image) {
        this.message = message;
        this.titre = titre;
        this.image = image;

        this.div = this.createAlert();
    }

    createAlert () {
        let div = document.createElement('div');
        div.setAttribute('class', 'jifi-alert-item');
        div.setAttribute('role', 'alert');
        div.innerHTML = `<i class="jifi-alert-close fa fa-times"></i><div class="flex justify-between"><img src="${this.image}" class="jifi-alert-image"><div class="jifi-alert-text"><span class="jifi-alert-text-title">${this.titre}</span><p>${this.message}</p></div></div>`;
        return div;
    }

    show () {

        if (!select('.jifi-alert-container')) {
            const div = document.createElement('div');
            div.setAttribute('class', 'jifi-alert-container');
            div.setAttribute('role', 'aside');
            document.body.prepend(div);
        }

        const container = select('.jifi-alert-container');
        container.prepend(this.div);
        fadeIn(this.div);
        select('.jifi-alert-close', false, this.div).addEventListener('click', () => remove(this.div));
    }
}