import { fadeIn } from './animations.js';
import { select, remove } from './dom.js';

export function showModal (icon, title, content, width = null) {
    const modal = new Modal(icon, title, content, width);
    modal.show();
}

export function closeModalItem (modal) {
    remove(modal);
}

export class Modal {
    /**
     * @param  {string} icon
     * @param  {string} title
     * @param  {HTMLElement} content
     * @param  {string|null} width=null
     */
    constructor(icon, title, content, width = null) {
        this.icon = icon;
        this.title = title;
        this.content = content;
        this.width = width;
        this.modal = this.createModal();
    }

    createModal () {

        const modal = document.createElement('div');
        modal.setAttribute('class', 'jifi-modal-item');
        modal.setAttribute('role', 'modal');

        const content =
        '<div class="jifi-modal-content"' + (this.width ? ' style="max-width: ' + this.width + '"' : null) + '>\
            <div class="content-title">\
                <h1><i class="fa fa-' + this.icon + '"></i> ' + this.title + '</h1>\
            </div>\
            <div style="height: calc(100% - 40px); overflow-y: auto; padding: 2px;">' + this.content + '</div>\
            <a class="jifi-modal-close js-jifi-modal-close">Fermer</a>\
        </div>';

        modal.innerHTML = content;
        return modal;
    }

    show () {
        document.body.prepend(this.modal);
        fadeIn(this.modal);
        select('a.js-jifi-modal-close', false, this.modal).addEventListener('click', () => closeModalItem(this.modal));
    }
}