import { select } from './dom.js';
import { fadeOut } from './animations.js';
import { showAlert, showNotif } from './notifications.js';
import { initialiseArccordeonNav, activeTrueLinkBeforeLoad } from './../tools/nav.js';

export function defaultInitialse () {
    const element = select('.js-loader-page');
    if (element) {
        fadeOut(element);
    }

    if (typeof getFlashes !== "undefined") {
        let i = 0;
        let timer = setInterval(showFlashes, 2500);

        function showFlashes () {

            if (i < getFlashes.length) {
                const getFlashe = getFlashes[i];
                showNotif(getFlashe.message, getFlashe.type);
                i++;
            } else {
                clearInterval(timer);
            }

        }
    }










        const InputPasswordIcons = {
                'show': '<svg width="16" height="16" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg" focusable="false" role="img" aria-hidden="true"><path d="M15.98 7.873c.013.03.02.064.02.098v.06a.24.24 0 01-.02.097C15.952 8.188 13.291 14 8 14S.047 8.188.02 8.128A.24.24 0 010 8.03v-.059c0-.034.007-.068.02-.098C.048 7.813 2.709 2 8 2s7.953 5.813 7.98 5.873zm-1.37-.424a12.097 12.097 0 00-1.385-1.862C11.739 3.956 9.999 3 8 3c-2 0-3.74.956-5.225 2.587a12.098 12.098 0 00-1.701 2.414 12.095 12.095 0 001.7 2.413C4.26 12.043 6.002 13 8 13s3.74-.956 5.225-2.587A12.097 12.097 0 0014.926 8c-.08-.15-.189-.343-.315-.551zM8 4.75A3.253 3.253 0 0111.25 8 3.254 3.254 0 018 11.25 3.253 3.253 0 014.75 8 3.252 3.252 0 018 4.75zm0 1C6.76 5.75 5.75 6.76 5.75 8S6.76 10.25 8 10.25 10.25 9.24 10.25 8 9.24 5.75 8 5.75zm0 1.5a.75.75 0 100 1.5.75.75 0 000-1.5z"></path></svg>',

                'hide': '<svg width="16" height="16" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg" focusable="false" role="img" aria-hidden="true"><path d="M5.318 13.47l.776-.776A6.04 6.04 0 008 13c1.999 0 3.74-.956 5.225-2.587A12.097 12.097 0 0014.926 8a12.097 12.097 0 00-1.701-2.413l-.011-.012.707-.708c1.359 1.476 2.045 2.976 2.058 3.006.014.03.021.064.021.098v.06a.24.24 0 01-.02.097C15.952 8.188 13.291 14 8 14a7.03 7.03 0 01-2.682-.53zM2.04 11.092C.707 9.629.034 8.158.02 8.128A.24.24 0 010 8.03v-.059c0-.034.007-.068.02-.098C.048 7.813 2.709 2 8 2c.962 0 1.837.192 2.625.507l-.78.78A6.039 6.039 0 008 3c-2 0-3.74.956-5.225 2.587a12.098 12.098 0 00-1.701 2.414 12.11 12.11 0 001.674 2.383l-.708.708zM8.362 4.77L7.255 5.877a2.262 2.262 0 00-1.378 1.378L4.77 8.362A3.252 3.252 0 018.362 4.77zm2.86 2.797a3.254 3.254 0 01-3.654 3.654l1.06-1.06a2.262 2.262 0 001.533-1.533l1.06-1.06zm-9.368 7.287a.5.5 0 01-.708-.708l13-13a.5.5 0 01.708.708l-13 13z"></path></svg>'
        }

        const passwords = document.querySelectorAll(".show-hide-password")

        if (passwords) {
                passwords.forEach(password => {
                        password.addEventListener('click', e => {
                                e.preventDefault()
                                const parent = password.parentNode
                                const input = parent.querySelector("input")

                                if (input.type.toLowerCase() === "password") {
                                        input.type = "text"
                                        password.innerHTML = InputPasswordIcons.hide
                                } else {
                                        input.type = "password"
                                        password.innerHTML = InputPasswordIcons.show
                                }
                        })
                })
        }















    
}

export function pageInitialise () {
    defaultInitialse();
}

export function appInitialise () {
    initialiseArccordeonNav();
    activeTrueLinkBeforeLoad();
    defaultInitialse();

    setTimeout(() => showAlert('Nous sommes heureux de vous revoir. N\'hésitez pas a nous contacter pour tous les problèmes rencontrés.', 'Bienvenue sur EasyTalk!', '/images/logo/easytalk/logo-48.png'), 5000);
}