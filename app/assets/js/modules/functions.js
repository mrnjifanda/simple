/**
 * updateCurrentPageHistory
 * 
 * @date 2021-05-06
 * @param {string} url
 * @param {sstring} title
 * @returns {void}
 */
export function updateCurrentPageHistory(url, title) {
    history.replaceState({
        path: url
    }, title, url);
}

/**
 * changeTitlePage
 * 
 * @date 2021-05-06
 * @param {string} title
 * @returns {void}
 */
export function changeTitlePage(title) {
    document.title = `${title} | easyTalk`;
}

/**
 * toFullScreen
 * Passer en mode plein ecran
 * 
 * @date 2021-05-06
 * @param {HTMLElement|null} elem
 * @returns {void}
 */
export function toFullScreen(elem) {
    const monElement = elem || document.querySelector('body');

    if (document.mozFullScreenEnabled) {
        if (!document.mozFullScreenElement) {
            monElement.mozRequestFullScreen();
        } else {
            document.mozCancelFullScreen();
        }
    }

    if (document.fullscreenElement) {
        if (!document.fullscreenElement) {
            monElement.requestFullscreen();
        } else {
            document.exitFullscreen();
        }
    }

    if (document.webkitFullscreenEnabled) {
        if (!document.webkitFullscreenElement) {
            monElement.webkitRequestFullscreen();
        } else {
            document.webkitExitFullscreen();
        }
    }

    if (document.msFullscreenEnabled) {
        if (!document.msFullscreenElement) {
            monElement.msRequestFullscreen();
        } else {
            document.msExitFullscreen();
        }
    }
}

/**
 * redirect
 * 
 * @date 2021-05-06
 * @param {string} url
 * @param {boolean} history=true
 * @returns {void}
 */
export function redirect(url, history = true) {
    if (history) {
        window.location = url;
    } else {
        window.location.replace(url);
    }
}