import { showNotif } from './../notifications.js';
import { remove, select, next, parent, addClass } from './../dom.js';

export function addAppFormLoader(element) {
    const elements = select('input, textarea, button, select, a', true, element);
    const button = select('button[type="submit"]', false, element);

    elements.forEach(elem => elem.setAttribute('disabled', 'true'));

    const span = document.createElement('span');
    span.innerHTML = '<i class="fa fa-spinner fa-pulse"></i>';
    button.prepend(span);
}

export function removeAppFormLoader(element) {
    const elements = select('input, textarea, button, select, a', true, element);
    const button = select('button[type="submit"]', false, element);

    elements.forEach(elem => elem.removeAttribute('disabled'));
    remove(select("span", false, button));
}

export function addErrorMessage (element, name, value) {
    const input = select('.' + name, false, element);
    next(parent(input)).innerText = value;
    addClass(input, 'has-error');
}

export function gestionErrors (element, response) {
    const errorForm = response.errorForm;
    const error = response.error;

    if (error) showNotif(error.message, error.type);
    if (errorForm) {
        for (let rs in errorForm) {
            addErrorMessage(element, rs, errorForm[rs]);
        }
    }
}

export function addElement (params) {
    const tag = select(params.element);

    if (!tag) return false;

    if (params.empty) {
        tag.innerHTML = params.value;
    } else {
        if (params.append) {
            tag.innerHTML += params.value;
        } else {
            tag.innerHTML = params.value + tag.innerHTML;
        }
    }
}