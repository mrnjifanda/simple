import { fadeIn } from "./animations.js";

export function select (tag, all = false, reference = null) {

    if (reference) {
        if (all === true) {
            return reference.querySelectorAll(tag);
        }
    
        return reference.querySelector(tag);
    }

    if (all === true) {
        return document.querySelectorAll(tag);
    }

    return document.querySelector(tag);
}

export function next (element) {
    return element.nextElementSibling;
}

export function nexts (element, order) {
    let nextElement = element;
    for (let i = 0; i < order; i++) {
        nextElement = next(nextElement);
    }
    return nextElement;
}

export function prev (element) {
    return element.previousElementSibling;
}

export function prevs (element, order) {
    let prevElement = element;
    for (let i = 0; i < order; i++) {
        prevElement = prev(prevElement);
    }
    return prevElement;
}

export function parent (element) {
    return element.parentNode;
}

export function parents (element, order) {
    let parentElement = element;
    for (let i = 0; i < order; i++) {
        parentElement = parent(parentElement);
    }
    return parentElement;
}

export function hasClass (element, className) {
    return element.classList.contains(className) || element.parentNode.classList.contains(className);
}

export function addClass (element, className) {
    if (!hasClass(element, className)) {
        element.classList.add(className);
    }
}

export function removeClass (element, className) {
    if (hasClass(element, className)) {
        element.classList.remove(className);
    }
}

export function toggleClass (element, classContent, classElement) {
    if (!element) {
        return false
    }

    element.addEventListener('click', function() {
        select(classContent).classList.toggle(classElement);
    })
}

export function classReplace (check, checkClass, classChange) {

    let item = check.classList;

    if (item.contains(checkClass)) {
        item.replace(checkClass, classChange)
    } else {
        item.replace(classChange, checkClass)
    }
}

export function remove (element) {
    element.parentNode.removeChild(element);
}

export function showContent (data, tag, animation = false) {
    const getContainer = select(tag);
    if (getContainer) {
        if (animation === true) {
            fadeIn(getContainer);
        }
        getContainer.innerHTML = data;
    }
}

export function getParseData (data, tag) {

    /*const doc = new DOMParser();
    doc.parseFromString(data, 'text/html');
    const element = select(tag, false, doc.body);*/

    const div = document.createElement('div');
    div.innerHTML = data;
    const element = div.querySelector(tag);

    return element ? element.innerHTML : null;
}