import { select, showContent, getParseData } from "./dom.js";
import { updateCurrentPageHistory, changeTitlePage, redirect } from "./functions.js";
import { get } from "./fetch.js";
import { showNotif } from './notifications.js';
import { showModal } from './modal.js';
import { activeTrueNavLink } from '../tools/nav.js';

export let active_page = {};

export function addLoader (a, type) {
    const loader = select((type === 'nav' ? '.big-load' : '.js-small-load'));
    loader.style.display = "block";
    a.setAttribute('aria-disabled', 'true');
    a.href = '';
}

export function removeLoader (a, type, url) {
    const loader = select((type === 'nav' ? '.big-load' : '.js-small-load'));
    loader.style.display = "none";
    a.removeAttribute('aria-disabled');
    a.href = url;
}

export function lockApp (content) {

    active_page = {
        "url": location.href,
        "title": document.title,
        "content": document.body
    }

    updateCurrentPageHistory('/lock', 'Application Verouille');
    showContent(content, 'body', true);
    changeTitlePage('Application Verouille');
}

export async function appLink(a) {

    const type = a.dataset.type;
    const url = a.href;
    const title = a.innerText;

    addLoader(a, type);

    const response = await get(url);

    if (response.ok === true) {
        const data = response.data;

        if (type === "nav") {
            activeTrueNavLink(a);
            updateCurrentPageHistory(url, title);
            showContent(data, '#js-show-nav', true);
            changeTitlePage(title);
        }

        if (type === "modal") {
            const resultat = JSON.parse(data);
            const width = resultat.width ?? null;
            showModal(resultat.icon, resultat.title, resultat.content, width);
        }

        if (type === "lock-screen") {
            lockApp(data);
        }

        if (type === "show-page") {

            const container = a.dataset.container;
            const change = a.dataset.change;
            const parseData = getParseData(data, container);

            showContent(parseData, container, true);

            if (change !== 'no') {
                const pageTitle = a.dataset.title ?? title;
                updateCurrentPageHistory(url, pageTitle);
                changeTitlePage(pageTitle);
            }
        }

    } else {
        console.log(response)
        console.log(response.data)

        if (response.data) {
            const data = response.data;
            const splitData = data.split(':')
            
            if (splitData[0] === 'redirect') redirect(splitData[1])
        }

        showNotif(response.error.message, response.error.type);
    }

    removeLoader(a, type, url);
}

export function addEventAppLink () {

    document.body.addEventListener('click', function(e) {
        let element = e.target;
        while (element) {
            if (element.classList.contains('js-app-link')) {
                e.preventDefault();
                appLink(element);
                element = false;
                break;
            } else if (element.tagName === "BODY") {
                element = false;
                break;
            } else {
                element = element.parentNode;
            }
        }
        //const paths = e.path;
        // const elementPaths =  paths.splice(0, paths.length - 4);

        // for (let i = 0; i < elementPaths.length; i++) {
        //     const elementPath = elementPaths[i];
        //     if (elementPath.classList.contains('js-app-link')) {
        //         e.preventDefault();
        //         appLink(elementPath);
        //         break;
        //     }
        // }
    });

}