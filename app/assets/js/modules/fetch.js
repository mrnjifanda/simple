export const HTTP_OK = 200;
export const HTTP_NO_CONTENT = 204;
export const HTTP_MOVED_PERMANENTLY = 301;
export const HTTP_FORBIDDEN = 403;
export const HTTP_NOT_FOUND = 404;
export const HTTP_UNPROCESSABLE_ENTITY = 422;
export const HTTP_INTERNAL_SERVER_ERROR = 500;

export const IS_FETCH = window.fetch ?? null;

/** Return Error **/
export function PAGE_ERROR (message, type, data = null) {
    return {
        "ok": false,
        "data": data,
        "error": { "type": type, "message": message }
    }
}

export function paramsFetch (params = {}) {
    return {
        headers: {
            'X-Requested-With': 'XMLHttpRequest',
            // 'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').attr('content')
        },
        ...params
    }
}

/**
 * @param {RequestInfo<string>} url
 * @param params
 * @return {Promise<Object>}
 */
export async function jsonFetch (url, params = {}) {

    params = paramsFetch(params);
    const response = await fetch(url, params);
    const status = response.status;

    if (status === HTTP_NOT_FOUND) return PAGE_ERROR("Page introuvable", "danger");

    if (status === HTTP_INTERNAL_SERVER_ERROR) return PAGE_ERROR("Internal Server Error", "warning");

    const data = await response.json();

    if (status === HTTP_FORBIDDEN) {
        if (data.error) {
            data.ok = false;
            return data;
        }
        return PAGE_ERROR("Contenu inaccessible", "warning", data);
    }

    if (status === HTTP_UNPROCESSABLE_ENTITY) {
        if (data.error) {
            data.ok = false;
            return data;
        }
        return PAGE_ERROR("Une erreur c'est produite", "warning", data);
    }

    if (response.ok && response.status === HTTP_OK) {
        return {
            "ok": true,
            ...data
        };
    }

    return PAGE_ERROR("Erreur inconnue (" + status + ")", "warning");
}

/**
 * @param {RequestInfo<string>} url
 * @param params
 * @return {Promise<Object>}
 */
export async function textFetch (url, params = {}) {

    params = paramsFetch(params);
    const response = await fetch(url, params);
    const status = response.status;

    if (status === HTTP_NOT_FOUND) return PAGE_ERROR("Page introuvable", "danger");

    if (status === HTTP_INTERNAL_SERVER_ERROR) return PAGE_ERROR("Internal Server Error", "warning");

    const data = await response.text();

    if (response.status === HTTP_UNPROCESSABLE_ENTITY) return PAGE_ERROR("Une erreur c'est produite", "warning", data);

    if (status === HTTP_FORBIDDEN) {
        if (data.error) {
            data.ok = false;
            return data;
        }
        return PAGE_ERROR("Contenu inaccessible", "warning", data);
    }

    if (response.ok && status === HTTP_OK) {
        return {
            "ok": true,
            "data" : data
        }
    }

    return PAGE_ERROR("Erreur inconnue (" + status + ")", "warning");
}

export async function get(url, params = {}, type) {
    if (type === "json") {
        return await jsonFetch(url, params);
    }

    return await textFetch(url, params);
}

export async function post (url, params = {}) {
    return await jsonFetch(url, params);
}
/*
    method: 'GET'
    method: 'POST'
    method: 'PUT'
    method: 'DELETE'
*/