/** Css **/
import "./../css/pages.css";

/** Modules **/
import { hasClass, next, parent, removeClass, showContent } from "./modules/dom.js";
import { updateCurrentPageHistory, changeTitlePage } from "./modules/functions.js";
import { textFetch } from "./modules/fetch.js";
import { showNotif } from './modules/notifications.js';
import { pageInitialise } from "./modules/initialise.js";

/** Tools **/
import "./tools/submit-form.js";

if (window.addEventListener) {
    window.addEventListener('load', pageInitialise, false);
} else {
    window.attachEvent('onload', pageInitialise);
}

/** Clique sur les liens de la page **/
document.body.addEventListener('click', async function(e) {
    const element = e.target;
    if (hasClass(element, 'js-app-link')) {
        e.preventDefault();
        const url = element.href;
        const title = element.innerText;
        const response = await textFetch(url);

        if (response.ok) {
            updateCurrentPageHistory(url, title);
            showContent(response.data, '#js-content-data', true);
            changeTitlePage(title);
        } else {
            const error = response.error;
            showNotif(error.message, error.type);
        }
    }

    if (hasClass(element, 'has-error')) {
        removeClass(element, 'has-error');
        next(parent(element)).innerText = '';
    }
});