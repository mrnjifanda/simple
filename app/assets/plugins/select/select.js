const jifiSelected = async function (element, url) {

    let selected = element.querySelector('select');

    try {
        const response = await fetch(url, {headers: {'X-Requested-With': 'XMLHttpRequest'}});

        if (response.ok) {

            const data = JSON.parse(await response.text());
            
            selected.options.length = 0;

            if (data.error) {

                selected.options[0] = new Option('-- Pas de sélection --', 0, false, false);
            } else {

                selected.options[0] = new Option('-- Choisir --', '-- Choisir --', false, false);
                for (let i in data.content) {
                    const content = data.content[i];
                    selected.options[(1*i)+1] = new Option(content.name, content.id, false, false);
                }
            }
                
        } else {
            selected.options.length = 0;
            selected.options[0] = new Option('-- Pas de sélection --', 0, false, false);
            console.log(response.status);
        }

    } catch (e) {
        console.log(e);
    }
}

document.body.addEventListener('change', function(e) {

    const elem = e.target;

    if (elem.tagName === 'SELECT' && elem.parentNode.parentNode.classList.contains('js-jifi-select')) {

        const target =  document.querySelector(elem.dataset.target),
              url = elem.dataset.url+'?id='+elem.value;

        jifiSelected(target, url);
    }
});