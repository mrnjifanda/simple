class Confirm {

    constructor (url, icon = 'trash', text = 'Êtes vous sur de vouloir supprimer définitivement') {
        this.url = url;
        this.text = text;
        this.icon = icon;
        this.confirm = this.createConfoirm();
    }

    createConfoirm () {
        let confirm = document.createElement('div'),
            content = 
            '<div class="jifi-confirm-item">\
                <div class="jifi-confirm-head">\
                    <h4>Confirmer</h4>\
                    <a href="#" class="js-close-confirm"><i class="fa fa-times"></i></a>\
                </div>\
                <div class="jifi-confirm-content">\
                    <div class="jifi-confirm-content-left"> <i class="fa fa-' + this.icon + '"></i></div>\
                    <div class="jifi-confirm-content-right">\
                        ' + this.text + '?\
                    </div>\
                </div>\
                <div class="jifi-confirm-body">\
                    <a href="#" class="js-close-confirm">Non</a>\
                    <form action="' + this.url + '" data-action="' + this.url + '" class="js-app-form js-confirm-confirm" method="POST" data-json="json"><button type="submit"><i class="fa fa-' + this.icon + '"></i> Oui</button></form>\
                </div>\
            </div>';

        confirm.setAttribute('class', 'jifi-confirm');
        confirm.innerHTML = content;
        return confirm;
    }

    show () {
        document.body.prepend(this.confirm);
        int(this.confirm);

        /*const action = this.confirm.querySelector(".js-confirm-confirm").addEventListener('submit', function(e) {
            e.preventDefault();
            console.log($(this));
            console.log(this);
            //new POST($(this), {
                //Post: true
            //});
        })*/
    }
}