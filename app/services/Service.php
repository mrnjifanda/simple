<?php

namespace Service;

use App\Errors\Errors;
use Repositories\BaseRepository;

class Service {

    protected $base_repository = null;

    private function getBaseRepository (?BaseRepository $base_repository = null)
    {

        if (is_null($this->base_repository) && is_null($base_repository)) {

            throw new Errors('Code error', '$base_repository variable not defined');
        }
        return $base_repository !== null ? $base_repository : $this->base_repository;
    }

    public function create(array $datas, bool $return = false, ?BaseRepository $base_repository = null)
    {

        return $this->getBaseRepository($base_repository)->create($datas, $return);
    }

    public function all(string|array $query = '*', ?BaseRepository $base_repository = null)
    {

        return $this->getBaseRepository($base_repository)->all($query);
    }

    public function find(int|array $query, ?BaseRepository $base_repository = null)
    {

        return $this->getBaseRepository($base_repository)->find($query);
    }

    public function update (int $id, array $datas, bool $return = true, ?BaseRepository $base_repository = null)
    {

        return $this->getBaseRepository($base_repository)->update($id, $datas, $return);
    }

    public function delete (int $id, array $wheres, ?BaseRepository $base_repository = null)
    {

        return $this->getBaseRepository($base_repository)->delete($id, $wheres);
    }

    public function paginated(int $page, string|array $query = '*', int $per_page = 25, ?BaseRepository $base_repository = null)
    {

        return $this->getBaseRepository($base_repository)->paginated($page, $query, $per_page);
    }

    public function count(array $where = [], ?BaseRepository $base_repository = null): ?int
    {

        return $this->getBaseRepository($base_repository)->count($where);
    }
}
