<?php

namespace Service\Users;

use App\Users\Auth;
use Service\Service;
use Service\Send\MailService;
use Repositories\BaseRepository;

class UserService extends Service {

    /**
     * @var Auth
     */
    private $auth;

    /**
     * @var MailService
     */
    private $mail_service;

    public function __construct(Auth $auth, bool $push_in_queue = true)
    {

        $this->base_repository = new BaseRepository('users');
        $this->mail_service = new MailService($push_in_queue);
        $this->auth = $auth;
    }

    public function forgetPassword(string $email): bool
    {

        $auth = $this->auth->sendForgetPassword($email);
        if ($auth) {

            $this->mail_service->forgetPassword($email, $auth['username'], $auth['token']);
            return true;
        }
        return false;
    }
}
