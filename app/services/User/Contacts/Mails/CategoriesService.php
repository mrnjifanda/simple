<?php

namespace Service\User\Contacts\Mails;

use Service\User\Contacts\ContactsService;
use Model\Contacts\Mails\CategorieModel;

class CategoriesService extends ContactsService {
    protected static $table = 'contacts_mail_categories';
    protected static $classMapings = CategorieModel::class;
}
