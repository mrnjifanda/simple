<?php

namespace Service;

use App\App;
use App\Logs\Logs;
use App\Helpers\Date;
use App\Helpers\Sessions;

class Service
{
    protected static $table;
    protected static $classMapings;
    
    /**
     * request
     *
     * @param  string $method
     * @param  array $where
     * @param  mixed $select
     * @param  mixed $customSQL
     * @return mixed
     */
    private static function request (string $method, string $select, array $where, ?string $customSQL = null)
    {
        return App::$app->$method(static::$table, static::$classMapings, $select, $where, $customSQL);
    }

    public static function all(string $select = "*", array $where = [], ?string $customSQL = null)
    {
        return self::request('all', $select, $where, $customSQL);
    }

    public static function find(?string $select = "*", array $where = [], ?string $customSQL = null)
    {
        return self::request('find', $select, $where, $customSQL);
    }

    public static function count(?array $where = []): ?int
    {
        return App::$app->slimCount(static::$table, array_merge($where, ['supprimer' => 'NULL']));
    }

    public static function add (array $set, ?bool $return = true)
    {
        App::$app->insert(static::$table, $set, true);

        $item = self::find("*", ['id' => App::$app->lastId()]);
        self::logsContent('INSERT', $item->getId());

        if ($return) {
            return $item;
        }
    }

    public static function edit (int $id, array $sets, array $wheres, ?bool $return = true)
    {
        $item =  App::$app->findArray(static::$table, ['id' => $id]);

        if (!$item) { return false; }

        self::logsContent('UPDATE', $id, $item);
        // App::$app->update(static::$table, $sets, $wheres);

        if ($return) {
            return self::find("*" , ['id' => $id]);
        }
    }

    public static function delete (int $id, array $wheres)
    {
        $item = self::find("*" , ['id' => $id]);

        if (!$item) {
            return false;
        }
    
        self::logsContent('DELETE', $id);
        App::$app->delete(static::$table, $wheres);
    }

    public static function logsContent(string $action, int $id_item, ?array $data = null): void
    {
        $content = [
            "action" => $action,
            "table" => static::$table,
            "id_user" => Sessions::$sessions->read('auth'),
            "id_item" => $id_item,
            "created_at" => Date::toDay(),
            "data" => $data ?? App::$app->findArray(static::$table, ['id' => $id_item])
        ];

        Logs::setFileLogs($content);
    }
}
