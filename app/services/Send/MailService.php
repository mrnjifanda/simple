<?php

namespace Service\Send;

use App\Mails\Mails;
use App\Helpers\Date;
use App\Logs\EmailsLogs;
use Service\Queues\QueuesService;

class MailService {

    /**
     * @var bool
     */
    private $in_queue;

    /**
     * @var string
     */
    private $queue = 'easytalk_queue';

    public function __construct(bool $in_queue = true)
    {

        $this->in_queue = $in_queue;
    }

    private function setDatas(string $categorie, string $action, array $datas)
    {
        return [
            'categorie' => $categorie,
            'action' => $action,
            'datas' => $datas
        ];
    }

    private function send(string $sujet, string $message, ?string $email = null, ?string $name = null, array $variables = []): void
    {

        $status = Mails::mail($sujet, $message, $email, $name, $variables);
        $datas = [
            'sujet' => $sujet,
            'message' => $message,
            'email' => $email,
            'name' => $name,
            'variables' => $variables,
            'status' => $status,
            'created_at' => Date::toDay(),
        ];
        EmailsLogs::setFileLogs($datas);
    }

    public function register (string $email, string $token): void
    {

        if ($this->in_queue === true) {

            $datas = $this->setDatas('mails', 'register', compact('email', 'token'));
            QueuesService::push($this->queue, $datas);
        } else {

            $this->send(__('mails.register.subject'), 'auth.register', $email, null, ['token' => $token]);
        }
    }

    public function forgetPassword (string $email, string $username, string $token): void
    {

        if ($this->in_queue === true) {

            $datas = $this->setDatas('mails', 'forgetPassword', compact('email', 'username', 'token'));
            QueuesService::push($this->queue, $datas);
        } else {

            $this->send(
                __('mails.forgetPassword.subject'),
                'auth.forgetPassword',
                $email,
                null,
                ['email' => $email, 'username' => $username, 'token' => $token]
            );
        }
    }
}
