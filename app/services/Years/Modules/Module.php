<?php
namespace App\EasyTalk\Years\Modules;
use App\App;
use \PDO;
use \DateTime;

    class Module {

        private $id;
        private $id_year;
        private $module;
        private $debut;
        private $fin;
        private $created_at;


        public function getID (): int
        {
            return $this->id;
        }

        public function getIDYear (): int
        {
            return $this->id_year;
        }

        public function getModule (): string
        {
            return $this->module;
        }

        public function getDebut (): ?string
        {
            return $this->debut;
        }

        public function getFin (): ?string
        {
            return $this->fin;
        } 
        
        public function getCreated (): ?string
        {
            return $this->created_at;
        }

        public function format (int $type): ?string
        {
            $date = ($type === 1) ? new DateTime($this->debut) : new DateTime($this->fin);
            return $date->format('d M Y');
        }
/*
        public function getStudents (App $app): ?int
        {
            $students = $app->query('SELECT * FROM easytalk_students WHERE id_year = :id_year AND id_class = :id_class ORDER BY nom, prenom', ['id_year' => $this->id_year, 'id_class' => $this->id])
                         ->fetchAll(PDO::FETCH_CLASS, Classe::class);
            return $total;
        }

        public function countStudents (App $app): ?int
        {
            $total = $app->count('SELECT COUNT(*) FROM easytalk_students WHERE id_year = :id_year AND id_class = :id_class', ['id_year' => $this->id_year, 'id_class' => $this->id]);
            return $total;
        }

        public function countFille (App $app): ?int
        {
            $fille = $app->query('SELECT COUNT(*) FROM easytalk_students WHERE id_year = :id_year AND id_class = :id_class AND sexe = :sexe', ['id_year' => $this->id_year, 'id_class' => $this->id, 'sexe' => 'F'])
                         ->fetch(PDO::FETCH_NUM)[0];
            return $fille;
        }

        public function countGarcon (App $app): ?int
        {
            $garcon = $app->query('SELECT COUNT(*) FROM easytalk_students WHERE id_year = :id_year AND id_class = :id_class AND sexe = :sexe', ['id_year' => $this->id_year, 'id_class' => $this->id, 'sexe' => 'M'])
                         ->fetch(PDO::FETCH_NUM)[0];
            return $garcon;
        }
*/
    }