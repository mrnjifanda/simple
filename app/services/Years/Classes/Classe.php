<?php
namespace App\EasyTalk\Years\Classes;
use App\App;
use \PDO;

    class Classe {

        private $id;
        private $id_year;
        private $classe;
        private $cycle;
        private $prix;
        private $created_at;


        public function getID (): int
        {
            return $this->id;
        }

        public function getIDYear (): int
        {
            return $this->id_year;
        }

        public function getClasse (): string
        {
            return $this->classe;
        }

        public function getCycle (): int
        {
            return $this->cycle;
        }

        public function getPrix (): int
        {
            return $this->prix;
        }

        public function getStudents (App $app): ?int
        {
            $students = $app->query('SELECT * FROM easytalk_students WHERE id_year = :id_year AND id_class = :id_class ORDER BY nom, prenom', ['id_year' => $this->id_year, 'id_class' => $this->id])
                         ->fetchAll(PDO::FETCH_CLASS, Classe::class);
            return $total;
        }

        public function countStudents (App $app): ?int
        {
            $total = $app->count('SELECT COUNT(*) FROM easytalk_students WHERE id_year = :id_year AND id_class = :id_class', ['id_year' => $this->id_year, 'id_class' => $this->id]);
            return $total;
        }

        public function countFille (App $app): ?int
        {
            $fille = $app->query('SELECT COUNT(*) FROM easytalk_students WHERE id_year = :id_year AND id_class = :id_class AND sexe = :sexe', ['id_year' => $this->id_year, 'id_class' => $this->id, 'sexe' => 'F'])
                         ->fetch(PDO::FETCH_NUM)[0];
            return $fille;
        }

        public function countGarcon (App $app): ?int
        {
            $garcon = $app->query('SELECT COUNT(*) FROM easytalk_students WHERE id_year = :id_year AND id_class = :id_class AND sexe = :sexe', ['id_year' => $this->id_year, 'id_class' => $this->id, 'sexe' => 'M'])
                         ->fetch(PDO::FETCH_NUM)[0];
            return $garcon;
        }
    }