<?php
namespace App\EasyTalk\Years\Matieres;

use App\EasyTalk\Years\Classes\Classe;
use App\App;

    class Matiere {
        private $id;
        private $id_year;
        private $id_class;
        private $id_prof;
        private $matiere;
        private $coef;
        private $groupe;
        private $created_at;

        public function getID (): ?int
        {
            return $this->id;
        }

        public function getIDYear (): ?int
        {
            return $this->id_year;
        }

        public function getIDClass (): ?int
        {
            return $this->id_class;
        }

        public function getIDProf (): ?int
        {
            return $this->id_prof;
        }

        public function getMatiere (): ?string
        {
            return $this->matiere;
        }

        public function getCoef (): ?int
        {
            return $this->coef;
        }

        public function getGroup (): ?string
        {
            if ($this->groupe == 1) {
                return  'Scientifique';
            } elseif ($this->groupe == 2) {
                return 'Littéraire';
            }
            return 'Autre';
        }

        public function getClass (App $app): ?Classe
        {
            $classe = $app->fetch('SELECT * FROM _easytalk_classes WHERE id_year = :id_year AND id = :id', ['id_year' => $this->id_year, 'id' => $this->id], Classe::Class);
            return $classe;
        }
    }