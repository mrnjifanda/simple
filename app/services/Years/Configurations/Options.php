<?php
namespace App\EasyTalk\Years\Configurations;
use App\App;
use App\EasyTalk\Years\Classes\Classe;

use \PDO;

    class Options {

        public $id;	
        public $section;	
        public $options;	
        public $created_at;	
        public $year;


        /*private $app;

        public function __construct (?App $app = null)
        {
            $this->app = $app ? $app : new App(true);
        }*/

        public function getSection (App $app)
        {
            return $app->fetch("SELECT * FROM config_sections WHERE id = :id AND year = :year", ['id' => $this->section, 'year' => $this->year]);
        }

        public function countOptions (App $app, int $year): ?int
        {
            return $app->count("SELECT COUNT(id) FROM config_options WHERE year = :year", ['year' => $year]);
        }

        public function select (App $app, ?int $section = null, ?int $year = null, ?string $options = null)
        {
            if ($year && $section && $options) {
                return $app->fetch("SELECT * FROM config_options WHERE year = :year AND  section = :section AND options = :options", ['year' => $year, 'section' => $section, 'options' => $options], Options::class);
            }

            if ($year && $section) {
                return $app->fetch("SELECT * FROM config_options WHERE year = :year AND section = :section", ['year' => $year, 'section' => $section], Options::class);
            }

            return $app->fetch("SELECT * FROM config_options WHERE year = :year ORDER BY created_at DESC LIMIT 0,1", ['year' => $this->year], Options::class);
        }

        public function addOptions (App $app, int $year, int $section, string $options)
        {
            $id = $app->fetch("SELECT id FROM config_sections WHERE id = :section AND year = :year", ['section' => $section, 'year' => $year]);
            
            if (!$id) {
                return 'sections';
            }

            if ($this->select($app, $section, $year, $options)) {
                return 'options';
            }

            $app->query("INSERT INTO config_options SET year = :year, section = :section, options = :options, created_at = NOW()",['year' => $year, 'section' => $section, 'options' => $options]);
            return $app->lastID();
        }



        public function getClasses (?int $id_sections = null)
        {
            if ($id_sections) {
                $classes = $this->app->query("SELECT * FROM _easytalk_classes WHERE id_year = :id_year AND id_options = :options AND id_sections = :sections", ['id_year' => $this->id_year, 'options' => $this->id, 'sections' => $id_sections])
                               ->fetchAll(PDO::FETCH_CLASS, Classe::class);
                return $classes ? $classes : null;
            }

            $classes = $this->app->query("SELECT * FROM _easytalk_classes WHERE id_year = :id_year AND id_options = :options", ['id_year' => $this->id_year, 'options' => $this->id])
                           ->fetchAll(PDO::FETCH_CLASS, Classe::class);
            return $classes ? $classes : null;
        }

        public function updateEnseignement (int $id_year, int $id_enregistrement, int $id_section)
        {            
            $enseignement = $this->select($id_year, $id_enregistrement);
            if (!$enseignement) {
                return "Cet enseignement n'existe pas";
            }

            $check_section = $this->getSection($id_section, $id_year);

            if (!$check_section || $enseignement->id_section == $id_section) {
                return "Veillez modifier la section";
            }

            $option = $this->app->fetch("SELECT * FROM _easytalk_options WHERE id_year = :id_year AND options = :options AND id_section = :id_section", ['id_year' => $id_year, 'options' => $enseignement->options, 'id_section' => $id_section]);
            if ($option) {
                return "Cet enseignement existe déjà";
            }

            $this->app->query('UPDATE _easytalk_options SET id_section = :id_section, created_at = NOW() WHERE id = :id AND id_year = :id_year', ['id_section' => $id_section, 'id' => $id_enregistrement, 'id_year' => $id_year]);

            $update = $this->getSection($id_section, $id_year);
            return $update->section;
        }
    }