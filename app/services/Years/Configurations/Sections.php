<?php
namespace App\EasyTalk\Years\Configurations;
use App\App;
use App\EasyTalk\Years\Classes\Classe;

use \PDO;

    class Sections {

        public $id;
        public $section;
        public $created_at;
        public $year;

        /*private $app;

        public function __construct (?App $app = null)
        {
            $this->app = $app ? $app : new App(true);
        }*/

        public function countSection (App $app): ?int
        {
            return $app->count("SELECT COUNT(id) FROM config_sections WHERE year = :year", ['year' => $this->year]);
        }

        public function getClasses (?int $id_options = null)
        {
            if ($id_options) {
                $classes = $this->app->query("SELECT * FROM _easytalk_classes WHERE id_year = :id_year AND id_section = :section AND id_options = :options", ['id_year' => $this->id_year, 'section' => $this->id, 'options' => $id_options])
                               ->fetchAll(PDO::FETCH_CLASS, Classe::class);
                return $classes ? $classes : null;
            }

            $classes = $this->app->query("SELECT * FROM _easytalk_classes WHERE id_year = :id_year AND id_section = :section", ['id_year' => $this->id_year, 'section' => $this->id])
                           ->fetchAll(PDO::FETCH_CLASS, Classe::class);
            return $classes ? $classes : null;
        }

        private function select (App $app, int $id, ?string $section = null)
        {
            if ($section) {
                return $app->fetch("SELECT * FROM config_sections WHERE year = :year AND section = :section", ['year' => $id, 'section' => $section], Sections::class);
            }

            return $app->fetch("SELECT * FROM config_sections WHERE year = :year ORDER BY created_at DESC LIMIT 0,1", ['year' => $id], Sections::class);
        }

        public function addSection (App $app, int $year, string $section)
        {
            if ($this->select($app, $year, $section)) {
                return null;
            }

            $app->query("INSERT INTO config_sections SET section = :section, year = :year,  created_at = NOW()", ['year' => $year, 'section' => $section]);
            return $this->select($app, $year);
        }
    }