<?php
namespace App\EasyTalk\Years\Students;

use App\App;
use App\EasyTalk\Years\Classes\Classe;
use \PDO;

    class Students {

        public $id;
        public $id_year;
        public $id_user;
        public $id_class;
        public $reduction;
        public $adresse;
        public $redoublant;
        public $absences;
        public $pension;
        public $avatar;
        public $created_at;

        public function getClass (App $app): ?Classe
        {
            return $app->fetch('SELECT * FROM years_classes WHERE id = :id', ['id' => $this->id_class], Classe::Class);
        }

        public function getStatut(): string
        {
            if ($this->reduction) {
                return 'OUI';
            }
            return 'NON';
        }

        public function totalAbsences (): ?int
        {
            $absences = explode('-', $this->absences);

            $absence = 0;
            foreach ($absences as $a) {
                $absence += $a;
            }

            return $absence;
        }

        public function getPension (): int
        {
            $pensions = explode('-', $this->pension);

            $pension = 0;
            foreach ($pensions as $p) {
                $pension += $p;
            }
            return $pension;
        }

        public function setPension (int $total): string
        {
            if ($this->getPension() + $this->reduction < $total) {
                return 'En cour';
            }
            return 'Payer';
        }

        public function resPension (int $total): ?string
        {
            $res = $total - ($this->getPension() + $this->reduction);
            return ($res === 0) ? '<span class="alert-success">Solder</span>' : '<span class="alert-danger">' . $res . ' Fr</span>';
        }

    }