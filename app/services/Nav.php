<?php
namespace Service;

use App\Html\Text;
use App\Html\A;

class Nav {

    private $functions;
    private $admin = false;

    /*public function __construct (bool $admin, string $functions)
    {
        $this->functions = $functions;
        $this->admin = $admin;
    }*/

    private function sa (string $name, string $icon, ?int $notifs = null): string
    {
        return '<a href="#" class="sous-menu-link">' . Text::i($icon) . ' ' . $name . ($notifs ? ' <span class="span-label">' . $notifs . '</span>' : null) . '</a>';
    }

    public function a (string $href, string $icon, string $name, ?bool $active = false): string
    {
        return '<li>' . A::a($href, Text::i($icon) . ' ' . $name, 'nav', ($active ? ' active' : null)) . '</li>';
    }

    public function FormCheckbox (string $label, string $id, string $value, ?bool $checked = false): string
    {
        return
        '<div class="">
            <input type="radio" name="checkbox-search" id="' . $id . '" value="' . $value . '"' . ($checked ? ' checked' : null) . '>
            <label for="' . $id . '">' . $label . '</label>
        </div>';
    }

    public function searchBLog (): ?string
    {
        //if ($this->admin) {
            return $this->FormCheckbox('Blog','blog-search', 'blog');
        //}
        //return null;
    }

    public function tBord (): string
    {
        return $this->a('dashboard', 'dashboard', 'Tableau de Bord', true);
    }

    public function adminEtablisement (?int $notifs = 0): ?string
    {
        return
        '<li class="sous-menu">'
            . $this->sa('Etablisements', 'building', $notifs) .

            '<ul class="sous-menu-content">'
                . $this->a('etablissements/add', 'plus', 'Nouvel établissement')
                . $this->a('etablissements/add-dirigeant', 'user', 'Nouveau dirigeant')
                . $this->a('etablissements/view', 'building', 'Listes des établissements') .
            '</ul>
        </li>';
    }

    public function logs (?int $notifs = 0): ?string
    {
        return
        '<li class="sous-menu">'
            . $this->sa('Logs', 'shield', $notifs) .

            '<ul class="sous-menu-content">'
                . $this->a('logs/database', 'database', 'Base de données')
                . $this->a('logs/email', 'envelope-o ', 'Emails')
                . $this->a('logs/error', 'exclamation-triangle', 'Erreurs') .
            '</ul>
        </li>';
    }







    public function alarme (): ?string
    {
        if ($this->admin) {
            return $this->a('alarme/bullhorn', 'bullhorn', 'Alarme');
        }
        return null;
    }

    public function adminSite (?int $notifs = 0): ?string
    {
        if ($this->admin) {
            return
            '<li class="sous-menu">'
                . $this->sa('Site web', 'th', $notifs) .

                '<ul class="sous-menu-content">'
                    . $this->a('site-web/blog', 'book', 'Blog')
                    . $this->a('site-web/galerie', 'camera', 'Galerie')
                    . $this->a('site-web/partenaires', 'handshake-o', 'Partenaires') .
                '</ul>
            </li>';
        }
        return null;
    }

    public function applications (?int $notifs = 0): ?string
    {
        if ($this->admin) {
            return
            '<li class="sous-menu">'
                . $this->sa('Application', 'cogs', $notifs) .

                '<ul class="sous-menu-content">'
                    . $this->a('applications/activites', 'exchange', 'Activités')
                    . $this->a('applications/administration', 'sitemap', 'Administration') .
                '</ul>
            </li>';
        }
        return null;
    }

    public function etablisement (?int $notifs = 0): ?string
    {
        if ($this->admin) {
            return
            '<li class="sous-menu">'
                . $this->sa('Etablisement', 'building', $notifs) .

                '<ul class="sous-menu-content">'
                    . $this->a('etablisement/eleves', 'users', 'Listes des élèves')
                    . $this->a('etablisement/classes', 'building', 'Listes des classes')
                    . $this->a('etablisement/professeurs', 'briefcase', 'Listes des professeurs') .
                '</ul>
            </li>';
        }
        return null;
    }

    public function contacts (?int $notifs = 0): ?string
    {
        return
        '<li class="sous-menu">'
            . $this->sa('Contacts', 'comments-o', $notifs) .

            '<ul class="sous-menu-content">'
                . $this->a('contacts/mail', 'envelope', 'Mail')
                . $this->a('contacts/messagerie', 'comments-o', 'Messagerie') .
            '</ul>
        </li>';
    }
}
