<?php

namespace Service\Auth;

use Repositories\BaseRepository;
use Service\Service;

class AuthService extends Service {

    /**
     * @var BaseRepository
     */
    private $users_repository;

    public function __construct()
    {

        $this->users_repository = new BaseRepository('users');
    }

    public function login(string $username, string $password, ?string $remember)
    {

        $user = $this->users_repository->app()->fetch("SELECT id, password FROM users WHERE (username = :username OR email = :username) AND confirmed_at IS NOT NULL", ['username' => $username], User::class);

        if (!$user) {
            return null;
        }

        if (password_verify($password, $user->getPassword())) {
            $id = $user->getId();
            if ($remember) {
                $token = Text::random(250);
                $this->app->query("UPDATE users SET remember_token = :token WHERE id = :id", ['token' => $token, 'id' => $id]);
                $this->cookies->write('remember', $id . '==' . $token . sha1($id . 'easyTalkappjifitech'), 60 * 60 * 24 * 7);
            }
            return $id;
        }

        return null;
    }
}

