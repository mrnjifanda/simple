<?php

namespace Service\Admin\Etablissements\Configs;

use Service\Admin\AdminService;
use Model\Admin\Etablissements\Configs\TypesModel;

class TypesService extends AdminService
{
    protected static $table = 'schools_configs_type';
    protected static $classMapings = TypesModel::class;
}
