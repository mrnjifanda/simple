<?php

namespace Service\Admin\Etablissements\Configs;

use Service\Admin\AdminService;
use Model\Admin\Etablissements\Configs\CyclesModel;

class CyclesService extends AdminService
{
    protected static $table = 'schools_configs_cycle';
    protected static $classMapings = CyclesModel::class;
}
