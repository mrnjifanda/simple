<?php

namespace Service\Admin\Etablissements\Configs;

use Service\Admin\AdminService;
use Model\Admin\Etablissements\Configs\LanguesModel;

class LanguesService extends AdminService
{
    protected static $table = 'schools_configs_langues';
    protected static $classMapings = LanguesModel::class;
}
