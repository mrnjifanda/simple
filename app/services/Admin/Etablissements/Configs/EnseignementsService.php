<?php

namespace Service\Admin\Etablissements\Configs;

use Service\Admin\AdminService;
use Model\Admin\Etablissements\Configs\EnseignementsModel;

class EnseignementsService extends AdminService
{
    protected static $table = 'schools_configs_enseignement';
    protected static $classMapings = EnseignementsModel::class;
}
