<?php
namespace Service\Admin\Etablissements;

use Service\Admin\AdminService;
use Model\Admin\Etablissements\EtablissementsAddressModel;

class EtablissementsAddressService extends AdminService
{
    protected static $table = 'schools_address';
    protected static $classMapings = EtablissementsAddressModel::class;
    
}
