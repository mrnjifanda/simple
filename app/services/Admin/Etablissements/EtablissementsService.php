<?php
namespace Service\Admin\Etablissements;

use Service\Admin\AdminService;
use Model\Admin\Etablissements\EtablissementsModel;

class EtablissementsService extends AdminService
{
    protected static $table = 'schools';
    protected static $classMapings = EtablissementsModel::class;
    
}
