<?php

namespace Service\Admin\Zones;

use Service\Admin\AdminService;
use Repositories\BaseRepository;

class ContinentsService extends AdminService {

    public function __construct()
    {

        $this->base_repository = new BaseRepository('continents');
    }
}
