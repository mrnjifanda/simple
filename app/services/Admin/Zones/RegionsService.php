<?php

namespace Service\Admin\Zones;

use Service\Admin\AdminService;
use Model\Admin\Zones\RegionsModel;

class RegionsService extends AdminService
{
    protected static $table = 'zones_regions';
    protected static $classMapings = RegionsModel::class;
}
