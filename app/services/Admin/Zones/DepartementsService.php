<?php

namespace Service\Admin\Zones;

use Service\Admin\AdminService;
use Model\Admin\Zones\DepartementsModel;

class DepartementsService extends AdminService
{
    protected static $table = 'zones_departements';
    protected static $classMapings = DepartementsModel::class;
}
