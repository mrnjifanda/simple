<?php
    
namespace Service\Admin\Zones;

use Service\Admin\AdminService;
use Model\Admin\Zones\ArrondissementsModel;

class ArrondissementsService extends AdminService {

    protected static $table = 'zones_arrondissement';
    protected static $classMapings = ArrondissementsModel::class;
}
