<?php

namespace Service\Admin\Zones;

use Service\Admin\AdminService;
use Repositories\BaseRepository;
use Model\Admin\Zones\CountriesModel;

class CountriesService extends AdminService
{

    public function __construct()
    {

        $this->base_repository = new BaseRepository('countries');
    }
}
