<?php

namespace Service;

use App\App;
use Model\Years\YearModel;

class YearsService
{
    private static function request(App $app, string $method, array $where = [], ?string $custom = null)
    {
        return $app->$method('years', YearModel::class, "id, debut, fin", $where, $custom);
    }

    public static function getLastYear(App $app): YearModel
    {
        return self::request($app, 'find', [], "ORDER BY id DESC LIMIT 0,1");
    }

    public static function getAllYears(App $app)
    {
        return self::request($app, 'all', [], "ORDER BY id DESC");
    }

    public static function getYear(App $app, int $id): YearModel
    {
        return self::request($app, 'find', ['id' => $id]);
    }

    public static function getActiveYear(App $app, ?int $id = null): YearModel
    {
        return $id ? self::getYear($app, $id) : self::getLastYear($app);
    }

    public static function countYears(App $app): ?int
    {
        return $app->slimCount('years');
    }
}
