<?php

    namespace Service\Queues;

    use App\Helpers\Date;
    use App\Logs\QueuesLogs;
    use App\Queues\Queues;
    use Service\Service;
    use Service\Send\MailService;

    class QueuesService extends Service
    {

        private static $queues = [
            'mails' => [
                'register' => [
                    'class' => MailService::class,
                    'method' => 'register'
                ],
                'forgetPassword' => [
                    'class' => MailService::class,
                    'method' => 'forgetPassword'
                ]
            ],
            'upload' => ''
        ];

        private $message;

        public static function getAction(string $categorie, string $action): ?array
        {

            return isset(self::$queues[$categorie][$action]) ? self::$queues[$categorie][$action] : null;
        }

        public function getQueue(string $queue_name)
        {
            # code...
        }

        public static function push(string $queue, array $datas): void
        {

            Queues::push($datas, $queue);
        }

        public static function run(string $message, string $queue_name)
        {

            try {

                $body = json_decode($message, true);
                $categorie = $body['categorie'];
                $action = $body['action'];
                $class_method = self::getAction($categorie, $action);
                if ($class_method) {

                    $class = new $class_method['class'](false);
                    $method = $class_method['method'];
                    $datas = $body['datas'];
                    $argc = array_values($datas);
                    call_user_func_array([$class, $method], $argc);
                    $body['created_at'] = Date::toDay();
                    QueuesLogs::setFileLogs($body);
                    print_r($message);
                } else {

                    print_r('Error #Send mail admin and error');
                    # Send mail admin ($message, $queue_name)
                }
            } catch (\Throwable $th) {
                //throw $th;
                 # Send mail admin and error
                echo json_encode([
                    "message" => $th->getMessage(),
                    "code" => $th->getCode()
                ]);
            }

            $queue = (new static)->getQueue($queue_name);
        }
    }
    