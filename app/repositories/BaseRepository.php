<?php

namespace Repositories;

use Model\Users\User;
use Model\API\BasicAuth;

class BaseRepository extends ModelRepository
{

    /**
     * Name of the model you want to use
     * Example: users for the User model
     *
     * @var string $model_name
     */
    protected $model_name;

    public function __construct(string $model_name)
    {

        $this->model_name = $model_name;
        $this->model = new ($this->model())();
    }

    /**
     * arrays of available models
     *
     * @var array $models
     */
    public $repository_models = [
        'basic_auth' => BasicAuth::class,
        'users' => User::class
    ];

    /**
     * searchabe keys for a model
     *
     * @var array $search_able
     */
    public $search_able = [
        'users' => [
            'first_name', 'last_name', 'email', 'phone_number', 'password',
            'social_id', 'social_type', 'last_login_day', 'last_login_time', 'last_login_ip',
            'confirmation_token', 'reset_token', 'api_token', 'api_token_expired_at',
            'status'
        ],
        'basic_auth' => ['api_username', 'api_password', 'api_role', 'status']
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {

        try {

            if (!array_key_exists($this->model_name, $this->search_able)) {

                // throw new ModelNotFoundException;
                throw new \Exception("Error Processing Request", 1);
            }
        } catch (\Exception $err) {

            // Log::error($err);
            dd($err);
        }
        return $this->search_able[$this->model_name];
    }

    /**
     * Configure the Model
     **/
    public function model()
    {

        try {

            if (!array_key_exists($this->model_name, $this->repository_models)) {

                // throw new ModelNotFoundException;
                throw new \Exception("Error Processing Request", 1);
            }
        } catch (\Exception $err) {

            dd($err);
        }
        return $this->repository_models[$this->model_name];
    }
}
