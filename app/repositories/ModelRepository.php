<?php

    namespace Repositories;

    use App\Logs\Logs;
    use App\Helpers\Date;
    use App\Helpers\Sessions;

    abstract class ModelRepository
    {

        protected $model;

        private function logs(string $action, int $id_item, $data = null, $old_data = null): void
        {

            $content = [
                "action" => $action,
                "table" => $this->model->getTable(),
                "id_user" => Sessions::$sessions->read('auth'),
                "id_item" => $id_item,
                "created_at" => Date::toDay(),
                "data" => $data
            ];

            if ($old_data) {

                $content['old_data'] = $old_data;
            }
            Logs::setFileLogs($content);
        }

        public function app()
        {

            return $this->model::app();
        }

        public function create(array $datas, bool $return = false)
        {

            $create = $this->model::create($datas);
            $this->logs('INSERT', $create, $datas);
            if ($return) {

                return $this->find($create);
            }
        }

        public function all(string|array $query = '*')
        {

            return $this->model::all($query);
        }

        public function find(int|array $query)
        {

            return $this->model::find($query);
        }

        public function update (int $id, array $datas, bool $return = true)
        {

            $old_data =  $this->find($id);
            if (!$old_data) {

                return 'item not find';
            }

            $this->model::update($id, $datas);
            $this->logs('UPDATE', $id, $datas, $old_data);
            if ($return) {

                return $this->find($id);
            }
        }

        public function delete (int $id, array $wheres)
        {

            $old_data =  $this->find($id);
            if (!$old_data) {

                return 'item not find';
            }
            $this->model::delete($id, $wheres);
            $this->logs('DELETE', $id, $old_data);
        }

        public function paginated(int $page, string|array $query = '*', int $per_page = 25)
        {

            return $this->model::paginated($page, $query, $per_page);
        }

        public function count(array $where = []): ?int
        {

            return $this->model::count($where);
        }
    }
