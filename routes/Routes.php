<?php
define('URL_ARRAY', ['login', 'unlock', 'forget-password', 'reset-password', 'activate-account']);
define('URL_FIRST', (isset(URL[1]) ? URL[1] : null));
define('URL_SECOND', (isset(URL[2]) ? URL[2] : null));
define('URL_PAGE', in_array(URL_FIRST, URL_ARRAY));
define('ROUTES_PATH', ROOT . 'routes' . DIRECTORY_SEPARATOR);

if (URL_PAGE) {

    require_once ROUTES_PATH . 'authRoutes.php'; # Toutes les routes qui ne nesicites pas d'être connecté.
} else {

    if (file_exists(ROUTES_PATH . URL_FIRST . 'Routes.php')) {

        require_once ROUTES_PATH . URL_FIRST . 'Routes.php';
    } else {

        if (is_dir(ROUTES_PATH . URL_FIRST) && file_exists(ROUTES_PATH . URL_FIRST . DIRECTORY_SEPARATOR . URL_SECOND . 'Routes.php')) {

            require_once ROUTES_PATH . URL_FIRST . DIRECTORY_SEPARATOR . URL_SECOND . 'Routes.php';
        }
    }
}

/**
 * @var App\Router $router
 */
$router
    ->redirect('/', '/dashboard')
    ->redirect('/api/documentation', '/docs/swagger')

    ->get('/dashboard', 'Controller\AppController#dashboard', 'dashboard')
    ->get('/account', 'Controller\AppController#account')

    ->post('/logout', 'Controller\AppController#logout', 'logout')
    ->post('/lock', 'Controller\AppController#lock', 'lock');

require_once ROOT . 'routes' . DIRECTORY_SEPARATOR . 'api' . DIRECTORY_SEPARATOR . 'api.php';
$router->run();
