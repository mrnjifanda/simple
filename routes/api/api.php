<?php

$namespaceApi = 'Controller\API\ApiController#';
$auth = 'Controller\API\ApiAuthController#';

$router
    ->prefix('/api/v1')
    
    ->post('/login', $auth . 'login', 'api.login')
    ->post('/forget-password', $auth . 'forgetPassword', 'api.forget-password.send')

    ->get('/users', $namespaceApi . 'users', 'api.users')
    ->get('/users/[i:id]', $namespaceApi . 'user', 'api.user')

    ->removePrefix()
;