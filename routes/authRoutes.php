<?php

/**
 * @var App\Router $router
 */
$router

    ->mix('/login', 'Controller\Auth\AuthController#login', 'login')
    ->mix('/forget-password', 'Controller\Auth\AuthController#forgetPassword', 'forget-password')
    ->mix('/reset-password/[a:username]/[*:token]', 'Controller\Auth\AuthController#resetPassword', 'reset-password')

    ->post('/unlock', 'lock')
;