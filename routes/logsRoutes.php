<?php
$namespaceDatabase = 'Controller\Admin\Logs\LogsDatabaseController#';
$namespaceEmail = 'Controller\Admin\Logs\LogsEmailController#';
$namespaceError = 'Controller\Admin\Logs\LogsErrorController#';

/**
 * @var App\Router $router
 */
$router
    ->get('/logs/database', $namespaceDatabase . 'database', 'database-logs')
    ->get('/logs/[*:day]/view/[i:id]', $namespaceDatabase . 'databaseLogDetail', 'database-logs-views')
    ->get('/logs/database/[*:day]', $namespaceDatabase . 'viewDetailDay', 'database-day')

    ->get('/logs/email', $namespaceEmail . 'email', 'email-logs')

    
    ->get('/logs/error', $namespaceError . 'error', 'error-logs')
;